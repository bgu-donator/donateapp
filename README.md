# Welcome to Donate it App
[![DonateItApp](https://img.shields.io/endpoint?url=https://dashboard.cypress.io/badge/detailed/mwbx9k&style=flat&logo=cypress)](https://dashboard.cypress.io/projects/mwbx9k/runs)
[![CodeFactor](https://www.codefactor.io/repository/bitbucket/bgu-donator/donateapp/badge/master?s=d8f00cd73fe319d4188fdf642d945211b2554b6b)](https://www.codefactor.io/repository/bitbucket/bgu-donator/donateapp/overview/master)
![Version](https://img.shields.io/badge/version-0.1-blue.svg?cacheSeconds=2592000)
[![Documentation](https://img.shields.io/badge/documentation-yes-brightgreen.svg)](doc)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](url)
![PyPI - Python Version](https://img.shields.io/pypi/pyversions/FastApi?color=purple)
![node-current](https://img.shields.io/node/v/vue-cli)

This project created as a 4th year graduating project at BGU.
The purpose of this project is to create a platform for non-profit associations and users
to sell 2nd-hand products and donate the money to a chosen association.

The Backend API Server developed using FastAPI (Python)

The Frontend developed in VueJS using Quasar Framework (for mobile devices native apps)

### 🏠 [Homepage](https://donateitapp.herokuapp.com/)

## Goto
### [Backend](Backend)
### [Frontend](Frontend/web)


## Run tests
Use the following command to run the tests of this project Backend,

```sh
cd Backend && pytest -s -v -W ignore::DeprecationWarning
```

Use the following command to get the coverage report of this project, <br>
this will open Microsoft Edge with the Coverage report.
```sh
coverage run --source=. --omit=tests/* -m pytest -s -v -W ignore::DeprecationWarning && coverage html && "C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" %CD%\htmlcov\index.html
```

Use the following command to run the tests of this project Frontend,
must first start vue server

```sh
** Make sure the Backend is running **
1. cd Frontend\web
2. start npm run serve
3. npx cypress run
```


## Authors

👤 **Merav Shaked**
* Github: [@shmera](https://github.com/shmera)

👤 **Yuval Ben Eliezer**
* Github: [@ybene](https://github.com/ybene)

👤 **Tair Cohen**
* Github: [@taircohen94](https://github.com/taircohen94)

👤 **Gal Rosenthal**
* Github: [@galrosenthal](https://github.com/galrosenthal)


## 🤝 Contributing

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://bgu-donator2.atlassian.net/jira/software/projects/DITA/boards/4/backlog). 


## 📝 License

Copyright © 2021

This project is [MIT](url) licensed.

***
_This README was generated with ❤ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
