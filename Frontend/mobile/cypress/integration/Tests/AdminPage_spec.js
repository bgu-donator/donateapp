// import { readFile } from 'fs';
const users_array_file = 'array_of_3_users.json'

function reset_users_array(){

  cy.readFile('cypress/fixtures/' + users_array_file + '.orig').then((users) => {
    set_users_array(users)
  })
}

function set_users_array(object_to_write_to_file){
  cy.writeFile('cypress/fixtures/' + users_array_file, object_to_write_to_file)
}

describe('Admin Page Tests', function () {
  beforeEach('Loading Website', () => {
    cy.getHome()
    reset_users_array()
  })



  it('Test login with correct user', function () {
    cy.interceptValidLogin()
    cy.login_with_params('shtut@gmail.com', '123456');
    cy.waitUntil(() => cy.getCookie('diaCookie')
      .then(cookie => Boolean(cookie && cookie.value)));
    cy.getCookie('diaCookie').should('exist')
    cy.location('hash').should('not.contain','login')
  });

  it('Test login with wrong user', function () {
    cy.interceptInvalidLogin()
    cy.login_with_params('shtut@gmail.com','123')
    cy.getCookie('diaCookie').should('not.exist')
    cy.location('hash').should('contain','login')
  });

  it('Test Admin page with no admin user', () => {
    cy.interceptInvalidLogin()
    cy.login_with_params('shtut@gmail.com', '123')
    cy.location('hash').should('contain', 'login')
    cy.getCookie('diaAdmin').should('not.exist')

    cy.visit('/admin')
    cy.location('hash').should('not.contain', 'admin')
    cy.getCookie('diaAdmin').should('not.exist')
  })

  function validate_existing_non_admin_user() {
    cy.request({
      url: 'https://localhost:8000/admin/set_admin',
      method: 'POST',
      body: {
        "shtut12@gmail.com": false
      },
    })
  }




  it('Test Admin page with admin user set admin', () => {
    cy.interceptValidLogin(true)


    cy.intercept('POST','/admin/set_admin', (req) => {
      req.reply({
        statusCode: 202,
        body: 'Success'
      })
    }).as('set_admin')

    cy.intercept('/admin', {fixture: 'array_of_3_users'})

    cy.login_with_params('shtut@gmail.com', '123456')
    cy.location('hash').should('not.contain', 'login')

      // validate_existing_non_admin_user()
    cy.waitUntil(() => cy.getCookie('diaCookie')
      .then(cookie => Boolean(cookie && cookie.value)));
    cy.waitUntil(() => cy.getCookie('diaAdmin')
      .then(cookie => Boolean(cookie && cookie.value)));

    cy.visit('/admin')
    cy.wait(1500)
    cy.location('hash').should('contain', 'admin')


    cy.get('button[type=button]')
      .within(() => {
        cy.contains('Set user as Admin').click()
      })

    cy.get('h2')
      .contains('Set User As Admin')
    cy.get('h5')
      .contains('Select User to make admin')

    cy.wait(500)

    cy.get('.q-select')
      .should('exist')
      .within(() => {
        cy.contains('arrow_drop_down').click()
      })
    cy.wait(500)


    cy.get('.q-menu')
      .contains('test1@test.com')
      .click()

    cy.get('button[type=button]')
      .within(() => {
        cy.contains('Set as Admin').click()
      })

    cy.get('#confirm_btn')
      .click()

    // validate_existing_non_admin_user()


  })

});
