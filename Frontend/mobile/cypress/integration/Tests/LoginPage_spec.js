describe('Login Page Tests', function () {

  beforeEach('Loading Website', () => {
    cy.getHome()
    cy.visit('/loginUser')
  })

  it('Test login with correct user', function () {
    cy.interceptValidLogin()
    cy.login_with_params('shtut@gmail.com', '123456');
    cy.location('hash').should('not.contain','login')
  });

  it('Test login with wrong user', function () {
    cy.interceptInvalidLogin()
    cy.login_with_params('shtut@gmail.com', '123');
    cy.location('hash').should('contain','login')
  });


});
