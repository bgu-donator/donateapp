describe('Home Page Tests', function () {

  beforeEach('Loading Website', () => {
    cy.getHome()
  })

  it('Validate NavBar is non logined', function () {
    cy.contains('עמוד הבית')
    cy.contains('התחברות')
    cy.contains('הרשמה')
    cy.contains('העמותות')
    cy.contains('logout').should('not.exist')
    cy.contains('user').should('not.exist')
    cy.contains('upload').should('not.exist')


  });

  it('Validate Carousle', () => {
    cy.get('.contents-web')
      .should('have.length', 2)
  })
});
