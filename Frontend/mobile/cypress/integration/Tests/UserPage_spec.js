describe('User Page Tests', function () {
  let prefix = ".dia-p-"
  beforeEach('Loading Website', () => {
    cy.getHome()
  })

  // afterEach('Deleting Cookies', () => {
  //   cy.log("Clearing cookies for next tests")
  //   cy.clearCookies()
  //   // TODO: validate the products in mongo and the user,
  //   //  should be 2 products and the user should have only 1 in its arr
  // })

  it('Test user details without login', function () {
    cy.visit('/user')
    cy.location('hash').should('not.contain', 'user')
    cy.location('hash').should('contain', 'login')

  });

  function test_login(email, password, should_be_logined) {
    // Test user logged in as admin user
    cy.interceptValidLogin(should_be_logined)
    let contain_value = ''
    let exist_value = 'not.'
    if (should_be_logined) {
      contain_value = 'not.'
      exist_value = ''
    }
    cy.login_with_params(email, password);
    cy.location('hash').should(contain_value + 'contain', 'login')

    if(should_be_logined)
    {
      cy.waitUntil(() =>
        cy.getCookie('diaAdmin')
        .then(cookie => Boolean(cookie && cookie.value))
      );
    }
    cy.getCookie('diaCookie').should(exist_value + 'exist')
  }

  it('Test user details with user', function () {
    test_login('shtut@gmail.com', '123456', true);

  });

  function validate_btn_failed() {
    cy.get('button[type=submit]')
      .should('exist')
      .click()
    cy.location('hash').should('contain', 'uploadProduct')
  }
  function uploadProduct() {
    cy.visit('/uploadProduct')
    cy.location('hash').should('contain', 'uploadProduct')

    validate_btn_failed()

    cy.get(prefix+'name')
      .should('exist')
      .type('product')
      .get('input[type=text]')
      .should('have.value', 'product')

    cy.get('.q-select')
      .should('exist')
      .within(() => {
        cy.contains('arrow_drop_down').click()
      }).wait(200)


    cy.get('.q-menu')
      .scrollTo('top')
      .contains('אבו גוש')
      .click()

    validate_btn_failed()

    cy.get(prefix+'price')
      .should('exist')
      .type('3500')

    validate_btn_failed()

    cy.get('input[type=number]')
      .should('exist')
      .should('have.value', '3500')

    validate_btn_failed()

    cy.get(prefix+'description')
      .should('exist')
      .type('desc')
      .get('textarea')
      .should('have.value', 'desc')

    validate_btn_failed()

    cy.get(prefix+'phone')
      .should('exist')
      .type('0528492147')
      .get('input[type=tel]')
      .should('have.value', '(052) 849 - 2147')

    validate_btn_failed()

    cy.get(prefix+'img')
      .should('exist')
      .type('http://www.google.com')
      .within(() => {
        cy.get('input[type=text]')
          .should('have.value', 'http://www.google.com')
      })


    cy.get(prefix+'main')
      .should('exist')
      .within(() => {
        cy.get('input[type=search]').click()
      })

    cy.get('.q-menu')
      .scrollTo('top')
      .contains('אלקטרוניקה')
      .click()

    cy.get(prefix+'sub')
      .should('exist')
      .click()

    cy.get('.q-menu')
      .contains('טאבלט')
      .click()

    cy.get('button[type=submit]')
      .should('exist')
      .click().end()

  }

  it('Test User delete inserted product', () => {
    test_login('shtut@gmail.com', '123456', true);
    cy.intercept('POST','/product/add_product', async (req) => {
      req.reply({
        statusCode: 201,
        fixture: 'add_product'
      })
    })

    cy.intercept('GET', '/users/shtut@gmail.com', (req) => {
      req.reply({
        statusCode: 200,
        fixture: 'validUser'
      })
    }).as('get_user')

    cy.intercept('GET', '/users/get_donations/shtut@gmail.com', (req) => {
      req.reply({
        statusCode: 200,
        fixture: 'getDonations'
      })
    })
    uploadProduct()
    cy.wait(1500)

    cy.visit('/user')
    cy.location('hash').should('contain', 'user')

    cy.get('.clearBtn').should('exist')
    cy.intercept('DELETE', '/delete/*', {
      statusCode: 202
    })
    cy.get('.clearBtn').click().end()
    cy.get('.clearBtn').should('not.exist')
  })

});
