/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
const fs = require('fs')

function write_file(path, data){
  return fs.writeFileSync(path,data)
}
/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
  on('task', {
    readFileFixture(data) {
      const filePath = data['path']
      const users = data['users']
      let users_list = users.map((user) => Object.keys(user)[0])
      if (!users_list) {
        return null
      }
      else {
        if (fs.existsSync(filePath)) {
          let file_content = fs.readFileSync(filePath, 'utf8')

          let u_idx = file_content['Users'].findIndex((value, index, array) => {
            return value['email'] === users_list[0]
          })

          file_content['Users'][u_idx]['is_admin'] = true
          file_content['Admins'].push(file_content['Users'][u_idx])

        }
        return write_file(filePath, users_obj)
      }

      return null
    },
  })
}
