import Vue from 'vue'
import VueRouter from 'vue-router'
import ProgressBar from 'vuejs-progress-bar'

import routes from './routes'
import store from '../store/index'

Vue.use(VueRouter)
Vue.use(ProgressBar)
/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requireAuth)) {
      // this route requires auth, check if logged in
      // if not, redirect to login page.
      if (to.matched.some(record => record.meta.requireAdmin)) {
        if (store().getters['user/admin'] == null) {
          next('/') // go to '/login';
        } else {
          next()
        }
      }
      else if (to.matched.some(record => record.meta.requireAssociation)) {
        if (store().getters['user/Association'] == null) {
          next('/')// go to '/login';
        } else {
          next()
        }
      }
      else if (store().getters['user/user'] == null) {
        next('/loginUser') // go to '/login';
      } else {
        next()
      }
    } else {
      next()
    }
  })
  return Router
}
