import firebase from 'firebase/app'
import 'firebase/auth'
import {Loading, LocalStorage} from 'quasar'
import store from '../store/index'
import {firebaseConfig} from './firebase-config'
import { api } from 'boot/axios'

const firebaseApp = firebase.initializeApp(firebaseConfig)
const firebaseAuth = firebaseApp.auth()
const googleProvider = new firebase.auth.GoogleAuthProvider()


function handleFirebaseResult(app, result) {
  console.log('INSIDE REDIRECT RESULT')
  try {
    console.log("GOT CREDS")
    /** @type {firebase.auth.OAuthCredential} */
    var credential = result.credential
    // This gives you a Google Access Token. You can use it to access the Google API.
    // eslint-disable-next-line no-unused-vars
    var token = credential.accessToken
    // eslint-disable-next-line camelcase
    var token_id = firebaseAuth.currentUser.getIdToken()
    // eslint-disable-next-line camelcase
    console.log('Token: ', token_id, typeof token_id)
    // ...
    // The signed-in user info.
    var user = result.user
    // eslint-disable-next-line camelcase
    const send_user_params = {
      token_id: token_id,
      email: user.email,
      uid: user.uid,
      name: user.displayName
    }

    // send token_id to backend with the user account
    // eslint-disable-next-line camelcase
    let is_valid = false
    const ip = store().getters['ip/ip']
    console.log("GET COOKIES")
    let url_to_login = ip + '/users/firebase_auth'
    api.post(url_to_login, send_user_params)
      .then(async (response) => {
        console.log("Response from Backend on Firbase Redirect")
        if (response.status === 200) {
          // eslint-disable-next-line camelcase
          is_valid = true
        } else {
          // eslint-disable-next-line camelcase
          is_valid = false
        }
        LocalStorage.set('loggedIn', is_valid)
        let dispatch_payload = {'email': send_user_params.email, 'url': url_to_login}
        const cookie_payload = await store().dispatch('user/get_cookies', dispatch_payload) // 1st Function
        // console.log(response.headers.cookies)
        store().dispatch('user/updateEmail', cookie_payload)
        Loading.hide()
        app.$router.push('/').catch(err => {
          console.log(err)
        })
      }).catch(err => {
      console.log(err)
    })
  } catch (error) {
    console.log("HANDLE FIREBASE RESULT ERROR")
    console.log(error)
  }


}

function handleRedirectResult(app) {
  firebaseAuth
    .getRedirectResult()
    .then((result) => {
      handleFirebaseResult(app, result)
    }).catch((error) => {
    // Handle Errors here.
    // eslint-disable-next-line no-unused-vars
    var errorCode = error.code
    // eslint-disable-next-line no-unused-vars
    var errorMessage = error.message
    // The email of the user's account used.
    // eslint-disable-next-line no-unused-vars
    var email = error.email
    // The firebase.auth.AuthCredential type that was used.
    // eslint-disable-next-line no-unused-vars
    var credential = error.credential
    // ...
  })
}

function handleAuthChanged(app) {
  firebaseAuth.onAuthStateChanged((user) => {
    console.log('Got new user on login')
    Loading.hide()
    // console.log("User: " + JSON.stringify(user))
    if (user) {
      // firebaseAuth.signOut()
      LocalStorage.set('loggedIn', true)
      app.$router.push('/').catch(err => {
        console.log(err)
      })
    } else {
      LocalStorage.set('loggedIn', false)
      app.$router.go(0).catch(err => {
        console.log(err)
      })
    }
  })
}

// function fbLogout(app){
//   firebaseAuth.signOut().catch(err => {
//     console.log(err)
//   })
//
//
// }

export {firebaseAuth, googleProvider, handleAuthChanged, handleRedirectResult, handleFirebaseResult}
