import store from '../store/index'

// all the data
const state = {
  category: null,
  categoryURL: null
}

// methods that manipulate state can't be async
const mutations = {
  updateCategory (state, payload) {
    state.category = payload
  },
  updateCategoryURL (state, payload) {
    state.categoryURL = payload
  },
}

// methods that manipulate state can be async
const actions = {
  // updateCategory ({ commit }, payload) {
  //   commit('updateCategory', payload)
  // },
  // updateCategoryURL ({ commit }, payload) {
  //   commit('updateCategoryURL', payload)
  // }
}

// data getters
const getters = {
  category: (state) => {
    return state.category
  },
  categoryURL: (state) => {
    return state.categoryURL
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
