// all the data
import { Cookies, LocalStorage } from 'quasar'
import store from '../store/index'
import {api as axios} from '../boot/axios'

const state = {
  email: LocalStorage.getItem('email'),
  userCookie: LocalStorage.getItem('diaCookie'),
  adminCookie: LocalStorage.getItem('diaAdmin'),
  AssociationCookie: LocalStorage.getItem('diaCookieASC')
}

// methods that manipulate state can't be async
const mutations = {
  logout (state) {
    LocalStorage.remove('email')
    state.email = ''
    LocalStorage.remove('diaCookie')
    LocalStorage.remove('diaAdmin')
    LocalStorage.remove('diaCookieASC')
    state.userCookie = LocalStorage.getItem('diaCookie')
    state.adminCookie = LocalStorage.getItem('diaAdmin')
    state.AssociationCookie = LocalStorage.getItem('diaCookieASC')
  },
  login (state, payload) {

    let admin = payload['admin']
    let user = payload['user']
    let asc = payload['asc']

    // console.log("##############################")
    // console.log(admin,user,asc)

    LocalStorage.set('email', payload['email'])
    LocalStorage.set(admin.cookie_key, admin.cookie_value)
    LocalStorage.set(user.cookie_key, user.cookie_value)
    LocalStorage.set(asc.cookie_key, asc.cookie_value)

    state.email = LocalStorage.getItem('email')
    state.userCookie = LocalStorage.getItem('diaCookie')
    state.adminCookie = LocalStorage.getItem('diaAdmin')
    state.AssociationCookie = LocalStorage.getItem('diaCookieASC')
  }
}
// methods that manipulate state can be async
const actions = {
  async logoutUser ({ commit }) {
    commit('logout')
    await axios.post(store().getters['ip/ip'] + '/users/logout')
  },
  updateEmail ({ commit }, payload) {
    commit('login', payload)
  },
  async get_cookies({ commit }, payload) {
    let email_addr = payload['email']
    let url_addr = payload['url']
    let cookies_payload = {}
    cookies_payload['email'] = email_addr
    cookies_payload['admin'] = {}
    cookies_payload['user'] = {}
    cookies_payload['asc'] = {}
    // console.log(window)
    try {
      if (await window.cordova) {
        await window.cordova.plugins.CookiesPlugin.getCookie(url_addr, async (cookies) => {
          let our_cookies = cookies.split(';')
          let admin=[], non_admin=[], asc=[];

          if (cookies.includes('Admin')){
            // Generate Admin and User cookies
            console.log('Admin Found')
            admin = our_cookies[0].split('=')
            non_admin = our_cookies[1].split('=')
            asc = ['','']
            console.log('AFTER ADMIN SPLIT')
            console.log(admin)
            console.log(non_admin)
          }
          else if (cookies.includes('Asc')){
            // Generate association cookies
            console.log('Assiciation Found')
            admin = ['','']
            non_admin = ['','']
          }
          else {
            // Generate non admin User cookies
            console.log('No Admin no Association Found')
            non_admin = our_cookies[0].split('=')
            admin = ['','']
            asc = ['','']
          }

          cookies_payload['admin'] = {'cookie_key': String(admin[0]).trim(), 'cookie_value': String(admin[1]).trim() + "==\""}
          cookies_payload['user'] = {'cookie_key': String(non_admin[0]).trim(), 'cookie_value': String(non_admin[1]).trim() + "==\""}
          cookies_payload['asc'] = {'cookie_key': String(asc[0]).trim(), 'cookie_value': String(asc[1]).trim() + "==\""}

          console.log('AFTER ASSGIN', cookies_payload['admin'],cookies_payload['user'],cookies_payload['asc'])

          console.log('BEFORE UPDATE',cookies_payload['admin'],cookies_payload['user'],cookies_payload['asc'])

          return cookies_payload

        })
      }
      else {
        console.log("GETTING COOKIES")
        console.log(Cookies.getAll())
        cookies_payload['admin'] = {'cookie_key': 'diaAdmin', 'cookie_value': Cookies.get('diaAdmin')}
        cookies_payload['user'] = {'cookie_key': 'diaCookie', 'cookie_value': Cookies.get('diaCookie')}
        cookies_payload['asc'] = {'cookie_key': 'diaCookieASC', 'cookie_value': Cookies.get('diaCookieASC')}
        return cookies_payload

      }

    }
    catch (e) {
      console.log(e)
      return null
    }
  }
}
// data getters
const getters = {
  email: (state) => {
    return state.email
  },
  user: (state) => {
    if(state.userCookie === null || state.userCookie === 'null')
    {
      return null
    }
    else{
      return state.userCookie
    }
  },
  admin: (state) => {
    if(state.adminCookie === null || state.adminCookie === 'null')
    {
      return null
    }
    else{
      return state.adminCookie
    }
  },
  Association: (state) => {
    if(state.AssociationCookie === null || state.AssociationCookie === 'null')
    {
      return null
    }
    else{
      return state.AssociationCookie
    }
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
