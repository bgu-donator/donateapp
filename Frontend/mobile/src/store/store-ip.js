// all the data
const state = {
  ip: 'https://localhost:8000'
  // ip: 'https://donateitapp.herokuapp.com'
}


// methods that manipulate state can't be async
const mutations = {}

// methods that manipulate state can be async
const actions = {}

// data getters
const getters = {
  ip: (state) => {
    return state.ip
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
