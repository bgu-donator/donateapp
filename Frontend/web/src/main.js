import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueRouter from "vue-router";
import store from './store'
import VueAxios from "vue-axios"
import axios from "axios"
import {shared_data} from "./localStore";

import Vuelidate from "vuelidate";

import './quasar'

import './styles/quasar.scss'
import '@quasar/extras/material-icons/material-icons.css'
import {
  Quasar,
  QVideo,
  Cookies
} from 'quasar'

Vue.config.productionTip = false
Vue.use(Vuelidate);
Vue.use(VueRouter);
axios.defaults.withCredentials = true
// Add a response interceptor
axios.interceptors.response.use(
  function(response) {
    // Do something with response data
    return response;
  },
  function(error) {
    // Do something with response error
    return Promise.reject(error);
  }
);



Vue.use(
  VueAxios,
  axios,
  Quasar, {
    components: {
      QVideo
    },
  config: {},
  plugins: {
      Cookies
  }
})



new Vue({
  router,
  data() {
    return {
      storeShare: shared_data,
    };
  },
  store,
  render: h => h(App)
}).$mount('#app')
