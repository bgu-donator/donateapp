import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import {shared_data as store} from "../localStore"

Vue.use(VueRouter)

const authPages = ['UserDetails', 'PaymentPage', 'UploadProductPage']


const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/SearchProducts/',
    name: 'SearchProducts',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "SearchProductsPage" */ '../views/SearchProductsPage.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/product/:productId',
    name: 'ProductPage',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/ProductPage.vue')
  },
  {
    path: '/loginUser',
    name: 'LoginUser',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/LoginUserPage.vue')
  },
  {
    path: '/loginAssoc',
    name: 'LoginAssoc',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/LoginAssocPage.vue')
  },
  {
    path: '/AssociationPage/:username',
    name: 'AssociationPage',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AssociationPage.vue')
  },
  {
    path: '/Associations',
    name: 'Associations',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Associations.vue')
  },
  {
    path: '/AssociationsSearch',
    name: 'AssociationsSearch',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../../../mobile/src/pages/AssociationsSearch.vue')
  },
  {
    path: '/payment/:productId',
    name: 'PaymentPage',
    meta: {
      requireAuth: true,
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/PaymentPage.vue')
  },
  {
    path: '/register',
    name: 'Registration',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/RegisterPage.vue')
  },
  {
    path: '/user',
    name: 'UserDetails',
    meta: {
      requireAuth: true,
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/UserDetails.vue')
  },
  {
    path: '/uploadProduct',
    name: 'UploadProductPage',
    meta: {
      requireAuth: true,
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/UploadProductPage.vue')
  }, {
    path: '/admin',
    name: 'admin',
    meta: {
      requireAuth: true,
      requireAdmin: true
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/AdminDashboard.vue')
  },
  {
    path: "*",
    name: "notFound",
    component: () => import(/* webpackChunkName: "about" */ '../views/NotFoundPage.vue')
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requireAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (to.matched.some(record => record.meta.requireAdmin)) {
      if (!store.isAdmin()) {
        next('/'); // go to '/login';
      }
      else {
        next()
      }
    }
    else if (!store.isLoggedIn()) {
        next('/login'); // go to '/login';
    }
    else {
      next()
    }

  }
  else {
    next()
  }
})

export default router
