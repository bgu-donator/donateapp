import firebase from "firebase/app"

import "firebase/auth"
import {Loading, LocalStorage} from "quasar";
import axios from "axios";

import {firebaseConfig} from "./firebase-config"
import {shared_data as store} from './localStore'

let firebaseApp = firebase.initializeApp(firebaseConfig)
let firebaseAuth = firebaseApp.auth()
let googleProvider = new firebase.auth.GoogleAuthProvider();

function handleRedirectResult(app) {

  firebaseAuth
    .getRedirectResult()
    .then((result) => {
      console.log("INSIDE REDIRECT RESULT")
      if (result.credential) {
        /** @type {firebase.auth.OAuthCredential} */
        var credential = result.credential;

        // This gives you a Google Access Token. You can use it to access the Google API.
        var token = credential.accessToken;
        var token_id = firebaseAuth.currentUser.getIdToken()
        console.log("Token: ", token_id, typeof token_id)
        // ...
        // The signed-in user info.
        var user = result.user;

        let send_user_params = {
          token_id: token_id,
          email: user.email,
          uid: user.uid,
          name: user.displayName
        }

        // send token_id to backend with the user account
        let is_valid = false

        axios.post(store.baseUrl + '/users/firebase_auth', send_user_params)
          .then(response => {
            if (response.status === 200) {
              is_valid = true
            } else {
              is_valid = false
            }
            LocalStorage.set('loggedIn', is_valid)
            app.$router.push('/').catch(err => {
              console.log(err)
            })
          }).catch(err => {
          console.log(err)
        })


      }

    }).catch((error) => {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // The email of the user's account used.
    var email = error.email;
    // The firebase.auth.AuthCredential type that was used.
    var credential = error.credential;
    // ...
  });
}


function handleAuthChanged(app) {
  firebaseAuth.onAuthStateChanged((user) => {
    console.log("Got new user on login")
    Loading.hide()
    // console.log("User: " + JSON.stringify(user))
    if (user) {
      // firebaseAuth.signOut()
      LocalStorage.set('loggedIn', true)
      app.$router.push('/').catch(err => {
        console.log(err)
      })
    } else {
      LocalStorage.set('loggedIn', false)
      app.$router.go(0).catch(err => {
        console.log(err)
      })
    }
  })
}

// function fbLogout(app){
//   firebaseAuth.signOut().catch(err => {
//     console.log(err)
//   })
//
//
// }


export { firebaseAuth, googleProvider, handleAuthChanged, handleRedirectResult}

