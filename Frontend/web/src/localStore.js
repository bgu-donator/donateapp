import axios from "axios";
import {Cookies} from "quasar";
let baseDomain = 'localhost'
const shared_data = {

  baseUrl: 'https://' + baseDomain + ':8000',
  username: localStorage.username,
  login(username) {
    this.username = JSON.stringify(username);
    localStorage.setItem("username", JSON.stringify(username));
  },
  async logout() {
    await axios.post(this.baseUrl + '/users/logout')

  },
  isLoggedIn() {
    // console.log(Cookies.getAll())
    return Cookies.get('diaCookie')
  },
  isAdmin(){
    return Cookies.get('diaAdmin')
  }
}


export {shared_data}
