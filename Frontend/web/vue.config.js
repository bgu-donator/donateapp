const fs = require('fs')

module.exports = {
  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: true
    }
  },
  transpileDependencies: [
    'quasar'
  ],
  configureWebpack: {
    devServer: {
      compress: true,
      disableHostCheck: true,   // That solved it
      https: {
        key: fs.readFileSync('../../donateitapp.com+3-key.pem'),
        cert: fs.readFileSync('../../donateitapp.com+3.pem'),
      },

    }
  }
}
