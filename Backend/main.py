import sys
import uvicorn
import os
## UVICORN = ASGI = Async Server Gateway Interface == NODEJS


if __name__ == "__main__":
    port = int(os.getenv('PORT')) if os.getenv('PORT') else 8000
    path_to_cert = "" if 'linux' in sys.platform else "../"
    uvicorn.run("server.app:app", host="0.0.0.0", port=port, reload=True,
                ssl_certfile=path_to_cert + 'donateitapp.com+3.pem',
                ssl_keyfile=path_to_cert + 'donateitapp.com+3-key.pem',
                log_level="debug")
