from pydantic import Field
from .UserClass import UserSchema


class AdminSchema(UserSchema):
    phone_number: str = Field(..., min_length=9)

    class Config:
        schema_extra = {
            "example": {
                "name": "Admin Admin",
                "email": "admin@gmail.com",
                "password": "123456",
                "username": "admin",
                "type": "ADMIN",
                "phone_number": "0501234567"
            }
        }