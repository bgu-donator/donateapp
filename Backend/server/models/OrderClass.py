from typing import Optional
from pydantic import BaseModel, Field
from datetime import datetime


class OrderSchema(BaseModel):
    product_id: str = Field(...)
    date: datetime = Field(...)
    buyer_email: str = Field(...)
    seller_email: str = Field(...)
    association_username: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "product_id": "5ff4bd20c4747df45dde8468",
                "date": "2019-06-01 12:22",
                "buyer_email": "tairc",
                "seller_email": "galgal",
                "association_username": "Or4Family",
            }
        }
