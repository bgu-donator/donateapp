from enum import Enum
from typing import Optional, List
from pydantic import BaseModel, EmailStr, Field
from .ProductClass import ProductSchema
from .OrderClass import OrderSchema


class LoginSchema(BaseModel):
    email: str = Field(...)
    password: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "email": "johnDoe@gmail.com",
                "password": "123456",
            }
        }


class TokenSchema(BaseModel):
    a: Optional[str] = ''
    b: Optional[str] = ''
    c: Optional[str] = ''
    d: Optional[str] = ''
    e: Optional[str] = ''
    f: Optional[str] = ''
    g: Optional[str] = ''
    h: Optional[str] = ''
    i: Optional[str] = ''


class UserType(Enum):
    USER = 1
    ASSOCIATION = 2
    ADMIN = 3


class FirebaseUserSchema(BaseModel):
    token_id: TokenSchema = Field(...)
    email: EmailStr = Field(...)
    uid: str = Field(...)
    name: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "email": "johnDoe@gmail.com",
                "uid": "ads87f97f9sdaf89",
                "token_id": "Azx324XC5sdfa",
                "name": "john doe"
            }
        }


class UserSchema(BaseModel):
    name: str = Field(...)
    username: str = Field(..., min_length=3, max_length=8)
    password: str = Field(..., min_length=6, max_length=20)
    email: EmailStr = Field(...)
    orders: Optional[List[OrderSchema]] = []
    products_for_sale: Optional[List[str]] = []
    total_donation: Optional[int] = 0
    is_admin: Optional[bool] = False
    salt: Optional[str] = ''

    class Config:
        schema_extra = {
            "example": {
                "name": "John Doe",
                "email": "shtut@gmail.com",
                "password": "123456",
                "username": "john_doe",
            }
        }

    # def __repr__(self):
    #     str = "{ " \
    #           "name: " + self.name + "\n" \
    #           "username: " + self.username + "\n" \
    #           "password: " + self.password + "\n" \
    #           "email: " + self.email + "\n}"
    #
    #     return str


class UpdateStudentModel(BaseModel):
    name: Optional[str]
    email: Optional[EmailStr]
    username: Optional[str]
    password: Optional[str]

    class Config:
        schema_extra = {
            "example": {
                "name": "John Doe",
                "email": "shtut@gmail.com",
                "password": "123456",
                "username": "john_doe"
            }
        }


class ChangePasswordSchema(BaseModel):
    current_password: str
    new_password: str
    confirm_password: str

    class Config:
        schema_extra = {
            "example": {
                "current_password": "123456",
                "new_password": "111111",
                "confirm_password": "111111"
            }
        }


class ChangeDetailsSchema(BaseModel):
    email: str
    username: str
    firstName: str
    lastName: str

    class Config:
        schema_extra = {
            "example": {
                "email": "test@gmail.com",
                "username": "user",
                "firstName": "user",
                "lastName": "user"
            }
        }


def ResponseModel(data, message, code=200):
    return {
        "data": [data],
        "code": code,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}
