from typing import Optional
from pydantic import BaseModel, Field


class ProductSchema(BaseModel):
    product_name: str = Field(...)
    description: str = Field(...)
    price: float = Field(...)
    main_category: str = Field(...)
    city: str = Field(...)
    phone_number: str = Field(...)
    seller_email: str = Field(...)
    sub_category: Optional[list] = []
    image: Optional[list] = []
    status: Optional[str] = "FOR_SALE"

    class Config:
        schema_extra = {
            "example": {
                "product_name": "Computer",
                "description": "Dell",
                "price": "1000",
                "main_category": "Electronics",
                "city": "Tel Aviv",
                "phone_number": "0521231231",
                "seller_email": "tair@gmail.com",
                "sub_category": ["Computers"],
                "image": [
                    "https://creatixcdn.azureedge.net/fetch/pc365/w_380,h_285,mode_pad,v_5/https://www.pc365.co.il/images/1(83).gif"]
            }
        }


class ProductIdSchema(BaseModel):
    p_id: str

    class Config:
        schema_extra = {
            "example": {
                "p_id": "4617461837rbvy4"
            }
        }


class UpdateProduct(BaseModel):
    id: str
    product_name: str
    description: str
    price: float
    main_category: str
    city: str
    phone_number: str
    seller_email: str
    sub_category: list
    image: list

    class Config:
        schema_extra = {
            "example": {
                "id": '60aea3d977269ce5ee5d120c',
                "product_name": "גיטרה חשמלית",
                "description": "fender",
                "price": "200",
                "main_category": "כלי נגינה",
                "city": 'באר שבע',
                "phone_number": '(054) 111 - 1234',
                "seller_email": 'arnav12@gmail.com',
                "sub_category": ["גיטרה","כלי-מיתר"],
                "image": [
                    "https://la-bama.co.il/wp-content/uploads/2018/12/fender-player-stratocaster-mn-3-ts.jpg"]

            }
        }


class UpdateImages(BaseModel):
    id: str
    image: list

    class Config:
        schema_extra = {
            "example": {
                "id": '60aea3d977269ce5ee5d120c',
                "image": [
                    "https://la-bama.co.il/wp-content/uploads/2018/12/fender-player-stratocaster-mn-3-ts.jpg"]

            }
        }

def ResponseModel(data, message, code=200):
    return {
        "data": [data],
        "code": code,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}
