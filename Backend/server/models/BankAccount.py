from pydantic import BaseModel, EmailStr, Field


class BankAccount(BaseModel):
    branch_number: str = Field(...)
    account_number: str = Field(...)
    bank_name: str = Field(...)
    bank_number: str = Field(...)