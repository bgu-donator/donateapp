from pydantic import BaseModel, Field
from datetime import datetime


class EventSchema(BaseModel):
    date: str = Field(...)
    description: str = Field(...)
    association_name: str = Field(...)
    association_email: str = Field(...)

    class Config:
        schema_extra = {
            "event": {
                "date": "2019-06-01 12:22",
                "description": "Donation event",
                "association_name": "אור למשפחות",
                "association_email": "Or4Family@gmail.com",
            }
        }
