from typing import Optional
from pydantic import BaseModel, EmailStr, Field
from .BankAccount import BankAccount


class LoginSchema(BaseModel):
    email: str = Field(...)
    password: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "email": "johnDoe@gmail.com",
                "password": "123456",
            }
        }


class AssociationSchema(BaseModel):
    name: str = Field(...)
    username: str = Field(..., min_length=3, max_length=8)
    password: str = Field(..., min_length=6, max_length=20)
    email: EmailStr = Field(...)
    phone_number: str = Field(..., min_length=9)
    url: str = Field(...)
    bank_account: BankAccount = Field()
    description: str = Field()
    activity: str = Field()
    image: Optional[list] = []
    donations: int = Field(...)
    salt: Optional[str] = ''

    class Config:
        schema_extra = {
            "example": {
                "name": "John Doe",
                "email": "shtut@gmail.com",
                "password": "123456",
                "username": "john_doe",
                "phone_number": "0555555555",
                "url": "give@gmail.com",
                "activity": "Battered Women",
                "description": "",
                "image": [],
                "donations": 0,
                "bank_account": {
                    "branch_number": "123",
                    "account_number": "123456",
                    "bank_name": "Hapoalim",
                    "bank_number": "12"
                }
            }
        }


class ChangeAssociationDetailsSchema(BaseModel):
    email: str
    username: str
    phone_number: str
    url: str
    description: str

    class Config:
        schema_extra = {
            "example": {
                "email": "assoc@gmail.com",
                "username": "username",
                "phone_number": "0546366234",
                "url": "user",
                "description": "user"
            }
        }
