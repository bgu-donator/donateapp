import firebase_admin
from firebase_admin import credentials, auth
from os import path
import sys
import logging
logger = logging.getLogger("uvicorn.error")

module_path = path.dirname(path.abspath(__file__))
path_to_json = "/donate-it-app-admin.json" if 'linux' in sys.platform else "\\donate-it-app-admin.json"
path_to_json = module_path + path_to_json
logger.debug("Looking for: " + str(path_to_json))
# firebase_admin.initialize_app()
if path.exists(path_to_json):
    logger.info("Found Firebase JSON file")
    cred = credentials.Certificate(path_to_json)
    firebase_admin.initialize_app(cred)
else: # pragma: no cover
    logger.error("Could not Find Firebase JSON file")
    firebase_admin.initialize_app()


def validate_user_token_id(token_id): # pragma: no cover
    decoded_token = auth.verify_id_token(token_id)
    uid = decoded_token['uid']
    return uid
