import logging
import motor.motor_asyncio
from bson import ObjectId
from datetime import datetime
from pymongo import ReturnDocument
import os

from Backend.RecommendationSystem.Rules import recommendation_system_rules

MONGO_DETAILS = "mongodb://localhost:27017"
OUR_DB_NAME = "donatitapp"
USERS_COLLECTION_NAME = "Users"
ASCS_COLLECTION_NAME = "Associations"
PRODUCTS_COLLECTION_NAME = "Products"
EVENTS_COLLECTION_NAME = "Events"
# ADMINS_COLLECTION_NAME = "Admins"
logger = logging.getLogger("uvicorn.error")


class _DB:  # pragma: no cover
    """
    This Class represents the connection to MongoDB
    using localhost and port 27017
    it is establishing the connection on creation
    and getting the DB with the name `const OUR_DB_NAME`
    """
    _instance = None

    def __init__(self):
        # if DB.__instance != None:
        #     raise Exception("This class is a singleton!")
        # else:
        # DB.__instance = self
        self.client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)
        self.db = self.client[OUR_DB_NAME]
        self.users_collection = self.db[USERS_COLLECTION_NAME]
        self.asc_collection = self.db[ASCS_COLLECTION_NAME]
        self.products_collection = self.db[PRODUCTS_COLLECTION_NAME]
        # self.admins_collection = self.db[ADMINS_COLLECTION_NAME]
        self.events_collection = self.db[EVENTS_COLLECTION_NAME]

    async def find_users(self):
        """
        This function is getting the list of all users in the collection `USERS_COLLECTION_NAME`\n
        Similar to `select * from Users`
        \n
        example: [{
        \t name: John,
        \n\t email: jd@g.cm,
        \n\t password: 123456
        \n\t }]
        \n

        :return: List of all Users
        """
        logger.info("Getting all Users")
        logger.info("select * from " + USERS_COLLECTION_NAME)
        list_of_users = []
        async for doc in self.users_collection.find():
            list_of_users.append(self.user_helper(doc))
        return list_of_users

    async def find_user_by_email(self, email):
        """
        This is an Inner Function and should not be called, or returned to a client.\n
        This Function is finding user by its email\n
        :param email: the email that should be found
        :return: A UserHelper Object that represents a User
        """
        logger.info("Selecting user: " + email)
        try:
            user = await self.users_collection.find_one({"email": email})
            if user:
                logger.info("Found user")
                return self.user_helper(user)
            else:
                logger.info("There is no User: " + email)
                return None
        except Exception as e:
            logger.error(e)
            return None

    async def find_association_by_email(self, email):
        """
        This Function is finding association by its email\n
        :param email: the email that should be found
        :return: An AssociationDetailsHelper Object that represents a User
        """
        logger.info("Selecting association:" + email)
        try:
            association = await self.asc_collection.find_one({"email": email})
            if association:
                logger.info("Found association")
                return self.asc_details_helper(association)
            else:
                logger.info("There is no association" + email)
                return None
        except Exception as e:
            logger.error(e)
            return None

    async def delete_user_by_email(self, email):
        """
        This Function is finding user by its email and deletes it from the DB
        :param email: the email that should be deleted
        :return: True if the user is deleted (or not exists in the DB) False for failure
        """
        logger.info("Selecting user:" + email)
        try:
            user = await self.users_collection.find_one({"email": email})
            if user:
                logger.info("Found user")
                deleted = await self.users_collection.delete_one({'_id': user['_id']})
            else:
                logger.info("There is no User" + email)
                raise Exception('Something went wrong while deleting the user')
            # return False
            # Does not matter if the user exists before the deletion or not,
            # return True as deleted
            return True
        except Exception as e:
            logger.error(e)
            return False

    async def get_donations_for_user(self, email):
        """
        This is an Inner Function and should not be called, or returned to a client.\n
        This Function is finding the donations of user by its email\n
        :param email: the email that should be found
        :return:
        """
        logger.info("Selecting user:" + email)
        try:
            user = await self.users_collection.find_one({"email": email})
            if user:
                logger.info("Found user")
                orders = user["orders"]
                donations = []
                donation_dict = {}
                for order in orders:
                    details_of_donations = await self.find_details_of_donations_per_order(
                        order)
                    # check if this association already exist in donations
                    association_name = details_of_donations["name"]
                    if association_name in donation_dict:
                        total = donation_dict[association_name]["total"] + \
                                details_of_donations["total"]
                        details_of_donations["total"] = total
                        donation_dict[association_name] = details_of_donations
                        # donations[association_name] = total
                    else:
                        # donations.append(details_of_donations)
                        donation_dict[association_name] = details_of_donations
                for value in donation_dict.values():
                    donations.append(value)
                return donations
            else:
                logger.info("There is no User" + email)
                return None
        except Exception as e:
            logger.error(e)
            return None

    async def find_details_of_donations_per_order(self, order):
        donations = {}
        association_username = order["association_username"]
        product_id = order["product_id"]
        association = await self.find_association_by_username(association_username)
        product = await self.get_product_by_id(product_id)
        if association and product:
            donations["name"] = association["name"]
            donations["total"] = product["price"]
        return donations

    async def check_password(self, email, password):
        """
        checking if password correct
        :param email: the email to check
        :param password: the password of the user
        :return: true if the password correct, False otherwise
        """
        try:
            logger.info("check password for user" + email)
            user = await self.users_collection.find_one({"email": email})
            logger.info("Found user: " + str(user))
            return True if user['password'] == password else False
        except Exception as e:
            logger.error(e)
            return None

    async def update_password(self, email, password):
        """
        update user password
        :param email: email user
        :param password: new password
        :return: True if new password updates successfully False otherwise
        """
        try:
            query = {"email": email}
            new_values = {"$set": {"password": password}}
            logger.info("update password for user" + email)
            self.users_collection.update_one(query, new_values)
            return True
        except Exception as e:
            logger.error(e)
            return False

    async def login(self, email, password):
        """
        Logging in user
        :param email: the email to login
        :param password: the password of the user
        :return: User helper of user if the user authenticated successfully else None
        """
        try:
            logger.info("Trying to login user" + email)
            user = await self.users_collection.find_one({"email": email})
            logger.debug("Found user: " + str(user))
            return self.user_helper(user) if user['password'] == password else None
        except Exception as e:
            logger.error(e)
            return None

    async def association_login(self, email, password):
        """
        :param email: association email
        :param password: association pw
        :return: association helper if the authenticated occurred successfully else return None
        """
        try:
            logger.info("Trying to login association by email: " + email)
            association = await self.asc_collection.find_one({"email": email})
            logger.info("Found association: " + str(association))
            return self.asc_helper(association) if association['password'] == password else None
        except Exception as e:
            logger.error(e)
            return None

    async def insert_product(self, new_product: dict) -> dict:
        """
        This function is inserting a product to the db,
        :param new_product: the product to add
        :return: the inserted product
        """
        logger.info("add new product:" + str(new_product))

        new_product["publish_at"] = datetime.now()
        product = await self.products_collection.insert_one(new_product)
        new_product = await self.products_collection.find_one({"_id": product.inserted_id})
        result = self.product_helper(new_product)

        # add the product to the user products list
        seller_email = new_product["seller_email"]
        user = self.find_user_by_email(seller_email)
        if user is not None:
            await self.users_collection.find_one_and_update({"email": seller_email},
                                                            {'$push': {
                                                                "products_for_sale": product.inserted_id}})
        return result

    async def load_salt_for_user(self, email_addr, collection):
        """
        This function is loading a salt for specific user during the login,
        it is used with hash function to hash the password, so it will not be saved in clear text
        :param email_addr: the email of the user to get salt for
        :param: collection: the collection [user_collection, association_collection] to use
        :return: the salt
        """
        logger.info("Function load salt for user: " + str(email_addr))
        try:
            logger.info("Searching the DB")
            user = await collection.find_one({"email": email_addr})
            if user is None:
                return None
            salt = user['salt']
        except Exception as e:
            logger.error(e)
            raise Exception('No salt for user ' + str(email_addr) + ' try again')

        return salt

    async def get_all_products(self):
        """
        This function is getting the list of all products in the collection `PRODUCTS_COLLECTION_NAME`\n
        Similar to `select * from Products`
        :return: List of all products
        """
        logger.info("select * from " + PRODUCTS_COLLECTION_NAME)
        list_of_products = []
        async for doc in self.products_collection.find({}):
            list_of_products.append(self.product_helper(doc))
        return list_of_products

    async def get_product_by_id(self, prod_id: str):
        """
        :param prod_id: Object id
        :return: product
        """
        logger.info("Selecting product by id = " + prod_id)
        product = None
        if ObjectId.is_valid(prod_id):
            object = {"_id": ObjectId(prod_id)}
            product = await self.products_collection.find_one(object)
        if product:
            logger.info("Found product")
            return self.product_helper(product)
        else:
            logger.info("There is no prduct with id = " + str(prod_id))
            return None

    async def insert_user(self, new_user: dict) -> dict:
        """
        This function is inserting a user to the db,
        Register a new user
        :param new_user: the user to be registered
        :return: the inserted user
        """
        logger.info("registering new user:" + str(new_user))

        user = await self.users_collection.insert_one(new_user)
        inserted_user = await self.users_collection.find_one({"_id": user.inserted_id})
        result = self.user_helper(inserted_user)
        return result

    async def insert_event(self, new_event: dict) -> bool:
        """
        This function is inserting an event to the db
        :param new_event: the event to add
        :return: true if success or false if failed
        """
        logger.info("adding new event:" + str(new_event))

        logger.info("searching association by email:" + new_event["association_email"])
        association = await self.asc_collection.find_one({"email": new_event["association_email"]})
        if association:
            logger.info("Found association")
            event = await self.events_collection.insert_one(new_event)
            return True
        else:
            logger.info("There is no association by this email:" + new_event["association_email"])
            return False

    def user_helper(self, user) -> dict:
        return {
            "id": str(user["_id"]),
            "name": user["name"],
            "email": user["email"],
            "username": user["username"],
            "total_donation": user["total_donation"],
            "is_admin": user['is_admin'] if 'is_admin' in user.keys() else 'false',
            "products_for_sale": [str(obj_id) for obj_id in user['products_for_sale']]
        }

    async def get_all_events(self):
        """
        This function is getting the list of all events in the collection `EVENTS_COLLECTION_NAME`\n
        Similar to `select * from Events`
        :return: List of all events
        """
        logger.info("select * from " + EVENTS_COLLECTION_NAME)
        list_of_events = []
        async for doc in self.events_collection.find({}):
            list_of_events.append(self.event_helper(doc))
        return list_of_events

    async def find_associations(self):
        """
        This function is getting the list of all association in the collection `ASCS_COLLECTION_NAME`\n
        Similar to `select * from Associations`
        \n
        example: [{
        \t name: Or4Family,
        \n\t username: Or4Family,
        \n\t password: 123456
        \n\t }]
        \n

        :return: List of all Associations
        """
        logger.info("select * from " + ASCS_COLLECTION_NAME)
        list_of_associations = []
        async for doc in self.asc_collection.find():
            list_of_associations.append(self.asc_helper(doc))
        return list_of_associations

    async def find_association_by_username(self, username):
        """
        :param username:
        :return:
        """
        logger.info("Selecting association:" + username)
        association = await self.asc_collection.find_one({"username": username})
        if association:
            logger.info("Found association")
            return self.asc_helper(association)
        else:
            logger.info("There is no association" + username)
            return None

    async def find_events_by_association_email(self, email):
        """
        :param email: association email
        :return:
        """
        logger.info("Selecting association events by association email:" + email)
        isAss = await self.asc_collection.find_one({"email": email})
        if isAss:
            resp = self.events_collection.find({"association_email": str(email)})
            list_of_events = []
            if resp:
                logger.info("Found events")
                async for doc in resp:
                    list_of_events.append(self.event_helper(doc))
            return list_of_events
        else:
            logger.info("There is no events for this association email:" + email)
            return None

    async def find_associations_by_category(self, category):
        """
        :param category: activity
        :return:
        """
        logger.info("Selecting associations by activity:" + category)
        collection = self.asc_collection
        resp = collection.find({"activity": str(category)})
        # resp = collection.aggregate([{'$sample': {'size': amount}}])
        list_of_elemants = []
        helper = self.asc_helper
        async for doc in resp:
            list_of_elemants.append(helper(doc))
        return list_of_elemants

    async def insert_association(self, new_association: dict) -> dict:
        """
        This function is inserting a association to the db,
        Register a new association
        :param new_association: the association to be registered
        :return: the inserted association
        """
        logger.info("regitering new association:" + str(new_association))
        # TODO: change the password to be hased before saving it to the db
        association = await self.asc_collection.insert_one(new_association)
        inserted_association = await self.asc_collection.find_one({"_id": association.inserted_id})
        result = self.asc_helper(inserted_association)
        return result

    async def delete_association_by_email(self, email):
        """
        This Function is finding an association by its email and deletes it from the DB
        :param email: the email that should be deleted
        :return: True if the association is deleted (or not exists in the DB) False for failure
        """
        logger.info("Selecting user:" + email)
        try:
            asc = await self.asc_collection.find_one({"email": email})
            if asc:
                logger.info("Found association")
                deleted = await self.asc_collection.delete_one({'_id': asc['_id']})
            else:
                logger.info("There is no User" + email)
                raise Exception('Something went wrong while deleting the user')
                # return False

            # Does not matter if the user exists before the deletion or not,
            # return True as deleted
            return True
        except Exception as e:
            logger.error(e)
            return False

    async def get_random_products(self, amount: int):
        MAX_NUM_OF_PRODUCTS = 20
        if amount <= MAX_NUM_OF_PRODUCTS:
            resp = await self.get_random_k_elemants(amount, "Products")
        else:
            resp = await self.get_random_k_elemants(MAX_NUM_OF_PRODUCTS, "Products")
        return resp

    async def get_random_associations(self, amount: int):
        MAX_NUM_OF_ASSOCIATIONS = 10

        if amount <= MAX_NUM_OF_ASSOCIATIONS:
            resp = await self.get_random_k_elemants(amount, "ASC")
        else:
            resp = await self.get_random_k_elemants(MAX_NUM_OF_ASSOCIATIONS, "ASC")
        return resp

    async def get_random_events(self, amount: int):
        MAX_NUM_OF_EVENTS = 10
        if amount <= MAX_NUM_OF_EVENTS:
            resp = await self.get_random_k_elemants(amount, "EVENT")
        else:
            resp = await self.get_random_k_elemants(MAX_NUM_OF_EVENTS, "EVENT")
        return resp

    async def get_random_k_elemants(self, amount: int, collection_string):
        list_of_elements = []
        if amount <= 0:
            return list_of_elements
        if collection_string == "ASC":
            collection = self.asc_collection
            helper = self.asc_helper
            resp = collection.aggregate([{'$sample': {'size': amount}}])
        elif collection_string == "EVENT":
            collection = self.events_collection
            helper = self.event_helper
            resp = collection.aggregate([{'$sample': {'size': amount}}])
        else:
            collection = self.products_collection
            helper = self.product_helper
            resp = collection.aggregate([
                {"$match": {"status": 'FOR_SALE'}},
                {"$sample": {"size": amount}}
            ])
        # resp = collection.find().limit(amount)
        # helper = self.asc_helper if collection_string == "ASC" else self.product_helper
        async for doc in resp:
            list_of_elements.append(helper(doc))
        return list_of_elements

    def product_helper(self, product) -> dict:
        return {
            "id": str(product["_id"]),
            "product_name": product["product_name"],
            "description": product["description"],
            "price": product["price"],
            "main_category": product["main_category"],
            "sub_category": product["sub_category"],
            "image": product["image"],
            "publish_at": product["publish_at"],
            "status": product["status"],
            "city": product["city"],
            "seller_email": product["seller_email"],
            "phone_number": product["phone_number"]
        }

    def asc_helper(self, asc: dict) -> dict:
        return {
            "name": asc["name"],
            "email": asc["email"],
            "username": asc["username"],
            "phone_number": asc["phone_number"],
            "url": asc["url"],
            "image": asc["image"],
            "description": asc["description"],
            "activity": asc["activity"],
            "donations": asc["donations"],
            "donations_list": [] if not 'donations_list' in asc.keys() else asc["donations_list"]
        }

    def asc_details_helper(self, asc) -> dict:
        return {
            "name": asc["name"],
            "email": asc["email"],
            "username": asc["username"],
            "phone_number": asc["phone_number"],
            "url": asc["url"],
            "description": asc["description"],
            "activity": asc["activity"],
            "donations": asc["donations"],
            "bank_account": asc["bank_account"],
            "image": asc["image"],
            "branch_number": asc["bank_account"]["branch_number"],
            "account_number": asc["bank_account"]["account_number"],
            "bank_name": asc["bank_account"]["bank_name"],
            "bank_number": asc["bank_account"]["bank_number"]
        }

    def order_helper(self, order) -> dict:
        return {
            "product_id": order["product_id"],
            "date": order["date"],
            "buyer_email": order["buyer_email"],
            "seller_email": order["seller_email"],
            "association_username": order["association_username"]
        }

    def event_helper(self, event) -> dict:
        return {
            "date": event["date"],
            "description": event["description"],
            "association_name": event["association_name"],
            "association_email": event["association_email"]
        }

    def admin_helper(self, admin) -> dict:
        return {
            "name": admin["name"],
            "id": str(admin["_id"]),
            "email": admin["email"],
            "password": admin["password"],
            "username": admin["username"],
            # "type": "ADMIN",
            # "phone_number": admin["phone_number"]
        }

    async def find_admins(self):
        """
        This function is getting the list of all users in the collection `USERS_COLLECTION_NAME`\n
        Similar to `select * from Users`
        \n
         "example": {
                "name": "Admin Admin",
                "email": "admin@gmail.com",
                "password": "123456",
                "username": "admin",
                "type": "ADMIN",
                "phone_number": "0501234567"
                }

        :return: List of all admins
        """
        logger.info("Getting all admins")
        logger.info("select * from " + USERS_COLLECTION_NAME)
        list_of_admins = []
        async for doc in self.users_collection.find():
            if 'is_admin' in doc.keys() and doc['is_admin']:
                list_of_admins.append(self.admin_helper(doc))
        return list_of_admins

    async def delete_product_by_id(self, product_id: str) -> dict:
        """
        This function is getting a product id and deletes it from the DB
        :param product_id: the product id to be deleted
        :return: boolean for success
        """
        try:
            logger.info("Selecting product by id = " + product_id)
            product = await self.products_collection.find_one_and_update({"_id": ObjectId(product_id)},
                                                                         {"$set": {
                                                                             "status": "DELETED"}},
                                                                         upsert=True)
            logger.info("Deleted product")
            return {
                'product': product_id,
                'deleted': True
            }
        except Exception as e:
            logger.error(e)
            logger.info("Could not delete product with id = ", str(product_id))
            return {
                'product': product_id,
                'deleted': False
            }
        #     seller = await self.products_collection.find_one({'_id': ObjectId(product_id)})
        #     seller_email = seller['seller_email']
        #     await self.products_collection.delete_one({"_id": ObjectId(product_id)})
        #     await self.delete_product_from_seller(seller_email, product_id)
        #     logger.info("Deleted product")
        #     return {
        #         'prodcut': product_id,
        #         'deleted': True
        #     }
        # except Exception as e:
        #     logger.error(e)
        #     logger.info("Could not delete product with id = ", str(product_id))
        #     return {
        #         'prodcut': product_id,
        #         'deleted': False
        #     }

    async def update_sold(self, product_id):
        try:
            logger.info("update product as sold id = " + product_id)
            product = await self.products_collection.find_one_and_update({"_id": ObjectId(product_id)},
                                                                         {"$set": {
                                                                             "status": "SOLD"}},
                                                                         upsert=True)
            return True
        except Exception as e:
            logger.error(e)
            logger.info("Could not update product with id = ", str(product_id))
            return False

    async def update_for_sale(self, product_id):
        try:
            logger.info("update product as for sale id = " + product_id)
            product = await self.products_collection.find_one_and_update({"_id": ObjectId(product_id)},
                                                                         {"$set": {
                                                                             "status": "FOR_SALE"}},
                                                                         upsert=True)
            return True
        except Exception as e:
            logger.error(e)
            logger.info("Could not update product with id = ", str(product_id))
            return False

    def donation_helper(self, seller_from_db, buyer_from_db, product, association) -> dict:
        return {
            "product_price": product["price"],
            "seller_username": seller_from_db["username"],
            "buyer_username": buyer_from_db["username"],
            "association_username": association["username"]
        }

    async def insert_order(self, new_order: dict, seller_from_db, buyer_from_db, product, association):
        """
        This function is inserting a order to the db,
        Register a new order
        :param association:
        :param product:
        :param buyer_from_db:
        :param seller_from_db:
        :param new_order: the order to be created
        :return: the inserted order
        """
        logger.info("creating new order:" + str(new_order))

        buyer_email = new_order["buyer_email"]
        seller_email = new_order["seller_email"]

        order_details = {
            'seller': seller_email,
            'buyer': buyer_email,
            'price': product['price'],
            'p_name': product['product_name'],
            'association': association['username']
        }
        # update total donation to buyer
        buyer_total_donation = buyer_from_db["total_donation"] + product["price"]

        # find the buyer email in the users_collection
        # UPDATE a new order to his orders list, and his total_donation
        buyer_user = await self.users_collection.find_one_and_update({"email": buyer_email},
                                                                     {'$push': {"orders": new_order},
                                                                      '$set': {"total_donation": buyer_total_donation}},
                                                                     return_document=ReturnDocument.AFTER)

        # change the product id of the order to be as stuatus "SOLD"
        product = await self.products_collection.find_one_and_update({"_id": ObjectId(new_order["product_id"])},
                                                                     {"$set": {
                                                                         "status": "SOLD"}},
                                                                     upsert=True)

        # update total donation seller , buyer
        seller_total_donation = seller_from_db["total_donation"] + product["price"]
        seller_user = await self.users_collection.find_one_and_update({"email": seller_email},
                                                                      {'$set': {
                                                                          "total_donation": seller_total_donation}})
        updated_list = association['donations_list']
        updated_list.append(order_details)
        asc_donate = association["donations"] + product["price"]
        association_donations = await self.asc_collection.find_one_and_update({"email": association["email"]},
                                                                              {"$set": {
                                                                                  "donations": asc_donate,
                                                                                  "donations_list": updated_list
                                                                              }})
        if len(buyer_user["orders"]) > 0:
            order = self.order_helper(buyer_user["orders"][-1])
        else:
            order = self.order_helper(buyer_user["orders"][0])
        return order

    async def search_products(self, main_category: str, sub_category: str,
                              min_price: float, max_price: float, city: str):
        logger.info("select * from " + PRODUCTS_COLLECTION_NAME)
        list_of_products = []
        query = {}
        if main_category != 'null':
            query["main_category"] = main_category
        subs = sub_category.split(sep=",")
        if city != 'null':
            query["city"] = city
        query["price"] = {'$gte': min_price, '$lte': max_price}
        if sub_category != 'null':
            # query["sub_category"] = {'$all': subs}
            query["sub_category"] = {'$in': subs}
        logger.info("Searching for query: " + str(query))
        async for product in self.products_collection.find(query):
            list_of_products.append(self.product_helper(product))
        logger.info("Found " + str(len(list_of_products)) + " products: " + str(list_of_products))
        list_of_products_only_for_sale = []
        for product in list_of_products:
            if product['status'] == 'FOR_SALE':
                list_of_products_only_for_sale.append(product)
        return list_of_products_only_for_sale

    async def recommendation_products(self, main_category: str):
        list_of_products = []
        query = {}
        if main_category != 'null':
            main = recommendation_system_rules[main_category]
            query['main_category'] = {'$in': main}
        logger.info("Searching for query: " + str(query))
        async for product in self.products_collection.find(query):
            if product['status'] == 'FOR_SALE':
                list_of_products.append(self.product_helper(product))
        if len(list_of_products) == 0:
            list_of_products = await self.get_random_k_elemants(6, "Products")
            for product in list_of_products:
                if product['status'] != 'FOR_SALE':
                    list_of_products.remove(product)
        logger.info("Found " + str(len(list_of_products)) + " products: " + str(list_of_products))
        return list_of_products[:6]

    async def delete_product_from_seller(self, seller_email, product_id):
        """
        Thif function is deleting the product from the seller after deleting the product
        :param seller_email:
        :param product_id:
        :return:
        """
        logger.info("Find seller")
        user_doc = await self.users_collection.find_one({'email': seller_email})
        logger.debug("Seller =", user_doc)
        prod_array = user_doc['products_for_sale']
        logger.info("Old products array = " + str([str(pid) for pid in prod_array]))
        new_arr = [prod_id for prod_id in prod_array if str(prod_id) != product_id]
        logger.info("New products array =" + str(new_arr))
        await self.users_collection.find_one_and_update({'email': seller_email},
                                                        {'$set': {
                                                            'products_for_sale': new_arr
                                                        }})
        logger.info("The new array was set successfully")

    async def set_admin(self, user_email: str, is_admin: bool) -> bool:
        """
        This function is getting an email and a boolean value whether to set a admin or not
        :param is_admin: boolean value to set the user as admin
        :param user_email: the user to be set as admin
        :return: Boolean that indicates success
        """
        try:
            logger.info("DB: Set admin for " + str(user_email) + " as " + str(is_admin))
            admin_user = await self.users_collection.find_one_and_update({"email": user_email},
                                                                         {'$set': {
                                                                             "is_admin": is_admin}})
            if admin_user is None:
                return False

            return True
        except Exception as e:
            logger.error(e)
            return False

    # async def check_username(self, email:str, username:str):
    async def update_product_details(self, id: str, product_name: str,
                                     description: str, price: float,
                                     main_category: str, city: str,
                                     phone_number: str, sub_category
                                     ):
        """
        update product details
        :param id:
        :param product_name:
        :param description:
        :param price:
        :param main_category:
        :param city:
        :param phone_number:
        :param sub_category:
        :return: True if updated successfully, false otherwise
        """
        try:
            logger.info("DB: update details for " + str(id))
            result = await self.products_collection.find_one_and_update({"_id": ObjectId(id)},
                                                            {'$set': {
                                                                "product_name": product_name,
                                                                "description": description,
                                                                "price": price,
                                                                "main_category": main_category,
                                                                "city": city,
                                                                "phone_number": phone_number,
                                                                "sub_category": sub_category
                                                            }})


            # product = await self.products_collection.find_one_and_update({"_id": ObjectId(product_id)},
            #                                                              {"$set": {
            #                                                                  "status": "SOLD"}},
            #                                                              upsert=True)
            logger.info("DB: updated details for " + str(id))
            return True
        except Exception as e:
            logger.error(e)
            return False


    async def update_product_imgs(self, id: str, images):
        """
        update product images  with the given id
        :param id: product
        :param images: Array of images-url
        :return: True if updated successfully, false otherwise
        """
        try:
            logger.info("update images for " + str(id))
            result = await self.products_collection.find_one_and_update({"_id": ObjectId(id)},
                                                            {'$set': {
                                                                "image": images
                                                            }})
            return True
        except Exception as e:
            logger.error(e)
            return False


    async def update_user_details(self, email: str, username: str,
                                  first_name: str, last_name: str):
        """
        update user details with the given email
        :param email: user email
        :param username: user username to be updated
        :param first_name: user first name to be updated
        :param last_name: user last name to be updated
        :return: True if updated successfully, false otherwise
        """
        try:
            logger.info("DB: update details for " + str(email))
            await self.users_collection.find_one_and_update({"email": email},
                                                            {'$set': {
                                                                "username": username,
                                                                "name": first_name + " " + last_name
                                                            }})
            logger.info("DB: updated details for " + str(email))
            return True
        except Exception as e:
            logger.error(e)
            return False

    async def update_association_details(self, email: str, username: str, url: str, phone_number: str,
                                         description: str):
        try:
            logger.info("DB: update details for association: " + str(email))
            await self.asc_collection.find_one_and_update({"email": email},
                                                          {'$set': {
                                                              "url": url,
                                                              "username": username,
                                                              "phone_number": phone_number,
                                                              "description": description,
                                                          }})
            logger.info("DB: updated details for association: " + str(email))
            return True
        except Exception as e:
            logger.error(e)
            return False


def DB(username=None, password=None, url=None):  # pragma: no cover
    global MONGO_DETAILS
    if _DB._instance is None:
        if username and password and url:
            logger.info("Logging ing to the server " + str(url))
            MONGO_DETAILS = "mongodb+srv://" + str(username) + ":" + str(password) + "@" + str(url) + "/"

        logged_in_server = str(url) if url else "localhost"
        logger.info("Logged in to: " + logged_in_server)
        _DB._instance = _DB()
    return _DB._instance
