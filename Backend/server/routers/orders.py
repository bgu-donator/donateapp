from fastapi import APIRouter, status, Response
import logging
from Backend.server.database import DB
from fastapi import APIRouter, Body
from fastapi.encoders import jsonable_encoder

from Backend.server.models.OrderClass import (
    OrderSchema,
)

from Backend.server.models.ProductClass import (
    ProductSchema,
    ErrorResponseModel,
    ResponseModel,
)

db = DB()
logger = logging.getLogger('uvicorn.error')

router = APIRouter(
    prefix="/orders",
    tags=["orders"],
    responses={404: {"description": "Not found"}},
)


@router.post("/create_order")
async def create_order(order: OrderSchema, response: Response):
    """
    This Function is creating a new order
    :param order: the json object representing a new order
    :return: the order inserted
    """
    logger.info("Creating a new order")
    seller = order.seller_email
    buyer = order.buyer_email

    seller_from_db = await db.find_user_by_email(seller)
    buyer_from_db = await db.find_user_by_email(buyer)

    # Checking if the user seller exist
    if seller_from_db is None:
        seller_not_exist = 'Seller email is not exists, could not create order'
        logger.error(seller_not_exist)
        response.status_code = status.HTTP_409_CONFLICT
        return ErrorResponseModel("An error occurred.", status.HTTP_409_CONFLICT,
                                  seller_not_exist)
    logger.info("There is seller with the given username")

    # Checking if the user buyer exist
    if buyer_from_db is None:
        buyer_not_exist = 'Buyer email is not exists, could not create order'
        logger.error(buyer_not_exist)
        response.status_code = status.HTTP_409_CONFLICT
        return ErrorResponseModel("An error occurred.", status.HTTP_409_CONFLICT,
                                  buyer_not_exist)
    logger.info("There is buyer with the given username")

    # Checking if product exists in the db
    product = await db.get_product_by_id(order.product_id)
    if product is None:
        product_not_exist = 'product id is not exists, could not create order'
        logger.error(product_not_exist)
        response.status_code = status.HTTP_409_CONFLICT
        return ErrorResponseModel("An error occurred.", status.HTTP_409_CONFLICT,
                                  product_not_exist)
    logger.info("There is product with the given id")

    # Checking if product status is FOR-SALE
    if product["status"] == "SOLD":
        product_already_sold = 'product id is already sold, could not create order'
        logger.error(product_already_sold)
        response.status_code = status.HTTP_409_CONFLICT
        return ErrorResponseModel("An error occurred.", status.HTTP_409_CONFLICT,
                                  product_already_sold)
    logger.info("The product is FOR-SALE")

    # Checking if the association username exist
    association = await db.find_association_by_username(order.association_username)
    if association is None:
        association_not_exist = 'association id is not exists, could not create order'
        logger.error(association_not_exist)
        response.status_code = status.HTTP_409_CONFLICT
        return ErrorResponseModel("An error occurred.", status.HTTP_409_CONFLICT,
                                  association_not_exist)
    logger.info("There is association with the given username")

    try:
        order = jsonable_encoder(order)
        inserted_order = await db.insert_order(order, seller_from_db, buyer_from_db, product, association)
        logger.info("Inserted order successfully")
        response.status_code = status.HTTP_201_CREATED
        return ResponseModel(inserted_order, "Order added successfully", code=201)
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong could not create order')
