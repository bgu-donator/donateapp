from fastapi import APIRouter, Body, status, Response, Request
from Backend.server.database import DB
import logging
from fastapi.encoders import jsonable_encoder

from .routes_config import decrypt_string, ADMIN_COOKIE, hash_password, generate_salt, NO_COOKIE_FOUND, \
    SOMETHING_WENT_WRONG
from Backend.server.models.AdminClass import (
    AdminSchema
)
from Backend.server.models.AssociationClass import AssociationSchema


from Backend.server.models.UserClass import ErrorResponseModel, ResponseModel

USERNAME_IS_NOT_EXISTS = 'Username is not exists'

SET_ADMIN_KEY = 'Users set as admin'
NOT_EXISTS = 'Email does not exists'
ADDED_SUCCESSFULLY = "Association added successfully"
ALREADY_EXISTS = 'Username already exists could not register'

db = DB()
logger = logging.getLogger('uvicorn.error')
router = APIRouter(
    prefix="/admin",
    tags=["admin"],
    responses={404: {"description": "Not found"}},
)


def is_user_admin(req, response):
    try:
        user_email_from_cookie = req.cookies[ADMIN_COOKIE]
        logger.info("fetching encrypted email from cookie " + str(user_email_from_cookie))
    except Exception as e:
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return False, {'message': NO_COOKIE_FOUND}

    logger.info("got encrypted email from cookie " + str(user_email_from_cookie))
    logger.info("decrypting email from cookie")
    user_email = decrypt_string(user_email_from_cookie.encode())
    if user_email is None:
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return False, {'message': SOMETHING_WENT_WRONG}
    logger.info("decrypted email from cookie " + str(user_email))
    return True, {'email': user_email}


@router.get("/", response_description="Retrieving all admins")
async def get_all_admins(response: Response, req: Request):
    """
    get all admins in system
    need authentication
    only admin can get all admins!

    """
    logger.info('Getting all users and admin')
    logger.info('Validate user is admin')
    try:
        is_admin, result = is_user_admin(req, response)
        # is_admin = True
        if is_admin:
            admins = await db.find_admins()
            users = await db.find_users()
            response.status_code = status.HTTP_200_OK
            return {
                'Admins': admins,
                'Users': users
            }
        else:
            logger.info('Wrong Cookie' + str(result))
            response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                      result)
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  SOMETHING_WENT_WRONG)


@router.post("/set_admin")
async def set_admin(response: Response, users_dict = Body(...)):
    """
    This function is getting an array of users
    and set them as admins
    :param response: the response param of the request
    :param users_dict: dictionary contains user emails as keys and boolean that represents set or unset as values
    :return: 202 with dictionary of {email (str) => is_changed (boolean)}
    """
    logger.info("Users to set as admin: " + str(users_dict))
    users_to_admin_dict = {}
    logger.info("Setting new Admins")
    for user in users_dict:
        logger.info("Setting " + str(user) + " as Admin")
        succeed = await db.set_admin(user, users_dict[user])
        users_to_admin_dict[user] = succeed

    response.status_code = status.HTTP_202_ACCEPTED
    logger.info("Users dictionary of changed values" + str(users_to_admin_dict))
    return {SET_ADMIN_KEY: users_to_admin_dict}


# @router.post("/register")
# async def register_admin(admin: AdminSchema = Body(...)):
#     """"
#     register new admin and add it to db
#     need authentication
#     only admin can add new admin!
#     """
#     logger.info("Registering a new admin")
#     username = admin.username
#     admin_pot = await db.find_user_by_username(username)
#     logger.info("Got user " + str(admin_pot))
#     if admin_pot is not None:
#         return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
#                                   'Username already exists could not register')
#     logger.info("There is no user with the given username")
#     try:
#         admin = jsonable_encoder(admin)
#         inserted_user = await db.insert_user(admin)
#         logger.info("Inserted Admin successfully")
#         return ResponseModel(inserted_user, "Admin added successfully", code=201)
#     except Exception as e:
#         logger.error(e)
#         return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
#                                   'Something went wrong could not register')


@router.get("/{admin_email}", response_description="Retrieving admin by email")
async def get_admin(admin_email: str, req: Request, response: Response):
    """
    :param req:
    :param response:
    :param admin_email: is the string of the username
    return admin if exists
    """
    logger.info("Finding the admin with the given email")
    logger.info('Validate user is admin')
    try:
        is_admin, result = is_user_admin(req, response)
        if is_admin:
            resp = await db.find_user_by_email(admin_email)
            if resp:
                if resp['is_admin']:
                    response.status_code = status.HTTP_200_OK
                    return {'Admin': resp}
        else:
            logger.error('Wrong Cookie ' + str(result))
            response.status_code = status.HTTP_404_NOT_FOUND
            return ErrorResponseModel("An error occurred.", status.HTTP_404_NOT_FOUND,
                                      result)
    except Exception as e: # pragma: no cover
        logger.info("There is no admin with the given email")
        response.status_code = status.HTTP_404_NOT_FOUND
        return ErrorResponseModel("An error occurred.", status.HTTP_404_NOT_FOUND,
                                  NOT_EXISTS)


@router.delete("/delete/association/{asc_email}", response_description="remove association from system")
async def delete_association(asc_email: str, response: Response, req: Request):
    """
    This function is deleting an Association
    its get the email of the association as parameter,
    and validates that the user who requested this delete is an Admin by decrypting its email
    from the Cookie
    :param req: the Request parameter
    :param response: the response parameter
    :param asc_email: is the string of the username to remove
    """
    logger.info('Asking to Delete Association' + str(asc_email))
    logger.info('Validate user is admin')
    try:
        is_admin, result = is_user_admin(req, response)
        if is_admin:
            logger.info('User is admin')
            resp = await db.delete_association_by_email(asc_email)
            if resp:
                logger.info('Association deleted')
                response.status_code = status.HTTP_200_OK
                return 'deleted'
            else:
                logger.error('Failed to delete Association')
                response.status_code = status.HTTP_404_NOT_FOUND
                return 'Association not found'
        else:
            response.status_code = status.HTTP_404_NOT_FOUND
            return result
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_404_NOT_FOUND
        return ErrorResponseModel("An error occurred.", status.HTTP_404_NOT_FOUND,
                                  USERNAME_IS_NOT_EXISTS)


@router.post("/register_association")
async def register_association(association: AssociationSchema, req: Request, response: Response):
    """
    Register new Association
    :param req:
    :param association: AssociationSchema
    :param response: Response
    :return: 201 if successfully registered, 409 or 500 otherwise
    """
    logger.info("Generate new salt")
    salt = generate_salt()
    try:
        is_admin, result = is_user_admin(req, response)
        if not is_admin:
            return result
        logger.info("Registering a new association")
        username = association.username
        associations = await db.find_associations()
        logger.info("Got all associations " + str(associations))
        for a in associations:
            if a['username'] == username:
                response.status_code = status.HTTP_409_CONFLICT
                return ErrorResponseModel("An error occurred.", status.HTTP_409_CONFLICT,
                                          ALREADY_EXISTS)
        logger.info("There is no association with the given username")
        try:
            hashed_password = hash_password(association.password, salt)
            association.password = hashed_password
            association.salt = salt
            association = jsonable_encoder(association)
            inserted_association = await db.insert_association(association)
            logger.info("Inserted association successfully")
            response.status_code = status.HTTP_201_CREATED
            return ResponseModel(inserted_association, ADDED_SUCCESSFULLY, code=201)
        except Exception as e: # pragma: no cover
            logger.error(e)
            response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                      'Something went wrong could not register')
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong could not register')



@router.delete("/delete/{email}")
async def delete_user_from_db(email: str, req: Request, response: Response):
    """
    This function delete the user by the given email address
    :param req:
    :param email:
    :param response:
    :return:
    """
    try:
        logger.info("Deleting user " + email)
        is_admin, result = is_user_admin(req, response)
        if not is_admin:
            return result
        is_deleted = await db.delete_user_by_email(email)
        if not is_deleted:
            raise Exception('Something went wrong while deleting the user')
        response.status_code = status.HTTP_200_OK
        return 'deleted'
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  e)



