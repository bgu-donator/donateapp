
from fastapi import APIRouter, status, Response, Request
from Backend.server.database import DB
import logging
from fastapi.encoders import jsonable_encoder
import random
from .routes_config import COOKIE_NAME, decrypt_string, NO_ID_BELONGS_TO_THIS_USER, NO_PRODUCT_WITH_THIS_ID, SOMETHING_WENT_WRONG, NO_COOKIE_FOUND, ADDED_SUCCESSFULLY, PRODUCT_DOES_NOT_EXISTS

from Backend.server.models.ProductClass import (
    ProductSchema,
    ErrorResponseModel,
    ResponseModel,
    ProductIdSchema,
    UpdateProduct,
    UpdateImages
)

MUST_BE_GREATER_THEN_ZERO = 'Amount of random products must be greater then 0'

db = DB()
logger = logging.getLogger('uvicorn.error')

router = APIRouter(
    prefix="/product",
    tags=["product"],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_description="Retrieving all products")
async def get_all_products(res: Response):
    try:
        response = await db.get_all_products()
        res.status_code = status.HTTP_200_OK
        return {"Products": response}
    except Exception as e: # pragma: no cover
        logger.error(e)
        res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong')


@router.post("/add_product")
async def add_product(product: ProductSchema, response: Response):
    logger.info("Add a new product")
    try:
        product = jsonable_encoder(product)
        inserted_product = await db.insert_product(product)
        logger.info("Inserted product successfully")
        response.status_code = status.HTTP_201_CREATED
        return ResponseModel(inserted_product, ADDED_SUCCESSFULLY, code=201)
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong could not add product to db')


@router.get("/{product_id}", response_description="Retrieving product with this specific id")
async def get_product_by_id(product_id: str, res: Response):
    try:
        response = await db.get_product_by_id(product_id)
        if response is None:
            res.status_code = status.HTTP_404_NOT_FOUND
            return {'message': PRODUCT_DOES_NOT_EXISTS}
        res.status_code = status.HTTP_200_OK
        return {"Products": response}
    except Exception as e: # pragma: no cover
        logger.error(e)
        res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong')


@router.delete("/delete/{p_id}", response_description="Deleting product")
async def delete_product_by_id(p_id: str, req: Request, res: Response):
    try:
        user_email_from_cookie = req.cookies[COOKIE_NAME]
        logger.info("fetching encrypted email from cookie " +
                    str(user_email_from_cookie))
        if not user_email_from_cookie:
            res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {'message': NO_COOKIE_FOUND}

        logger.info("got encrypted email from cookie " +
                    str(user_email_from_cookie))
        logger.info("decrypting email from cookie")
        user_email = decrypt_string(user_email_from_cookie.encode())
        if user_email is None:
            res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {'message': SOMETHING_WENT_WRONG}
        logger.info("decrypted email from cookie " + str(user_email))
        logger.info("fetching product from db with id" + str(p_id))
        product = await db.get_product_by_id(p_id)
        if product is None:
            logger.error("No Product with id:" + str(p_id))
            res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {'message': NO_PRODUCT_WITH_THIS_ID}
        logger.info("got product from db with id", product)
        logger.info("Validating product belongs to user")
        if product['seller_email'] != user_email:
            logger.error("Product does not belong to user")
            res.status_code = status.HTTP_403_FORBIDDEN
            return {'message': NO_ID_BELONGS_TO_THIS_USER}

        logger.info("product belongs to user")
        delete_res = await db.delete_product_by_id(p_id)
        res.status_code = status.HTTP_202_ACCEPTED
        return delete_res
    except Exception as e: # pragma: no cover
        logger.error(e)
        res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  SOMETHING_WENT_WRONG)


@router.get("/get_random_products/{amount}", response_description="Retrieving X random product")
async def get_random_products(amount: int, res: Response):
    try:
        if amount < 1:
            res.status_code = status.HTTP_406_NOT_ACCEPTABLE
            return MUST_BE_GREATER_THEN_ZERO
        resp = await db.get_random_products(amount)
        res.status_code = status.HTTP_200_OK
        return {'Product': resp}
    except Exception as e: # pragma: no cover
        logger.error(e)
        res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong')


@router.get("/search_products/{main_category}/{sub_category}/{min_price}/{max_price}/{city}", response_description="Search products by main & sub category")
async def search_products(main_category: str, sub_category: str, min_price: float, max_price: float, city: str,
                          res: Response):
    try:
        logger.info("Searching for product with params:")
        logger.info("Main category:" + str(main_category))
        logger.info("Sub category:" + str(sub_category))
        logger.info("Min price:" + str(min_price))
        logger.info("Max price:" + str(max_price))
        logger.info("City:" + str(city))
        resp = await db.search_products(main_category, sub_category, min_price, max_price, city)
        if len(resp) > 0:
            res.status_code = status.HTTP_200_OK
            return {'Product': resp}
        else:
            # res.status_code = status.HTTP_204_NO_CONTENT
            # return {'Product': resp}
            return Response(status_code=status.HTTP_204_NO_CONTENT)
    except Exception as e: # pragma: no cover
        logger.error(e)
        res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong')


@router.post("/update-sold", response_description='Retrieving product with this specific id and update as sold')
async def update_sold(ProductId: ProductIdSchema, res: Response):
    try:
        logger.info("update product as sold " + str(ProductId.p_id))
        resp = await db.update_sold(ProductId.p_id)
        if resp:
            logger.info("updated product as sold " + str(ProductId.p_id))
            res.status_code = status.HTTP_200_OK
            return {'Product': resp}
        else:
            logger.error("updated product as sold Fail" + str(ProductId.p_id))
            res.status_code = status.HTTP_409_CONFLICT
            return {'Product': resp}
    except Exception as e: # pragma: no cover
        logger.error(e)
        res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong')


@router.post("/update-for-sale", response_description='Retrieving product with this specific id and update as for sale')
async def update_sold(ProductId: ProductIdSchema, res: Response):
    try:
        logger.info("update product as for-sale " + str(ProductId.p_id))
        resp = await db.update_for_sale(ProductId.p_id)
        if resp:
            logger.info("updated product as for-sale " + str(ProductId.p_id))
            res.status_code = status.HTTP_200_OK
            return {'Product': resp}
        else:
            logger.error("updated product as for-sale Fail" + str(ProductId.p_id))
            res.status_code = status.HTTP_409_CONFLICT
            return {'Product': resp}
    except Exception as e: # pragma: no cover
        logger.error(e)
        res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                              'Something went wrong')


@router.patch("/update", response_description="update product details")
async def change_product_details(
        change_details: UpdateProduct, req: Request, res: Response
):
    try:
        user_email_from_cookie = req.cookies[COOKIE_NAME]
        logger.info("fetching encrypted email from cookie " + str(user_email_from_cookie))
        if not user_email_from_cookie:
            res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {'message': NO_COOKIE_FOUND}
        logger.info("got encrypted email from cookie " + str(user_email_from_cookie))
        logger.info("try decrypting email from cookie")
        user_email = decrypt_string(user_email_from_cookie.encode())
        email = await db.find_user_by_email(user_email)
        if email is None:
            res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {'message': SOMETHING_WENT_WRONG}
        logger.info("decrypted email from cookie " + str(user_email))
        logger.info("check email match with cookie")
        logger.info("update product details for: " + str(user_email))
        updated = await db.update_product_details(change_details.id,change_details.product_name,
                                                  change_details.description, change_details.price,
                                                  change_details.main_category,
                                                  change_details.city, change_details.phone_number,
                                                  change_details.sub_category)
        if updated:
            logger.info("updated success product for: " + str(user_email))
            res.status_code = status.HTTP_202_ACCEPTED
            return 'updated'
        else:
            logger.info("update product unsuccessful for: " + str(user_email))
            res.status_code = status.HTTP_451_UNAVAILABLE_FOR_LEGAL_REASONS
            return


    except Exception as e: # pragma: no cover
        logger.error(e)
        res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  SOMETHING_WENT_WRONG)


@router.patch("/update-imgs", response_description="update imgs to product")
async def update_imgs(
        update_imgs: UpdateImages, req: Request, res: Response
):
    try:
        user_email_from_cookie = req.cookies[COOKIE_NAME]
        logger.info("fetching encrypted email from cookie " + str(user_email_from_cookie))
        if not user_email_from_cookie:
            res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {'message': NO_COOKIE_FOUND}
        logger.info("got encrypted email from cookie " + str(user_email_from_cookie))
        logger.info("try decrypting email from cookie")
        user_email = decrypt_string(user_email_from_cookie.encode())
        email = await db.find_user_by_email(user_email)
        if email is None:
            res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {'message': SOMETHING_WENT_WRONG}
        logger.info("decrypted email from cookie " + str(user_email))
        logger.info("check email match with cookie")
        logger.info("update product details for: " + str(user_email))
        updated = await db.update_product_imgs(update_imgs.id, update_imgs.image)

        if updated:
            logger.info("updated success img product for: " + str(user_email))
            res.status_code = status.HTTP_202_ACCEPTED
            return 'updated'
        else:
            logger.info("update product img unsuccessful for: " + str(user_email))
            res.status_code = status.HTTP_451_UNAVAILABLE_FOR_LEGAL_REASONS
            return


    except Exception as e: # pragma: no cover
        logger.error(e)
        res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  SOMETHING_WENT_WRONG)




@router.get("/recommendation/{main_category}", response_description="recommendation on products by main category")
async def recommendation(main_category: str, res: Response):
    try:
        logger.info("Recommendation for product with params:")
        logger.info("Main category:" + str(main_category))

        resp = await db.recommendation_products(main_category)
        res.status_code = status.HTTP_200_OK
        return {'Product': resp}
    except Exception as e: # pragma: no cover
        logger.error(e)
        res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong')