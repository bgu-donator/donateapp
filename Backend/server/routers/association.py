from fastapi import APIRouter, status, Response,Request
from Backend.server.database import DB
import logging
from fastapi import APIRouter, Body
from fastapi.encoders import jsonable_encoder
from .routes_config import COOKIE_NAME, encrypt_string, decrypt_string, hash_password

from Backend.server.models.UserClass import (
    ErrorResponseModel,
    ResponseModel,
    UpdateStudentModel,
)

from Backend.server.models.AssociationClass import (
    AssociationSchema,
    LoginSchema,
    ChangeAssociationDetailsSchema
)

MUST_BE_GREATER_THEN_ZERO = 'Amount of random associations must be greater then 0'




USERNAME_IS_NOT_EXISTS = 'Username is not exists'
UNAUTHORIZED_ERROR = 'could not authenticate association'
ASC_COOKIE = "diaCookieASC"
SOMETHING_WENT_WRONG= 'Something went wrong'
db = DB()
logger = logging.getLogger('uvicorn.error')

router = APIRouter(
    prefix="/association",
    tags=["association"],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_description="Retrieving all association")
async def get_all_associations(response: Response):
    """
    Getting the list of all associations in the DB
    :param response:
    :return:
    """
    try:
        logger.info("Getting all associations")
        resp = await db.find_associations()
        response.status_code = status.HTTP_200_OK
        return {'Association': resp}
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong could not register')


@router.get("/get_association_by_email/{email}", response_description="Retrieving association by email")
async def get_association_by_email(email: str, response: Response):
    """
    This function is getting the data of the association
    :param email: is the string of the email
    """
    try:
        logger.info("Searching for association: " + str(email))
        resp = await db.find_association_by_email(email)
        if resp:
            logger.info("Found association: " + str(resp))
            response.status_code = status.HTTP_200_OK
            return {'Association': resp}
        logger.info("There is no association with the given email")
        response.status_code = status.HTTP_404_NOT_FOUND
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'email does not exists')
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong could not register')


@router.get("/{username}", response_description="Retrieving association by username")
async def get_association(username: str, response: Response):
    """
    :param username: is the string of the username
    """
    try:
        logger.info("Searching for " + str(username))
        resp = await db.find_association_by_username(username)
        if resp:
            logger.info("Found association " + str(username))
            response.status_code = status.HTTP_200_OK
            return {'Association': resp}
        logger.warning("There is no association with the given username")
        response.status_code = status.HTTP_404_NOT_FOUND
        return ErrorResponseModel("An error occurred.", status.HTTP_404_NOT_FOUND,
                                  USERNAME_IS_NOT_EXISTS)
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong could not register')




@router.get("/get_random_associations/{amount}", response_description="Retrieving k random associations")
async def get_random_associations(amount: int, response: Response):
    """
    This function is getting an amount of random associations to retrieve
    and fetches it from the DB
    :param amount: the number of asc to get
    :param response: the reponse object of the http request
    :return: list of associations in the size of amount
    """
    try:
        if amount < 1:
            response.status_code = status.HTTP_406_NOT_ACCEPTABLE
            return MUST_BE_GREATER_THEN_ZERO
        resp = await db.get_random_associations(amount)
        response.status_code = status.HTTP_200_OK
        return {'Association': resp}
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong')


@router.get("/get_associations_by_category/{category}", response_description="Retrieving k random associations")
async def get_associations_by_category(category: str, response: Response):
    """
    This function is getting associations by category
    :param category: category
    :param response: the reponse object of the http request
    :return: list of associations in this category
    """
    try:
        print(category)
        categories = category.split('&')
        ass = []
        for category in categories:
            resp = await db.find_associations_by_category(category)
            ass += resp
        response.status_code = status.HTTP_200_OK
        return {'Association': ass}
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong')


def set_cookie(email, response, name=ASC_COOKIE):
    logger.info("association: " + str(email) + " Successfully logged in")
    response.status_code = status.HTTP_200_OK
    logger.info("Sending Cookie")
    encrypted_mail = encrypt_string(email).decode()
    response.set_cookie(key=name, value=encrypted_mail, samesite='none', secure=True)


@router.post("/login")
async def login(login_params: LoginSchema, response: Response):
    """
    :param login_params: email and pw
    :param response: the response Object that returns the information to the association
    :return: response Object
    """
    try:
        logger.info("Login Process for association")
        email = login_params.email
        password = login_params.password

        logger.info("Loading Salt from DB")
        salt = await db.load_salt_for_user(email, db.asc_collection)
        if salt is None:
            # The user is probably not registered, return unauthorized
            response.status_code = status.HTTP_401_UNAUTHORIZED
            return UNAUTHORIZED_ERROR

        password = hash_password(password, salt)

        logger.info("Association email: " + str(email) + " try to log in")
        resp = await db.association_login(email, password)
        if resp:
            set_cookie(email, response, name=ASC_COOKIE)
            return email
        logger.info("Association: " + str(email) + " have wrong email or password")
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return UNAUTHORIZED_ERROR
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return {'Something went wrong, please try again'}


@router.patch("/update", response_description="update association details")
async def change_association_details(change_details: ChangeAssociationDetailsSchema, req: Request, res: Response):
    try:

        association_email_from_cookie = req.cookies[ASC_COOKIE]
        logger.info("fetching encrypted email from cookie " + str(association_email_from_cookie))
        if not association_email_from_cookie:
            res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {'message': SOMETHING_WENT_WRONG}
        logger.info("got encrypted email from cookie " + str(association_email_from_cookie))
        logger.info("try decrypting email from cookie")
        association_email = decrypt_string(association_email_from_cookie.encode())
        if association_email is None:
            res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {'message': SOMETHING_WENT_WRONG}
        logger.info("decrypted email from cookie " + str(association_email))
        logger.info("check email match with cookie")
        if association_email == change_details.email:
            logger.info("update user details for: " + str(association_email))
            updated = await db.update_association_details(change_details.email, change_details.username,
                                                          change_details.url, change_details.phone_number,
                                                          change_details.description)
            if updated:
                logger.info("updated success " + str(association_email))
                res.status_code = status.HTTP_202_ACCEPTED
                return 'updated'
            else:
                logger.info("update user details unsuccessful: " + str(association_email))
                res.status_code = status.HTTP_451_UNAVAILABLE_FOR_LEGAL_REASONS
                return
        else:
            logger.info("emails not match to cookie : " + str(association_email))
            res.status_code = status.HTTP_409_CONFLICT

    except Exception as e: # pragma: no cover
        logger.error(e)
        res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR)
