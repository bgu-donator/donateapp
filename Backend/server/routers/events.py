from fastapi import APIRouter, status, Response
from Backend.server.database import DB
import logging
from fastapi import APIRouter, Body
from fastapi.encoders import jsonable_encoder

from Backend.server.models.UserClass import (
    ErrorResponseModel,
    ResponseModel
)

from Backend.server.models.EventClass import (
    EventSchema
)

MUST_BE_GREATER_THEN_ZERO = 'Amount of random events must be greater then 0'

DELETED = 'deleted'

ADDED_SUCCESSFULLY = "Event added successfully"

EMAIL_IS_NOT_EXISTS = 'Email is not exists'

db = DB()
logger = logging.getLogger('uvicorn.error')

router = APIRouter(
    prefix="/association/event",
    tags=["events", "association"],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_description="Retrieving all events")
async def get_all_events(response: Response):
    """
    Getting the list of all events in the DB
    :param response:
    :return:
    """
    try:
        logger.info("Getting all events")
        resp = await db.get_all_events()
        response.status_code = status.HTTP_200_OK
        return {'Event': resp}
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong')


@router.get("/{email}", response_description="Retrieving events by association username")
async def get_associations_events(email: str, response: Response):
    """
    :param email: is the string of the email
    """
    try:
        logger.info("Searching for " + str(email))
        resp = await db.find_events_by_association_email(email)
        if resp is not None:
            logger.info("Found events for " + str(email) + " email association")
            response.status_code = status.HTTP_200_OK
            return {'Event': resp}
        logger.warning("There is no events for association with the given email")
        response.status_code = status.HTTP_404_NOT_FOUND
        return ErrorResponseModel("An error occurred.", status.HTTP_404_NOT_FOUND,
                                  EMAIL_IS_NOT_EXISTS)
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong')


@router.post("/add_event")
async def add_event(event: EventSchema, response: Response):
    """
    Adding new event
    :param event:
    :param response:
    :return:
    """
    try:
        logger.info("Add a new event")
        try:
            event = jsonable_encoder(event)
            inserted_event = await db.insert_event(event)
            if not inserted_event:
                response.status_code = status.HTTP_404_NOT_FOUND
                return ErrorResponseModel("An error occurred.", status.HTTP_404_NOT_FOUND,
                                          EMAIL_IS_NOT_EXISTS)
            logger.info("Inserted event successfully")
            response.status_code = status.HTTP_201_CREATED
            return ResponseModel(inserted_event, ADDED_SUCCESSFULLY, code=201)
        except Exception as e: # pragma: no cover
            logger.error(e)
            response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                      'Something went wrong')
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong could not register')


@router.get("/get_random_events/{amount}", response_description="Retrieving k random events")
async def get_random_events(amount: int, response: Response):
    """
    This function is getting an amount of random events to retrieve
    and fetches it from the DB
    :param amount: the number of events to get
    :param response: the reponse object of the http request
    :return: list of events in the size of amount
    """
    try:
        if amount < 1:
            response.status_code = status.HTTP_406_NOT_ACCEPTABLE
            return MUST_BE_GREATER_THEN_ZERO
        resp = await db.get_random_events(amount)
        response.status_code = status.HTTP_200_OK
        return {'Event': resp}
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong')
