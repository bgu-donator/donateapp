from fastapi import APIRouter, status
from fastapi.responses import JSONResponse
from pydantic import BaseModel

FAILED_PAYMENT = "Failed Payment"


class Message(BaseModel):
    message: str


router = APIRouter(
    prefix="/pay",
    tags=["Payment-Mock"],
    responses={404: {"description": "Not found"}}
)


@router.post("/")
async def pay():
    """
    This function is mocking the payment process
    :return: true as mocking a successfull payment
    """
    return JSONResponse(status_code=202)

@router.post("/fail")
async def pay_failure():
    """
    This function is mocking the payment process
    :return: false as mocking a failed payment
    """
    return JSONResponse(status_code=402, content={"message": FAILED_PAYMENT})
