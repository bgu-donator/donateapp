from fastapi import APIRouter, status, Response, Body, Request
import logging
from Backend.server.database import DB
from fastapi.encoders import jsonable_encoder
from .routes_config import COOKIE_NAME, encrypt_string, decrypt_string, NO_COOKIE_FOUND, SOMETHING_WENT_WRONG, \
    hash_password, generate_salt, ADMIN_COOKIE, ASC_COOKIE
import Backend.server.firebase as fb
import random
import string
# from Backend.server.utils import emailUtil
from Backend.server.models.UserClass import (
    ErrorResponseModel,
    ResponseModel,
    UserSchema,
    LoginSchema,
    FirebaseUserSchema,
    ChangePasswordSchema,
    ChangeDetailsSchema
)


USER_ADDED_SUCCESSFULLY = "User added successfully"
UNAUTHORIZED_ERROR = 'could not authenticate user'
ALREADY_EXISTS_ERROR_MESSAGE = 'User already exists could not register'

db = DB()
logger = logging.getLogger('uvicorn.error')

router = APIRouter(
    prefix="/users",
    tags=["users"],
    responses={404: {"description": "Not found"}},
)


def generate_random_password(): # pragma: no cover
    password_characters = string.ascii_letters + string.digits + string.punctuation
    password = ''.join(random.choice(password_characters) for i in range(8))
    return password


@router.post("/firebase_auth")
async def verify_login(response: Response, user: FirebaseUserSchema): # pragma: no cover
    """
    This Function is the redirect url from the SSO login
    :param response: the response of the request
    :param user: the user params for the SSO login
    :return: Valid or Invalid login with the relevant status code
    """
    logger.info("Firebase Authentication redirected")
    try:
        user_id = fb.validate_user_token_id(user.token_id.i)
        if user_id == user.uid:
            registered_user = await db.find_user_by_email(user.email)
            if registered_user is None:
                logger.debug("User is not registered in Mongo, register it now")

                new_user = UserSchema(
                    username=user.uid[:8],
                    name=user.name,
                    email=user.email,
                    password=generate_random_password()
                )
                logger.debug("Generated user from auth request")
                await register_user(new_user, response)
                logger.debug("Redirected User is registered")
            logger.info("Checking if User is admin " + str(registered_user))
            if 'is_admin' in registered_user.keys() and registered_user['is_admin']:
                logger.info("Admin Logged in")
                set_cookie(registered_user['email'], response, name=ADMIN_COOKIE)
            set_cookie(user.email, response, name=COOKIE_NAME)
            response.status_code = status.HTTP_200_OK
            return 'Valid Login'
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return 'Invalid Login'
    except Exception as e:
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong could not register')


@router.post("/logout")
async def logout(response: Response):
    """
    Logout a user by deleting the Cookie from its browser
    """
    try:
        logger.info("Logout Process")
        logger.info("Set Cookie with 0 age in order to remove it from the Browser Cookie")
        # response.set_cookie(COOKIE_NAME, max_age=0)
        response.set_cookie(COOKIE_NAME, samesite='none', secure=True, expires=0, max_age=0, path='/')
        response.set_cookie(ADMIN_COOKIE, samesite='none', secure=True, expires=0, max_age=0, path='/')
        response.set_cookie(ASC_COOKIE, samesite='none', secure=True, expires=0, max_age=0, path='/')
        return 'Logged Out'
    except Exception as e: # pragma: no cover
        logger.error(e)
        return {'Something went wrong, please try again'}


def set_cookie(email, response, name=COOKIE_NAME):
    logger.info("User: " + str(email) + " Successfully logged in")
    response.status_code = status.HTTP_200_OK
    logger.info("Sending Cookie")
    encrypted_mail = encrypt_string(email).decode()
    response.set_cookie(key=name, value=encrypted_mail, samesite='none', secure=True)


@router.post("/login")
async def login(login_params: LoginSchema, response: Response):
    """
    :param login_params: the email and password schema
    :param response: the response Object that returns the information to the client
    :return: response Object
    """


    try:
        logger.info("Login Process")
        email = login_params.email
        password = login_params.password

        logger.info("Loading Salt from DB")
        salt = await db.load_salt_for_user(email, db.users_collection)
        if salt is None:
            # The user is probably not registered, return unauthorized
            response.status_code = status.HTTP_401_UNAUTHORIZED
            return UNAUTHORIZED_ERROR
        logger.info("User: " + str(email) + " Logging in")
        password = hash_password(password, salt)
        resp = await db.login(email, password)
        if resp:
            logger.info("Checking if User is admin " + str(resp))
            if 'is_admin' in resp.keys() and resp['is_admin']:
                logger.info("Admin Logged in")
                set_cookie(email, response, name=ADMIN_COOKIE)
            set_cookie(email, response, name=COOKIE_NAME)
            return email
        logger.info("User: " + str(email) + " wrong email or password")
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return UNAUTHORIZED_ERROR
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return {'Something went wrong, please try again'}



@router.post("/register")
async def register_user(user: UserSchema, response: Response):
    """
    This Function is registering a new user
    :param response: the response of the request
    :param user: the json object representing a new user
    :return: the user inserted without its password
    """
    logger.info("Generate new salt")
    salt = generate_salt()
    logger.info("Registering a new user")
    email = user.email
    user_from_db = await db.find_user_by_email(email)
    if user_from_db is not None:
        logger.error(ALREADY_EXISTS_ERROR_MESSAGE)
        response.status_code = status.HTTP_409_CONFLICT
        return ErrorResponseModel("An error occurred.", status.HTTP_409_CONFLICT,
                                  ALREADY_EXISTS_ERROR_MESSAGE)
    logger.info("There is no user with the given email")
    try:
        hashed_password = hash_password(user.password, salt)
        user.password = hashed_password
        user.salt = salt
        user = jsonable_encoder(user)
        inserted_user = await db.insert_user(user)
        logger.info("Inserted User successfully")
        response.status_code = status.HTTP_201_CREATED
        return ResponseModel(inserted_user, USER_ADDED_SUCCESSFULLY, code=201)
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong could not register')


@router.get("/{email}", response_description="Retrieving user by email")
async def get_user(email: str, response: Response):
    """
    This function is getting the data of the user
    :param email: is the string of the email
    """
    try:
        logger.info("Searching for user: " + str(email))
        # TODO: check if the user that asks this page is the login user
        resp = await db.find_user_by_email(email)
        if resp:
            logger.info("Found user: " + str(resp))
            response.status_code = status.HTTP_200_OK
            return {'User': resp}
        logger.info("There is no user with the given email")
        response.status_code = status.HTTP_404_NOT_FOUND
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'email does not exists')
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong could not register')

def is_user_admin(req, response):
    user_email_from_cookie = req.cookies[ADMIN_COOKIE]
    logger.info("fetching encrypted email from cookie " + str(user_email_from_cookie))
    if not user_email_from_cookie:
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return False, {'message': NO_COOKIE_FOUND}

    logger.info("got encrypted email from cookie " + str(user_email_from_cookie))
    logger.info("decrypting email from cookie")
    user_email = decrypt_string(user_email_from_cookie.encode())
    if user_email is None:
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return False, {'message': SOMETHING_WENT_WRONG}
    logger.info("decrypted email from cookie " + str(user_email))
    return True, {'email': user_email}


@router.get("/get_donations/{email}", response_description="Retrieving user donations by username")
async def get_donations_for_user(email: str, response: Response):
    """
    This function is getting the donations of the user
    :param username: is the string of the username
    """
    try:
        logger.info("Searching for user email: " + str(email))
        # TODO: check if the user that asks this page is the logined user

        # resp = await db.find_user_by_username(username)
        resp = await db.get_donations_for_user(email)
        if resp is not None:
            logger.info("Found donation: " + str(resp))
            response.status_code = status.HTTP_200_OK
            return {'Donation': resp}
        logger.info("There is no user with the given username")
        response.status_code = status.HTTP_404_NOT_FOUND
        return 'Username does not exists'
    except Exception as e: # pragma: no cover
        logger.error(e)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  'Something went wrong could not register')


@router.patch("/change-password", response_description="change user password")
async def change_user_password(
        change_password: ChangePasswordSchema, req: Request, res: Response
):
    try:
        user_email_from_cookie = req.cookies[COOKIE_NAME]
        logger.info("fetching encrypted email from cookie " + str(user_email_from_cookie))
        if not user_email_from_cookie:
            res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {'message': NO_COOKIE_FOUND}
        logger.info("got encrypted email from cookie " + str(user_email_from_cookie))
        logger.info("try decrypting email from cookie")
        user_email = decrypt_string(user_email_from_cookie.encode())
        email = await db.find_user_by_email(user_email)
        if email is None:
            res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {'message': SOMETHING_WENT_WRONG}
        logger.info("decrypted email from cookie " + str(user_email))
        logger.info("check password")

        logger.info("Hashing passwords")
        logger.info("Loading Salt from DB")
        salt = await db.load_salt_for_user(user_email, db.users_collection)
        if salt is None:
            # The user is probably not registered, return unauthorized
            res.status_code = status.HTTP_401_UNAUTHORIZED
            return UNAUTHORIZED_ERROR
        current_pass_hash = hash_password(change_password.current_password, salt)
        new_pass_hash = hash_password(change_password.new_password, salt)

        password = await db.check_password(user_email, current_pass_hash)
        if password is False:
            logger.error("wrong password" + str(user_email))
            res.status_code = status.HTTP_404_NOT_FOUND
            return {'message': UNAUTHORIZED_ERROR}
        logger.info("password belongs to user" + str(user_email))
        logger.info("checking new password " + str(user_email))
        if change_password.new_password == change_password.confirm_password:
            update_password = await db.update_password(user_email, new_pass_hash)
            if update_password:
                logger.info("updated password " + str(user_email))
                res.status_code = status.HTTP_202_ACCEPTED
                return 'updated'
            else:
                logger.info("unsuccessful update" + str(user_email))
                res.status_code = status.HTTP_451_UNAVAILABLE_FOR_LEGAL_REASONS
        else:
            logger.info("unsuccessful update" + str(user_email))
            res.status_code = status.HTTP_409_CONFLICT
    except Exception as e: # pragma: no cover
        logger.error(e)
        res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  SOMETHING_WENT_WRONG)


@router.patch("/update", response_description="update user details")
async def change_user_details(
        change_details: ChangeDetailsSchema, req: Request, res: Response
):
    try:
        user_email_from_cookie = req.cookies[COOKIE_NAME]
        logger.info("fetching encrypted email from cookie " + str(user_email_from_cookie))
        if not user_email_from_cookie:
            res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {'message': NO_COOKIE_FOUND}
        logger.info("got encrypted email from cookie " + str(user_email_from_cookie))
        logger.info("try decrypting email from cookie")
        user_email = decrypt_string(user_email_from_cookie.encode())
        email = await db.find_user_by_email(user_email)
        if email is None:
            res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {'message': SOMETHING_WENT_WRONG}
        logger.info("decrypted email from cookie " + str(user_email))
        logger.info("check email match with cookie")
        if user_email == change_details.email:
            logger.info("update user details for: " + str(user_email))
            updated = await db.update_user_details(change_details.email, change_details.username, change_details.firstName, change_details.lastName)
            if updated:
                logger.info("updated success " + str(user_email))
                res.status_code = status.HTTP_202_ACCEPTED
                return 'updated'
            else:
                logger.info("update user details unsuccessful: " + str(user_email))
                res.status_code = status.HTTP_451_UNAVAILABLE_FOR_LEGAL_REASONS
                return
        else:
            logger.info("emails not match to cookie : " + str(user_email))
            res.status_code = status.HTTP_409_CONFLICT

    except Exception as e: # pragma: no cover
        logger.error(e)
        res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
                                  SOMETHING_WENT_WRONG)




# @router.post("/reset-password", response_description="reset user password")
# async def reset_user_password(
#         email: str, res: Response
# ):
#     try:
#         logger.info("check if user email exist" + str(email))
#         user = await db.find_user_by_email(email)
#         if user is None:
#             logger.error("user email does not exist" + str(email))
#             res.status_code = status.HTTP_404_NOT_FOUND
#         else:
#             logger.info("create reset code for" + str(email))
#             reset_code = random.randint(0, 999999)
#             subject = "reset password"
#             recipient = email
#             message = """
#             <!DOCTYPE html>
#             <html>
#             <title>Reset Password</title>
#             <body>
#             <h1>Hello {0:}</h1>
#             <p>You have requested a password reset for your account {0:}. Your temporary code to log
#              in is: {1:} This code is active for 5 minutes. If you do not wish to change your password, you may ignore
#              this message and your password will not be changed </p>
#             </body>
#             </html>
#             """.format(email, reset_code)
#             logger.info("sending mail.." + str(email))
#             result = await emailUtil.send_email(subject, list(recipient.split(" ")), message)
#             db.add_reset_code(email)
#
#     except Exception as e:
#         logger.error(e)
#         res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
#         return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR,
#                                   SOMETHING_WENT_WRONG)
