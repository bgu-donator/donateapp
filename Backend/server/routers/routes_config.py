from cryptography.fernet import Fernet
import logging
import hashlib
import string
import os

logger = logging.getLogger('uvicorn.error')
COOKIE_NAME = "diaCookie"
ADMIN_COOKIE = "diaAdmin"
NO_ID_BELONGS_TO_THIS_USER = "No Product with the id belongs to this user"
NO_PRODUCT_WITH_THIS_ID = "No Product with this id"
SOMETHING_WENT_WRONG = "Something went wrong"
NO_COOKIE_FOUND = "No Cookie found"
ADDED_SUCCESSFULLY = "Product added successfully"
PRODUCT_DOES_NOT_EXISTS = "Product does not exists"
ASC_COOKIE = "diaCookieASC"

# generate a key for encryptio and decryption
# You can use fernet to generate
# the key or use random key generator
# here I'm using fernet to generate key
key = Fernet.generate_key()
# Instance the Fernet class with the key
fernet = Fernet(key)


def encrypt_string(string_to_encrypt: str) -> bytes:
    """
    This function is encrypting a string using Fernet encryptor
    :param string_to_encrypt: the string that should be encrypted
    :return: the encrypted string in bytes or None if failed
    """
    try:
        return fernet.encrypt(string_to_encrypt.encode())
    except Exception as e: # pragma: no cover
        logger.error(e)
        return None


def decrypt_string(encrypted_string: bytes) -> str:
    """
    This function is decrypting a string using Fernet decryptor
    :param encrypted_string: the encrypted string that should be decrypted
    :return: the decrypted string or None if failed
    """
    try:
        return fernet.decrypt(encrypted_string).decode()
    except Exception as e: # pragma: no cover
        logger.error(e)
        return None


def hash_password(password: string, salt: string):
    """
    This Function is getting a user and hashing its password
    :param password: the password to hash
    :return: the hashed password
    :except: throws exception on failing to hash the password
    """
    try:
        logger.info("Hashing the password")
        hashed_password = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt.encode('utf-8'), 10000).hex()
        return hashed_password
    except Exception as e: # pragma: no cover
        logger.error(e)
        raise e


def generate_salt():
    """
    This function is loading a salt for specific user during the registration,
    it is used with hash function to hash the password, so it will not be saved in clear text
    :return: the salt
    """
    try:
        logger.info("Generating new salt")
        salt = str(os.urandom(32))
        return salt

    except Exception as e: # pragma: no cover
        logger.error(e)
        raise Exception("Could not generate salt for user")
