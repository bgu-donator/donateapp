import sys

from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse

from Backend.server import database
import os

# Generate the DB Singleton
atlas_username = os.getenv('ATLAS_USERNAME')
atlas_password = os.getenv('ATLAS_PASSWORD')
atlas_url = os.getenv('ATLAS_URL')
# DB(atlas_username, atlas_password, atlas_url)
database.DB(atlas_username,atlas_password,atlas_url)

from Backend.server.routers import users, association, products, admin
from Backend.server.routers import orders, payRoutes, events
import logging


app = FastAPI()
get_par = os.path.abspath('.')
is_tests = False
if 'tests' in get_par:
    is_tests = True

if 'Backend' not in get_par:
    get_par += '\\Backend'

if not is_tests:
    static_spa_folder = get_par+'/Frontend/mobile/dist/spa/' if 'linux' in sys.platform else get_par+'\\..\\Frontend\\mobile\\dist\\spa\\'
    app.mount("/spa", StaticFiles(directory=static_spa_folder), name="spa")
    app.mount("/css", StaticFiles(directory=static_spa_folder + "css"), name="css")
    app.mount("/img", StaticFiles(directory=static_spa_folder + "img"), name="img")
    app.mount("/icons", StaticFiles(directory=static_spa_folder + "icons"), name="icons")
    app.mount("/fonts", StaticFiles(directory=static_spa_folder + "fonts"), name="fonts")
    app.mount("/js", StaticFiles(directory=static_spa_folder + "js"), name="js")

    templates = Jinja2Templates(static_spa_folder)



origins = [
    "https://localhost",
    "https://localhost:8080",
    "https://localhost:8081",
    "https://donateitapp.com:8080",
    "https://192.168.1.30:8080",
    "https://donateitapp.herokuapp.com/"
    # "*"
]




@app.get("/", response_class=HTMLResponse, response_description="Retrieving the index.html static file")
async def root(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
logger = logging.getLogger("uvicorn.error")


app.include_router(users.router)
app.include_router(association.router)
app.include_router(admin.router)
app.include_router(products.router)
app.include_router(orders.router)
app.include_router(payRoutes.router)
app.include_router(events.router)
