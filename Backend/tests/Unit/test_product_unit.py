from Backend.server import database
import os
import Backend.tests.initialize as init
import pytest
import sys
import asyncio

VALID_USER_EMAIL = 'arnav@gmail.com'
INVALID_USER_EMAIL = 'arnav123@gmail.com'
VALID_USER_PASS = '123456'
databaseDB = database.DB()
OS_SEPRATOR = '/' if 'linux' in sys.platform else '\\'
fake_prod = {
    'id': '5ff71fa409ca7a36f17525cd',
    "product_name": "מחשב",
    "description": "מחשב חדש דנדש",
    "price": 3500,
    "main_category": "אלקטרוניקה",
    "city": "מיתר",
    "image": [],
    'status': 'FOR_SALE',
    "phone_number": "(050) 848 - 2761",
    "seller_email": VALID_USER_EMAIL,
    "sub_category": ["מחשב-נייד", "מחשב-נייח"],
}
event_loop_for_tests = asyncio.get_event_loop()


@pytest.fixture(scope="module", autouse=True)
def pytest_sessionstart(session_mocker):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """
    db_col_to_clean = [
        {
            'name': 'Products.json',
            'collection': init.products_collection
        }]
    for col in db_col_to_clean:
        json_file = str(os.path.dirname(__file__)) + OS_SEPRATOR + '..' + OS_SEPRATOR + 'files' + OS_SEPRATOR + col['name']
        init.clean_collection(col['collection'], json_file)



@pytest.fixture
def event_loop():
    yield event_loop_for_tests


# def teardown_module(module):
#     """ teardown any state that was previously setup with a setup_module
#     method.
#     """
#     event_loop_for_tests.close()

# @pytest.yield_fixture
# def event_loop():
#     """Create an instance of the default event loop for each test case."""
#     policy = asyncio.get_event_loop_policy()
#     res = policy.new_event_loop()
#     asyncio.set_event_loop(res)
#     res._close = res.close
#     res.close = lambda: None
#
#     yield res
#
#     res._close()
# @pytest.fixture
# def event_loop():
#     yield asyncio.get_event_loop()
#
#
# def pytest_sessionfinish(session, exitstatus):
#     asyncio.get_event_loop().close()


def test_insert_product(event_loop):
    product = event_loop.run_until_complete(databaseDB.insert_product(fake_prod))
    assert product['product_name'] == 'מחשב'
    assert product['price'] == 3500
    assert product['seller_email'] == VALID_USER_EMAIL


def test_get_all_products(event_loop):
    product = event_loop.run_until_complete(databaseDB.get_all_products())
    assert len(product) == 5
    assert isinstance(product, list)


def test_get_product_by_id(event_loop):
    product = event_loop.run_until_complete(databaseDB.get_product_by_id("606c7f2c4fc44f676ade5bce"))
    assert product['product_name'] == 'MP3'
    assert product['price'] == 250
    assert product['seller_email'] == VALID_USER_EMAIL


def test_get_product_by_id_not_exist(event_loop):
    product = event_loop.run_until_complete(databaseDB.get_product_by_id("c7f2c4fc44f676ade5bc"))
    assert product is None


def test_delete_product_by_id(event_loop):
    product = event_loop.run_until_complete(databaseDB.delete_product_by_id("5ff71fa409ca7a36f17525cd"))
    assert product['deleted'] is True


def test_delete_product_by_non_exist_id(event_loop):
    product = event_loop.run_until_complete(databaseDB.delete_product_by_id("c7f2c4fc44f676ade5bc"))
    assert product['deleted'] is False


def test_update_sold(event_loop):
    product = event_loop.run_until_complete(databaseDB.update_sold("5ff71fa409ca7a36f17525cd"))
    assert product is True


def test_update_sold_non_exist_id(event_loop):
    product = event_loop.run_until_complete(databaseDB.update_sold("5ff71fa409ca7a36f17525c"))
    assert product is False


def test_update_for_sale(event_loop):
    product = event_loop.run_until_complete(databaseDB.update_for_sale("5ff71fa409ca7a36f17525cd"))
    assert product is True


def test_update_for_sale_non_exist_id(event_loop):
    product = event_loop.run_until_complete(databaseDB.update_for_sale("5ff71fa409ca7a36f17525c"))
    assert product is False


# def test_insert_order(event_loop):
#     product = {
#         "_id": {
#             "$oid": "606c81e24fc44f676ade5bcf"
#         },
#         "product_name": "בושם מונט בלנק",
#         "description": "בושם לגבר לא לטעמי",
#         "price": 99,
#         "main_category": "בריאות ויופי",
#         "city": "עומר ",
#         "phone_number": "(052) 783 - 5611",
#         "seller_email": "arnav@gmail.com",
#         "sub_category": [
#             "בישום",
#             "גברים",
#             "טיפוח"
#         ],
#         "image": [
#             "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVTYDooPqZ5noMG98f02SCRLuTvblCvQgDwkydfDEssgFfp1u9HaFrVBMssg&usqp=CAc"
#         ],
#         "status": "FOR_SALE",
#         "publish_at": {
#             "$date": "2021-04-06T18:44:34.376Z"
#         }
#     }
#     association = {
#         "_id": {
#             "$oid": "5ff37a3ba2f287a7e125ff89"
#         },
#         "name": "אור למשפחות",
#         "username": "john_doe",
#         "password": "655f6bc893872be72ab64e11ffc330daf1320697b43081b3e9eeed463fbd9366",
#         "salt": "b'&\\x82I\\t\\xa3N\\xbd\\x92@\\xe4\\xd9\\x00N\\x9b\\xe5<\\xa5H\\xf4\\xd5S{\\xebt\\x84\\xe4A\\xae\\xa1\\xd7\\xcbu'",
#         "email": "or4family@gmail.com",
#         "phone_number": "0555555555",
#         "donations": 0,
#         "url": "give@gmail.com",
#         "bank_account": {
#             "branch_number": "123",
#             "account_number": "123456",
#             "bank_name": "Hapoalim",
#             "bank_number": "12"
#         },
#         "activity": "כוחות הבטחון",
#         "image": [
#             "https://yt3.ggpht.com/ytc/AAUvwnjiTYkzY6-wtwulLBcXs3uazyw2OzHiGScjCSiE=s900-c-k-c0x00ffffff-no-rj"
#         ],
#         "description": "עמותת אור למשפחות הוקמה בשנת 2008 במטרה לחזק הורים שאיבדו את ילדיהם\n\nבמערכות ישראל באמצעות חוויות מקרבות ומעצימות. העמותה חוללה מהפיכה בתפיסת\n\nהשכול בישראל ועל כך קיבלה את\nאות נשיא המדינה למתנדב לשנת 2013\nאות המופת כנס שדרות לחברה לשנת 2018\nאות פול האריס על תרומה למען הקהילה והחברה 2019\n\nאלפי הורים חברים היום באור למשפחות. שואבים כוח, ובוחרים בחיים.\n\nהעמותה הוקמה על ידי עירית אורן גונדרס, סגן אלוף במיל’ וראש ענף כוח אדם בחיל\n\nהנדסה קרבית, יו”ר ומנכ”ל העמותה. עירית פועלת מביתה בהתנדבות מלאה וללא לאות לקידומה \n\nוהרחבתה של העמותה ורואה בה מפעל חיים בעל חשיבות עליונה. לשליחות זו נרתמו\n\nכל בני משפחתה וחבריה שדבקים עמה במשימה במסירות אין קץ."
#     }
#     order = {
#         "product_id": "606c81e24fc44f676ade5bcf",
#         "date": "2019-06-01 12:22",
#         "buyer_email": "arnav@gmail.com",
#         "seller_email": "arnav12@gmail.com",
#         "association_username": "john_doe",
#     }
#     order = event_loop.run_until_complete(databaseDB.insert_order(order, "arnav@gmail.com", "arnav12@gmail.com", product,association))
#     assert order['seller'] == "arnav@gmail.com"
#     assert order['buyer'] == "arnav12@gmail.com"



def test_search_products(event_loop):
    product = event_loop.run_until_complete(databaseDB.search_products("בריאות ויופי","null",0,10000,"null"))
    assert len(product) == 1
    assert isinstance(product, list)
