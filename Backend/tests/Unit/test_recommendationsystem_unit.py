import os
import Backend.tests.initialize as init
from fastapi.testclient import TestClient
from Backend.server.app import app
from Backend.server import database
import pytest
import sys
import asyncio

client = TestClient(app)
databaseDB = database.DB()
OS_SEPRATOR = '/' if 'linux' in sys.platform else '\\'

event_loop_for_tests = asyncio.get_event_loop()


@pytest.fixture(scope="module", autouse=True)
def pytest_sessionstart(session_mocker):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """
    json_file = str(os.path.dirname(__file__)) + OS_SEPRATOR + OS_SEPRATOR + '..' + OS_SEPRATOR + 'files' + OS_SEPRATOR + 'Products.json'
    init.clean_collection(init.products_collection, json_file)


@pytest.fixture
def event_loop():
    yield event_loop_for_tests


# def teardown_module(module):
#     """ teardown any state that was previously setup with a setup_module
#     method.
#     """
#     event_loop_for_tests.close()


def test_get_recommendation_by_main_category_there_is_products_with_connected_categories_in_db(event_loop):
    main_category = "בריאות ויופי"
    products_array = event_loop.run_until_complete(databaseDB.recommendation_products(main_category))
    assert len(products_array) == 1
    assert isinstance(products_array, list)
    assert products_array[0]['main_category'] == 'לבוש ואביזרים'


def test_get_recommendation_by_main_category_dont_have_products_with_connected_categories_in_db(event_loop):
    main_category = "אלקטרוניקה"
    products_array = event_loop.run_until_complete(databaseDB.recommendation_products(main_category))
    assert len(products_array) == 2
    assert isinstance(products_array, list)

