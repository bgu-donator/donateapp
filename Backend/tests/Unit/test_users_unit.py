from Backend.server import database
import os
import Backend.tests.initialize as init
import pytest
import sys
import asyncio

VALID_USER_EMAIL = 'arnav@gmail.com'
INVALID_USER_EMAIL = 'shtut16736@gmail.com'
VALID_USER_PASS = '655f6bc893872be72ab64e11ffc330daf1320697b43081b3e9eeed463fbd9366'
INVALID_USER_PASS = '123457'
databaseDB = database.DB()
OS_SEPRATOR = '/' if 'linux' in sys.platform else '\\'
event_loop_for_tests = asyncio.get_event_loop()


@pytest.fixture(scope="module", autouse=True)
def pytest_sessionstart(session_mocker):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """
    db_col_to_clean = [
        {
            'name': 'Users.json',
            'collection': init.users_collection
        }]
    for col in db_col_to_clean:
        json_file = str(os.path.dirname(__file__)) + OS_SEPRATOR + '..' + OS_SEPRATOR + 'files' + OS_SEPRATOR + col['name']
        init.clean_collection(col['collection'], json_file)


@pytest.fixture
def event_loop():
    yield event_loop_for_tests


# def teardown_module(module):
#     """ teardown any state that was previously setup with a setup_module
#     method.
#     """
#     event_loop_for_tests.close()

# @pytest.yield_fixture
# def event_loop():
#     """Create an instance of the default event loop for each test case."""
#     policy = asyncio.get_event_loop_policy()
#     res = policy.new_event_loop()
#     asyncio.set_event_loop(res)
#     res._close = res.close
#     res.close = lambda: None
#
#     yield res
#
#     res._close()
# @pytest.fixture
# def event_loop():
#     yield asyncio.get_event_loop()
#
#
# def pytest_sessionfinish(session, exitstatus):
#     asyncio.get_event_loop().close()


def test_find_users(event_loop):
    users = event_loop.run_until_complete(databaseDB.find_users())
    assert len(users) == 2
    assert isinstance(users, list)


def test_find_user_by_email(event_loop):
    user = event_loop.run_until_complete(databaseDB.find_user_by_email(VALID_USER_EMAIL))
    assert user['name'] == 'John Doe'
    assert user['email'] == VALID_USER_EMAIL
    assert user['username'] == 'john_doe'


def test_find_user_by_email_by_non_exist_email(event_loop):
    user = event_loop.run_until_complete(databaseDB.find_user_by_email(INVALID_USER_EMAIL))
    assert user is None


def test_get_donations_for_user(event_loop):
    donation = event_loop.run_until_complete(databaseDB.get_donations_for_user("arnav12@gmail.com"))
    assert len(donation) == 1
    assert isinstance(donation, list)
    assert donation[0]['total'] == 250


def test_get_donations_for_user_by_non_exist_email(event_loop):
    donation = event_loop.run_until_complete(databaseDB.get_donations_for_user(INVALID_USER_EMAIL))
    assert donation is None


def test_check_password(event_loop):
    password = event_loop.run_until_complete(databaseDB.check_password(VALID_USER_EMAIL, VALID_USER_PASS))
    assert password is True


def test_check_password_wrong_password(event_loop):
    password = event_loop.run_until_complete(databaseDB.check_password(VALID_USER_EMAIL, INVALID_USER_PASS))
    assert password is False


def test_check_password_wrong_email(event_loop):
    password = event_loop.run_until_complete(databaseDB.check_password("arnav1234@gmail.com", INVALID_USER_PASS))
    assert password is None


def test_login_valid_email_valid_password(event_loop):
    user = event_loop.run_until_complete(databaseDB.login(VALID_USER_EMAIL, VALID_USER_PASS))
    assert user['name'] == 'John Doe'
    assert user['email'] == VALID_USER_EMAIL
    assert user['username'] == 'john_doe'


def test_login_valid_email_invalid_password(event_loop):
    user = event_loop.run_until_complete(databaseDB.login(VALID_USER_EMAIL, INVALID_USER_PASS))
    assert user is None


def test_login_invalid_email_invalid_password(event_loop):
    user = event_loop.run_until_complete(databaseDB.login("arnav1234@gmail.com", INVALID_USER_PASS))
    assert user is None


def test_insert_user(event_loop):
    user_to_register = {
        'name': 'shtut',
        'username': 'shtut',
        'email': "galrosenthal12@gmail.com",
        'password': VALID_USER_PASS,
        "total_donation": 0,
        "is_admin": False,
        "products_for_sale": []
    }
    user = event_loop.run_until_complete(databaseDB.insert_user(user_to_register))
    assert user['name'] == 'shtut'
    assert user['email'] == 'galrosenthal12@gmail.com'
    assert user['username'] == 'shtut'


def test_find_admins(event_loop):
    admin = event_loop.run_until_complete(databaseDB.find_admins())
    assert len(admin) == 1
    assert isinstance(admin, list)
    assert admin[0]['email'] == VALID_USER_EMAIL


def test_set_admin(event_loop):
    admin = event_loop.run_until_complete(databaseDB.set_admin("galrosenthal12@gmail.com",True))
    assert admin is True


def test_set_admin_not_admin(event_loop):
    admin = event_loop.run_until_complete(databaseDB.set_admin("galrosenthal12@gmail.com",False))
    assert admin is True


def test_update_user_details(event_loop):
    updated = event_loop.run_until_complete(databaseDB.update_user_details("galrosenthal12@gmail.com","galgal","gal","rosen"))
    assert updated is True


# def test_update_user_details_non_exist(event_loop):
#     updated = event_loop.run_until_complete(databaseDB.update_user_details("notexist@gmail.com","galgal","gal","rosen"))
#     assert updated is False


def test_update_password(event_loop):
    password = event_loop.run_until_complete(databaseDB.update_password(VALID_USER_EMAIL, INVALID_USER_PASS))
    assert password is True


def test_delete_user_by_email_by_non_exist_email(event_loop):
    deleted = event_loop.run_until_complete(databaseDB.delete_user_by_email(INVALID_USER_EMAIL))
    assert deleted is False


def test_delete_user_by_email(event_loop):
    deleted = event_loop.run_until_complete(databaseDB.delete_user_by_email(VALID_USER_EMAIL))
    assert deleted is True


def test_find_details_of_donations_per_order(event_loop):
    order = {
        "product_id": "606c81e24fc44f676ade5bcf",
        "date": "2019-06-01 12:22",
        "buyer_email": "arnav@gmail.com",
        "seller_email": "arnav12@gmail.com",
        "association_username": "john_doe",
    }
    donations = event_loop.run_until_complete(databaseDB.find_details_of_donations_per_order(order))
    assert donations["name"] == 'אור למשפחות'
    assert donations["total"] == 99

