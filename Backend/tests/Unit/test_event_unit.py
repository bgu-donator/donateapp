from Backend.server import database
import os
import Backend.tests.initialize as init
import pytest
import sys
import asyncio

EXIST_EMAIL = "or4family@gmail.com"
EXIST_EMAIL_WITHOUT_EVENTS = "paamonim@gmail.com"
NON_EXIST_EMAIL = "arnav12@gmail.com"
databaseDB = database.DB()
OS_SEPRATOR = '/' if 'linux' in sys.platform else '\\'

event_loop_for_tests = asyncio.get_event_loop()


@pytest.fixture(scope="module", autouse=True)
def pytest_sessionstart(session_mocker):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """
    db_col_to_clean = [
        {
            'name': 'Events.json',
            'collection': init.events_collection
        },{
            'name': 'Associations.json',
            'collection': init.associations_collection
        }]
    for col in db_col_to_clean:
        json_file = str(os.path.dirname(__file__)) + OS_SEPRATOR + '..' + OS_SEPRATOR + 'files' + OS_SEPRATOR + col['name']
        init.clean_collection(col['collection'], json_file)


@pytest.fixture
def event_loop():
    yield event_loop_for_tests

#
# def teardown_module(module):
#     """ teardown any state that was previously setup with a setup_module
#     method.
#     """
#     event_loop_for_tests.close()
#
#
# def test_get_all_events(event_loop):
#     num_of_events = event_loop.run_until_complete(databaseDB.get_all_events())
#     assert len(num_of_events) == 4


def test_get_association_events_by_exists_email_and_exist_events(event_loop):
    event_array = event_loop.run_until_complete(databaseDB.find_events_by_association_email(EXIST_EMAIL))
    assert len(event_array) == 3
    assert isinstance(event_array, list)
    assert event_array[0]['association_name'] == 'אור למשפחות'
    assert event_array[1]['association_name'] == 'אור למשפחות'
    assert event_array[2]['association_name'] == 'אור למשפחות'


def test_get_association_events_by_exists_email_and_non_exist_events(event_loop):
    event_array = event_loop.run_until_complete(databaseDB.find_events_by_association_email(EXIST_EMAIL_WITHOUT_EVENTS))
    assert len(event_array) == 0
    assert isinstance(event_array, list)


def test_get_association_events_by_non_exists_email(event_loop):
    event_array = event_loop.run_until_complete(databaseDB.find_events_by_association_email(NON_EXIST_EMAIL))
    assert event_array is None


def test_get_random_events_with_number_greather_then_number_of_events(event_loop):
    events = event_loop.run_until_complete(databaseDB.get_random_events(10))
    assert isinstance(events, list)
    assert len(events) == 4


def test_get_random_events_with_number_until_max_num_of_events(event_loop):
    events = event_loop.run_until_complete(databaseDB.get_random_events(4))
    assert isinstance(events, list)
    assert len(events) == 4


def test_get_random_events_with_number_less_then_zero(event_loop):
    events = event_loop.run_until_complete(databaseDB.get_random_events(-1))
    assert isinstance(events, list)
    assert len(events) == 0


def test_insert_new_event_with_non_exist_email_association(event_loop):
    new_event = {
        "date": "2021-05-26",
        "description": "אירוע התרמה חדש שנוסף בטסטים",
        "association_name": "אור למשפחות",
        "association_email": NON_EXIST_EMAIL
    }
    num_of_events = event_loop.run_until_complete(databaseDB.get_all_events())
    assert len(num_of_events) == 4
    response = event_loop.run_until_complete(databaseDB.insert_event(new_event))
    assert response is False
    num_of_events_after_insert = event_loop.run_until_complete(databaseDB.get_all_events())
    assert len(num_of_events_after_insert) == 4


def test_insert_new_event_with_exist_email_association(event_loop):
    new_event = {
        "date": "2021-05-26",
        "description": "אירוע התרמה חדש שנוסף בטסטים",
        "association_name": "אור למשפחות",
        "association_email": EXIST_EMAIL
    }
    num_of_events = event_loop.run_until_complete(databaseDB.get_all_events())
    assert len(num_of_events) == 4
    response = event_loop.run_until_complete(databaseDB.insert_event(new_event))
    assert response is True
    num_of_events_after_insert = event_loop.run_until_complete(databaseDB.get_all_events())
    assert len(num_of_events_after_insert) == 5




