from Backend.server import database
import os
import Backend.tests.initialize as init
import pytest
import sys
import asyncio

EXIST_EMAIL = "or4family@gmail.com"
NON_EXIST_EMAIL = "arnav12@gmail.com"
RIGHT_PASSWORD = '655f6bc893872be72ab64e11ffc330daf1320697b43081b3e9eeed463fbd9366'
WRONG_PASSWORD = '1234567'
EXIST_USERNAME = "john_doe"
NON_EXIST_USERNAME = "john_doe2"
EXIST_CATEGORY = "כוחות הבטחון"
NON_EXIST_CATEGORY = "לא קיים"
BASE_URL = '/association/'
databaseDB = database.DB()
OS_SEPRATOR = '/' if 'linux' in sys.platform else '\\'

event_loop_for_tests = asyncio.get_event_loop()


@pytest.fixture(scope="module", autouse=True)
def pytest_sessionstart(session_mocker):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """
    json_file = str(os.path.dirname(__file__)) + OS_SEPRATOR + '..' + OS_SEPRATOR + 'files' + OS_SEPRATOR + 'Associations.json'
    init.clean_collection(init.associations_collection, json_file)


@pytest.fixture
def event_loop():
    yield event_loop_for_tests


# def teardown_module(module):
#     """ teardown any state that was previously setup with a setup_module
#     method.
#     """
#     event_loop_for_tests.close()

# @pytest.yield_fixture
# def event_loop():
#     """Create an instance of the default event loop for each test case."""
#     policy = asyncio.get_event_loop_policy()
#     res = policy.new_event_loop()
#     asyncio.set_event_loop(res)
#     res._close = res.close
#     res.close = lambda: None
#
#     yield res
#
#     res._close()
# @pytest.fixture
# def event_loop():
#     yield asyncio.get_event_loop()
#
#
# def pytest_sessionfinish(session, exitstatus):
#     asyncio.get_event_loop().close()


def test_find_association_by_exist_email(event_loop):
    assoc = event_loop.run_until_complete(databaseDB.find_association_by_email(EXIST_EMAIL))
    assert assoc['name'] == 'אור למשפחות'
    assert assoc['email'] == EXIST_EMAIL
    assert assoc['username'] == 'john_doe'


def test_find_association_by_non_exist_email(event_loop):
    assoc = event_loop.run_until_complete(databaseDB.find_association_by_email(NON_EXIST_EMAIL))
    assert assoc is None


def test_login_association_right_email_right_password(event_loop):
    assoc = event_loop.run_until_complete(databaseDB.association_login(EXIST_EMAIL, RIGHT_PASSWORD))
    assert assoc['name'] == 'אור למשפחות'
    assert assoc['email'] == EXIST_EMAIL
    assert assoc['username'] == 'john_doe'


def test_login_association_right_email_wrong_password(event_loop):
    assoc = event_loop.run_until_complete(databaseDB.association_login(EXIST_EMAIL, WRONG_PASSWORD))
    assert assoc is None


def test_login_association_wrong_email_wrong_password(event_loop):
    assoc = event_loop.run_until_complete(databaseDB.association_login(NON_EXIST_EMAIL, WRONG_PASSWORD))
    assert assoc is None


def test_login_association_not_exist_email(event_loop):
    assoc = event_loop.run_until_complete(databaseDB.association_login(NON_EXIST_EMAIL, RIGHT_PASSWORD))
    assert assoc is None


def test_find_all_associations(event_loop):
    association_array = event_loop.run_until_complete(databaseDB.find_associations())
    assert len(association_array) == 5
    assert isinstance(association_array, list)
    assert association_array[0]['name'] == 'אור למשפחות'


def test_find_association_by_exist_username(event_loop):
    assoc = event_loop.run_until_complete(databaseDB.find_association_by_username(EXIST_USERNAME))
    assert assoc['name'] == 'אור למשפחות'
    assert assoc['email'] == EXIST_EMAIL
    assert assoc['username'] == EXIST_USERNAME


def test_find_association_by_non_exist_username(event_loop):
    assoc = event_loop.run_until_complete(databaseDB.find_association_by_username(NON_EXIST_USERNAME))
    assert assoc is None


def test_get_associations_by_exist_category_with_exist_association_in_this_category(event_loop):
    assoc = event_loop.run_until_complete(databaseDB.find_associations_by_category(EXIST_CATEGORY))
    assert isinstance(assoc, list)
    assert len(assoc) == 1


def test_find_associations_by_non_exist_category(event_loop):
    assoc = event_loop.run_until_complete(databaseDB.find_associations_by_category(NON_EXIST_CATEGORY))
    assert isinstance(assoc, list)
    assert len(assoc) == 0


def test_insert_association(event_loop):
    new_assoc = {
        'activity': 'Battered Women',
        'description': '',
        'email': 'test_register_asc@gmail.com',
        'image': [],
        'name': 'John Doe',
        'phone_number': '0555555555',
        'url': 'test_register_asc@gmail.com',
        'password': '123456',
        'donations': 0,
        'username': 'testUser',
        'bank_account': {
            'branch_number': '',
            'account_number': '',
            'bank_name': '',
            'bank_number': ''
        }
    }
    assoc = event_loop.run_until_complete(databaseDB.insert_association(new_assoc))
    assert assoc['email'] == 'test_register_asc@gmail.com'
    association_array = event_loop.run_until_complete(databaseDB.find_associations())
    assert isinstance(association_array, list)
    assert len(association_array) == 6


def test_delete_association_by_email_exists(event_loop):
    new_assoc = {
        'activity': 'Battered Women',
        'description': '',
        'email': 'test_register_asc@gmail.com',
        'image': [],
        'name': 'John Doe',
        'phone_number': '0555555555',
        'url': 'test_register_asc@gmail.com',
        'password': '123456',
        'donations': 0,
        'bank_account': {
            'branch_number': '',
            'account_number': '',
            'bank_name': '',
            'bank_number': ''
        },
        'username': 'testUser'
    }
    response = event_loop.run_until_complete(databaseDB.delete_association_by_email(new_assoc['email']))
    assert response is True


def test_delete_association_by_email_non_exists(event_loop):
    response = event_loop.run_until_complete(databaseDB.delete_association_by_email(NON_EXIST_EMAIL))
    assert response is False


def test_get_random_association_with_number_greather_then_number_of_association(event_loop):
    assoc = event_loop.run_until_complete(databaseDB.get_random_associations(10))
    assert isinstance(assoc, list)
    assert len(assoc) == 5


def test_get_random_asssociation_with_number_until_max_num_of_assocations(event_loop):
    assoc = event_loop.run_until_complete(databaseDB.get_random_associations(5))
    assert isinstance(assoc, list)
    assert len(assoc) == 5


def test_get_random_asssociation_with_number_less_then_zero(event_loop):
    assoc = event_loop.run_until_complete(databaseDB.get_random_associations(-1))
    assert isinstance(assoc, list)
    assert len(assoc) == 0


def test_update_association_details(event_loop):
    response = event_loop.run_until_complete(databaseDB.update_association_details("or4family@gmail.com",
                                                                                   "john_doe", "give@gmail.com",
                                                                                   "0555555556", "new description"))
    assert response is True
    assoc = event_loop.run_until_complete(databaseDB.find_association_by_email(EXIST_EMAIL))
    assert assoc['name'] == 'אור למשפחות'
    assert assoc['email'] == EXIST_EMAIL
    assert assoc['username'] == 'john_doe'
    assert assoc['description'] == "new description"
    assert assoc['phone_number'] == "0555555556"
    assert assoc['url'] == "give@gmail.com"
