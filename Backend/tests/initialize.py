import json
from datetime import datetime
from bson import ObjectId
from Backend.server import database
import pymongo
#database.OUR_DB_NAME = "TairTests"
database.OUR_DB_NAME = "GalTests"
# database.OUR_DB_NAME = "YuvalTests"
# database.OUR_DB_NAME = "MeravTests"

from Backend.server import app
MONGO_CONNECTION_PARAMS = "mongodb+srv://" + str(app.atlas_username) + ":" + str(app.atlas_password) + "@" + str(app.atlas_url) + "/"
mongo_client = pymongo.MongoClient(MONGO_CONNECTION_PARAMS)
our_db = mongo_client[database.OUR_DB_NAME]
products_collection = our_db[database.PRODUCTS_COLLECTION_NAME]
users_collection = our_db[database.USERS_COLLECTION_NAME]
associations_collection = our_db[database.ASCS_COLLECTION_NAME]
events_collection = our_db[database.EVENTS_COLLECTION_NAME]


def fix_file_issues(opened_file):
    """
    This function is fixing the issues in the json files,
    for example the _id contains a dictionary of {"$oid": '43dsff32vsd4324'} instead of just the value
    :param opened_file: the file to fix its issues, already opened and read with json
    :return: the list of object items after the fix
    """
    for obj in opened_file:
        for key,value in obj.items():
            if isinstance(obj[key], dict):
                # All collections, _id field
                if '$oid' in value:
                    obj[key] = ObjectId(value['$oid'])
                # Products collections, publish_at field
                if '$date' in value.keys():
                    obj[key] = datetime.strptime(value['$date'], '%Y-%m-%dT%H:%M:%S.%fZ')
            if isinstance(obj[key], list):
                # Users collection, products_for_sale field
                for child_index in range(len(value)):
                    if '$oid' in value[child_index]:
                        obj[key][child_index] = ObjectId(obj[key][child_index]['$oid'])

    return opened_file


def clean_collection(collection, file_path):
    """
    This function is getting a collection and a file path of a json file
    - reads the file
    - deleting all documents from the collection
    - inserting all the relevant documents read from the file
    :param collection: the collection to init
    :param file_path: the json file path to read
    """
    # Load the json file
    with open(file_path, 'r', encoding='utf-8') as f:
        opened_file = json.load(f)

    # Fix the json file issues
    op_file = fix_file_issues(opened_file)

    # Delete all documents from the collection
    collection.delete_many({})
    # Add file documents to the collection
    collection.insert_many(op_file)
