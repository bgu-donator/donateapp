import os
from uuid import uuid4
from random import randint
import Backend.tests.initialize as init
from fastapi.testclient import TestClient
from Backend.server.app import app
from Backend.server.routers.admin import NOT_EXISTS, SET_ADMIN_KEY, ALREADY_EXISTS, ADDED_SUCCESSFULLY
import pytest
import names
import sys

from Backend.server.routers.routes_config import encrypt_string, ADMIN_COOKIE, NO_COOKIE_FOUND, COOKIE_NAME, \
    SOMETHING_WENT_WRONG

client = TestClient(app)
VALID_ADMIN_EMAIL = 'arnav@gmail.com'
VALID_USER_EMAIL = 'arnav@gmail.com'
INVALID_USER_EMAIL = 'arnav12@gmail.com'
VALID_USER_PASS = '123456'
BASE_PATH = '/admin'
VALID_ASC_USRNM = "john_doe"

OS_SEPRATOR = '/' if 'linux' in sys.platform else '\\'


@pytest.fixture(scope="module", autouse=True)
def pytest_sessionstart(session_mocker):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """
    db_col_to_clean = [
        {
            'name': 'Users.json',
            'collection': init.users_collection
        },{
            'name': 'Associations.json',
            'collection': init.associations_collection
        }]
    for col in db_col_to_clean:
        json_file = str(os.path.dirname(__file__)) + OS_SEPRATOR + '..' + OS_SEPRATOR + 'files' + OS_SEPRATOR + col['name']
        init.clean_collection(col['collection'], json_file)


class FakeDb:
    users_list = []
    admin_list = []
    name = 'test'

    @staticmethod
    async def find_admins():
        return FakeDb.admin_list

    @staticmethod
    async def find_users():
        return FakeDb.users_list

    @staticmethod
    async def find_user_by_email(email):
        for admin in FakeDb.admin_list:
            if admin['email'] == email:
                return admin
        return None

    @staticmethod
    async def set_admin(email: str, change: bool):
        return True if email == VALID_USER_EMAIL else False


def generate_users(num_of_users=3):
    for i in range(num_of_users):
        fname = names.get_first_name()
        lname = names.get_last_name()
        n = 10
        phone = ''.join(["{}".format(randint(0, 9)) for num in range(0, n)])
        FakeDb.users_list.append({
            'name': fname + lname,
            'username': fname + str(i),
            'email': fname + '@gmail.com',
            'password': '123456',
            'phone_number': phone,
            'is_admin': False,
            'id': str(uuid4()),
            'orders': [],
            'products_for_sale': [],
            'total_donation': 0
        })

    while len(FakeDb.admin_list) <= int(num_of_users / 3):
        u = FakeDb.users_list[randint(0, num_of_users - 1)]
        if u not in FakeDb.admin_list:
            u['is_admin'] = True
            FakeDb.admin_list.append(u)

    FakeDb.admin_list[0]['email'] = VALID_ADMIN_EMAIL


def mock_db(monkeypatch):
    return
    import inspect
    list_functions = inspect.getmembers(FakeDb, predicate=inspect.isfunction)
    for func in list_functions:
        monkeypatch.setattr(db, func[0], getattr(FakeDb, func[0]))

    if len(FakeDb.admin_list) == 0 and len(FakeDb.users_list) == 0:
        generate_users(7)


#
# @pytest.fixture(autouse=True)
# def after_each_delete_invalid_user():
#     """
#     This is an after_each function
#     it runs after each test
#     to validate all the function that are called after the yield statement
#     """
#     yield
#     method_specified_test('/users/delete/'+INVALID_USER_EMAIL, method=client.delete, expected_status_code=200, json_data=INVALID_USER_EMAIL)


# @pytest.fixture(autouse=True)
# def before_each_delete_invalid_user():
#     """
#     This is an before_each function
#     it runs before each test
#     to validate all the function that are called before the yield statement
#     """
#     yield


def method_specified_test(url_path, method=client.get, expected_status_code=200, json_data=None,
                          cookies=None, base_url=BASE_PATH):
    """
    This function is getting the url_path for the tested url a method by which to test the url and expected status
    code for the response, and an additional json_data if valid in the url
    :param cookies:
    :param base_url:
    :param url_path: the route to test
    :param method: the method to use in the test
    :param expected_status_code: the expected status code of the response from the url  | DEFAULT = 200
    :param json_data: the aditional json_data to send to the url  | DEFAULT - send no data
    :return: the response from the url for additional assets
    """
    specific_url = str(base_url) + str(url_path)
    response = method(url=specific_url, json=json_data, cookies=cookies)
    assert response.status_code == expected_status_code
    return response


def test_admin_with_no_path(monkeypatch):
    mock_db(monkeypatch)
    # create cookie
    cookie = {ADMIN_COOKIE: encrypt_string(VALID_USER_EMAIL).decode()}
    response = method_specified_test("/", method=client.get, expected_status_code=200, cookies=cookie)
    assert len(response.json()) == 2

    # Validate every admin is also a user
    admin_arr = list(map(lambda admin: admin['email'], response.json()['Admins']))
    user_arr = list(map(lambda user: user['email'], response.json()['Users']))
    admin_arr.sort()
    user_arr.sort()
    test = [user for user in user_arr if user in admin_arr]

    assert test == admin_arr


def test_admin_with_no_path_get_invalid_user():
    cookie = {COOKIE_NAME: encrypt_string(INVALID_USER_EMAIL).decode()}
    response = method_specified_test("/", method=client.get, expected_status_code=500, cookies=cookie)
    assert response.json()['message'] == {'message': NO_COOKIE_FOUND}


def test_admin_with_is_admin_wrong_enc():
    cookie = {ADMIN_COOKIE: INVALID_USER_EMAIL.encode().decode()}
    response = method_specified_test("/", method=client.get, expected_status_code=500, cookies=cookie)
    assert response.json()['message'] == {'message': SOMETHING_WENT_WRONG}



def test_admin_with_no_path_post():
    method_specified_test("/", method=client.post, expected_status_code=405)


def test_admin_get_user_post():
    method_specified_test("/" + VALID_USER_EMAIL, method=client.post, expected_status_code=405)
    method_specified_test("/" + INVALID_USER_EMAIL, method=client.post, expected_status_code=405)


def test_admin_get_user_by_email(monkeypatch):
    mock_db(monkeypatch)
    cookie = {ADMIN_COOKIE: encrypt_string(VALID_USER_EMAIL).decode()}
    method_specified_test("/" + VALID_USER_EMAIL, method=client.get, expected_status_code=200, cookies=cookie)


def test_users_get_user_by_email_not_exists():
    response = method_specified_test("/" + INVALID_USER_EMAIL, method=client.get, expected_status_code=404)
    assert response.json()['message'] == {'message': NO_COOKIE_FOUND}


def test_admin_set_admin_post(monkeypatch):
    mock_db(monkeypatch)
    users_to_set_admin = {VALID_USER_EMAIL: True, INVALID_USER_EMAIL: True}
    users_dict = {VALID_USER_EMAIL: True, INVALID_USER_EMAIL: True}
    response = method_specified_test("/set_admin", method=client.post, expected_status_code=202, json_data=users_dict)
    user_dict = response.json()[SET_ADMIN_KEY]
    assert len(user_dict) == len(users_to_set_admin)
    for user in users_to_set_admin:
        assert user_dict[user] == users_to_set_admin[user]
        get_user_response = client.get('/users/' + user)
        if users_to_set_admin[user]:
            user_by_email = get_user_response.json()['User']
            assert user_by_email['is_admin'] == users_to_set_admin[user]

    # ALll good
    # Return invalid user to not admin


def test_admin_set_admin_get():
    # This is 404 and not 405 since the get {admin_email} is fetching this request and returns 404 not found
    method_specified_test("/set_admin", method=client.get, expected_status_code=404)


def test_register_association_get():
    method_specified_test("register_association", expected_status_code=404)


def test_register_association_post_user_exists(monkeypatch):
    mock_db(monkeypatch)
    register_user = {
        'activity': 'Battered Women',
        'description': '',
        'email': VALID_USER_EMAIL,
        'image': [],
        'name': 'John Doe',
        'phone_number': '0555555555',
        'url': 'give@gmail.com',
        'password': '123456',
        'donations': 0,
        'bank_account': {
            'branch_number': '',
            'account_number': '',
            'bank_name': '',
            'bank_number': ''
        },
        'username': VALID_ASC_USRNM
    }
    response = register_user_func(register_user, status_code=409)
    assert response.json()['message'] == ALREADY_EXISTS


def register_user_func(register_user_dict, status_code=201, use_cookie=True):
    cookie = None
    if use_cookie:
        cookie = {ADMIN_COOKIE: encrypt_string("arnav@gmail.com").decode()}
    return method_specified_test("/register_association", method=client.post, expected_status_code=status_code,
                                 json_data=register_user_dict, cookies=cookie)


def test_register_association_post_user_not_exists(monkeypatch):
    mock_db(monkeypatch)
    register_user = {
        'activity': 'Battered Women',
        'description': '',
        'email': 'shtut1@gmail.com',
        'image': [],
        'name': 'John Doe',
        'phone_number': '0555555555',
        'url': 'test_register_asc@gmail.com',
        'password': '123456',
        'donations': 0,
        'bank_account': {
            'branch_number': '',
            'account_number': '',
            'bank_name': '',
            'bank_number': ''
        },
        'username': 'testUser'
    }
    response = register_user_func(register_user)
    assert response.json()['message'] == ADDED_SUCCESSFULLY
    assert response.json()['code'] == 201
    assert isinstance(response.json()['data'], list)
    assert isinstance(response.json()['data'][0], dict)
    assert response.json()['data'][0]['email'] == register_user['email']

    # Delete the registered user after creating it
    delete_asc(register_user['email'])


def delete_asc(email):
    cookie = {ADMIN_COOKIE: encrypt_string("arnav@gmail.com").decode()}
    deleted = method_specified_test('/delete/association/' + email, method=client.delete, expected_status_code=200,
                                    cookies=cookie)
    assert deleted.json() == 'deleted'


def test_register_association_post_no_cookie(monkeypatch):
    mock_db(monkeypatch)
    register_user = {
        'activity': 'Battered Women',
        'description': '',
        'email': 'shtut1@gmail.com',
        'image': [],
        'name': 'John Doe',
        'phone_number': '0555555555',
        'url': 'test_register_asc@gmail.com',
        'password': '123456',
        'donations': 0,
        'bank_account': {
            'branch_number': '',
            'account_number': '',
            'bank_name': '',
            'bank_number': ''
        },
        'username': 'testUser'
    }
    register_user_func(register_user, status_code=500, use_cookie=False)


def test_delete_user_valid(monkeypatch):
    mock_db(monkeypatch)
    user_to_register = {
        'name': 'shtut',
        'username': 'shtut',
        'email': 'blabla@gmail.com',
        'password': '123456'
    }
    method_specified_test('/register', method=client.post, expected_status_code=201,
                          json_data=user_to_register, base_url='/users')

    cookie = {ADMIN_COOKIE: encrypt_string(VALID_USER_EMAIL).decode()}
    method_specified_test('/delete/' + 'blabla@gmail.com', method=client.delete,
                          expected_status_code=200, cookies=cookie)


def test_delete_user_invalid_no_cookie(monkeypatch):
    mock_db(monkeypatch)
    user_to_register = {
        'name': 'shtut',
        'username': 'shtut',
        'email': 'blabla@gmail.com',
        'password': '123456'
    }
    method_specified_test('/register', method=client.post, expected_status_code=201,
                          json_data=user_to_register, base_url='/users')

    method_specified_test('/delete/' + 'blabla@gmail.com', method=client.delete,
                          expected_status_code=500)
    cookie = {ADMIN_COOKIE: encrypt_string(VALID_USER_EMAIL).decode()}
    method_specified_test('/delete/' + 'blabla@gmail.com', method=client.delete,
                          expected_status_code=200, cookies=cookie)


def test_delete_user_invalid_user(monkeypatch):
    mock_db(monkeypatch)
    cookie = {ADMIN_COOKIE: encrypt_string(VALID_USER_EMAIL).decode()}
    method_specified_test('/delete/' + 'blabla@gmail.com', method=client.delete,
                          expected_status_code=500,cookies=cookie)


def test_delete_ass_valid(monkeypatch):
    mock_db(monkeypatch)
    register_ass = {
        "name": "אור למשפחות",
        "username": "john_doe",
        "password": "123456",
        "email": "or4family@gmail.com",
        "phone_number": "0555555555",
        "url": "give@gmail.com",
        'donations': 0,
        "bank_account": {
            "branch_number": "123",
            "account_number": "123456",
            "bank_name": "Hapoalim",
            "bank_number": "12"
        },
        "activity": "כוחות הבטחון",
        "image": [
            "https://yt3.ggpht.com/ytc/AAUvwnjiTYkzY6-wtwulLBcXs3uazyw2OzHiGScjCSiE=s900-c-k-c0x00ffffff-no-rj"
        ],
        "description": "עמותת אור למשפחות הוקמה בשנת 2008 במטרה לחזק הורים שאיבדו את ילדיהם\n\nבמערכות ישראל באמצעות חוויות מקרבות ומעצימות. העמותה חוללה מהפיכה בתפיסת\n\nהשכול בישראל ועל כך קיבלה את\nאות נשיא המדינה למתנדב לשנת 2013\nאות המופת כנס שדרות לחברה לשנת 2018\nאות פול האריס על תרומה למען הקהילה והחברה 2019\n\nאלפי הורים חברים היום באור למשפחות. שואבים כוח, ובוחרים בחיים.\n\nהעמותה הוקמה על ידי עירית אורן גונדרס, סגן אלוף במיל’ וראש ענף כוח אדם בחיל\n\nהנדסה קרבית, יו”ר ומנכ”ל העמותה. עירית פועלת מביתה בהתנדבות מלאה וללא לאות לקידומה \n\nוהרחבתה של העמותה ורואה בה מפעל חיים בעל חשיבות עליונה. לשליחות זו נרתמו\n\nכל בני משפחתה וחבריה שדבקים עמה במשימה במסירות אין קץ."
    }

    cookie = {ADMIN_COOKIE: encrypt_string(VALID_USER_EMAIL).decode()}
    method_specified_test('/delete/association/' + 'or4family@gmail.com', method=client.delete,
                          expected_status_code=200, cookies=cookie)
    register_user_func(register_ass, status_code=201)


def test_delete_ass_invalid_no_cookie(monkeypatch):
    mock_db(monkeypatch)
    response = method_specified_test('/delete/association/' + 'or4family@gmail.com', method=client.delete,
                          expected_status_code=404)
    assert (response, 'An error occurred.')

def test_delete_ass_invalid_ass_mail(monkeypatch):
    mock_db(monkeypatch)
    cookie = {ADMIN_COOKIE: encrypt_string(VALID_USER_EMAIL).decode()}
    response = method_specified_test('/delete/association/' + 'blabla@gmail.com', method=client.delete,
                          expected_status_code=404, cookies=cookie)
    assert (response,'Association not found')
