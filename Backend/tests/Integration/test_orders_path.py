import Backend.tests.initialize as init
from fastapi.testclient import TestClient
from Backend.server.app import app
import pytest
import os
import sys

client = TestClient(app)
VALID_USER_EMAIL = 'arnav@gmail.com'
INVALID_USER_EMAIL = 'not_exists@nope.com'
VALID_USER_PASS = '123456'
BASE_URL = '/orders/'

OS_SEPRATOR = '/' if 'linux' in sys.platform else '\\'

@pytest.fixture(scope="module", autouse=True)
def pytest_sessionstart(session_mocker):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """
    db_col_to_clean = [
        {
            'name': 'Users.json',
            'collection': init.users_collection
        },{
            'name': 'Products.json',
            'collection': init.products_collection
        }]
    for col in db_col_to_clean:
        json_file = str(os.path.dirname(__file__)) + OS_SEPRATOR + '..' + OS_SEPRATOR + 'files' + OS_SEPRATOR + col['name']
        init.clean_collection(col['collection'], json_file)


order_with_exist_users_and_for_sale_product = {
    "product_id": "606c81e24fc44f676ade5bcf",
    "date": "2021-05-01 12:22",
    "buyer_email": "arnav12@gmail.com",
    "seller_email": "arnav@gmail.com",
    "association_username": "shanti",
}

order_with_exist_users_not_exist_product = {
    "product_id": "5ff4b1",
    "date": "2021-05-01 12:22",
    "buyer_email": "galrosenthal@gmail.com",
    "seller_email": "arnav@gmail.com",
    "association_username": "shanti",
}

order_with_exist_product_not_exist_seller_user = {
    "product_id": "5ff71fa409ca7a36f17525cd",
    "date": "2021-05-01 12:22",
    "buyer_email": "galrosenthal@gmail.com",
    "seller_email": "arnav121232@gmail.com",
    "association_username": "shanti",
}

order_with_exist_product_not_exist_buyer_user = {
    "product_id": "5ff71fa409ca7a36f17525cd",
    "date": "2021-05-01 12:22",
    "buyer_email": "galrosenthal121232@gmail.com",
    "seller_email": "arnav@gmail.com",
    "association_username": "shanti",
}

order_with_asc_not_exist = {
    "product_id": "5ff71fa409ca7a36f17525cd",
    "date": "2021-05-01 12:22",
    "buyer_email": "galrosenthal@gmail.com",
    "seller_email": "arnav@gmail.com",
    "association_username": "LALA",
}

order_with_exist_users_and_sold_product = {
    "product_id": "606c7f2c4fc44f676ade5bce",
    "date": "2021-05-01 12:22",
    "buyer_email": "galrosenthal@gmail.com",
    "seller_email": "arnav@gmail.com",
    "association_username": "shanti",
}



def method_specified_test(url_path, method=client.get, expected_status_code=200, json_data=None, cookie=None):
    """
    This function is getting the url_path for the tested url a method by which to test the url and expected status
    code for the response, and an additional json_data if valid in the url
    :param url_path: the route to test
    :param method: the method to use in the test
    :param expected_status_code: the expected status code of the response from the url
    :param json_data: the aditional json_data to send to the url
    :return: the response from the url for additional assets
    """
    response = method(url=BASE_URL + url_path, json=json_data, cookies=cookie)
    assert response.status_code == expected_status_code
    return response


def test_create_order_with_exist_users_and_for_sale_product():
    method_specified_test("create_order", method=client.post, json_data=
    order_with_exist_users_and_for_sale_product
                                     , expected_status_code=201)

def test_create_order_with_exist_users_not_exist_product():
    method_specified_test("create_order", method=client.post,
                                     json_data=order_with_exist_users_not_exist_product
                                     ,expected_status_code=409)

def test_create_order_with_exist_product_not_exist_seller_user():
    method_specified_test("create_order", method=client.post, json_data=
    order_with_exist_product_not_exist_seller_user
                                     , expected_status_code=409)

def test_create_order_with_exist_product_not_exist_buyer_user():
    method_specified_test("create_order", method=client.post, json_data=
    order_with_exist_product_not_exist_buyer_user
                                     , expected_status_code=409)

def test_create_order_with_asc_not_exist():
    method_specified_test("create_order", method=client.post, json_data=
    order_with_asc_not_exist, expected_status_code=409)

def test_create_order_with_exist_users_and_sold_product():
    method_specified_test("create_order", method=client.post, json_data=
    order_with_exist_users_and_sold_product
                                     , expected_status_code=409)
