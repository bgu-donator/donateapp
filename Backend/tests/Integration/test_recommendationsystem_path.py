import os
import Backend.tests.initialize as init
from fastapi.testclient import TestClient
from Backend.server.app import app
from Backend.server.routers.products import PRODUCT_DOES_NOT_EXISTS, ADDED_SUCCESSFULLY, NO_COOKIE_FOUND, \
    SOMETHING_WENT_WRONG, NO_PRODUCT_WITH_THIS_ID, NO_ID_BELONGS_TO_THIS_USER, MUST_BE_GREATER_THEN_ZERO
from Backend.server.routers.routes_config import encrypt_string, COOKIE_NAME
import pytest
import json
import sys

client = TestClient(app)
VALID_USER_EMAIL = 'arnav@gmail.com'
INVALID_USER_EMAIL = 'arnav12@gmail.com'
VALID_USER_PASS = '123456'

BASE_URL = '/product/'

fake_prod = {
    'id': '5ff71fa409ca7a36f17525cd',
    "product_name": "מחשב",
    "description": "מחשב חדש דנדש",
    "price": 3500,
    "main_category": "אלקטרוניקה",
    "city": "מיתר",
    'status': 'FOR_SALE',
    "phone_number": "(050) 848 - 2761",
    "seller_email": VALID_USER_EMAIL,
    "sub_category": ["מחשב-נייד", "מחשב-נייח"],
}
OS_SEPRATOR = '/' if 'linux' in sys.platform else '\\'
json_file = str(os.path.dirname(__file__)) + OS_SEPRATOR + '..' + OS_SEPRATOR + 'files' + OS_SEPRATOR + 'Products.json'


@pytest.fixture(scope="module", autouse=True)
def pytest_sessionstart(session_mocker):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """

    init.clean_collection(init.products_collection, json_file)



fake_prod_to_delete = {
    'id': '',
}

fake_prod_to_insert = {
    "product_name": "מחשב",
    "description": "מחשב חדש דנדש",
    "price": 3500,
    "main_category": "אלקטרוניקה",
    "city": "מיתר",
    'status': 'FOR_SALE',
    "phone_number": "(050) 848 - 2761",
    "seller_email": VALID_USER_EMAIL,
    "sub_category": ["מחשב-נייד", "מחשב-נייח"],
}

product_id_update_status = {
    "p_id": "5ff71fa409ca7a36f17525cd"
}

product_id_not_exist_update_status = {
    "p_id": "2342342"
}


class FakeDb:
    prod_list = [fake_prod_to_insert]
    name = 'test'

    @staticmethod
    async def get_all_products():
        return FakeDb.prod_list

    @staticmethod
    async def get_product_by_id(prod_id):
        for p in FakeDb.prod_list:
            if p['id'] == prod_id:
                return p
        return None

    @staticmethod
    async def insert_product(prod: dict):
        prod['id'] = fake_prod_to_insert['id']
        FakeDb.prod_list.append(prod)
        return prod

    @staticmethod
    async def delete_product_by_id(p_id):
        FakeDb.prod_list = [p for p in FakeDb.prod_list if p['id'] != p_id]

    @staticmethod
    async def get_random_products(num):
        return FakeDb.prod_list[:num]


def mock_db(monkeypatch):
    return
    import inspect
    list_functions = inspect.getmembers(FakeDb, predicate=inspect.isfunction)
    for func in list_functions:
        monkeypatch.setattr(db, func[0], getattr(FakeDb, func[0]))


def method_specified_test(url_path, method=client.get, expected_status_code=200, json_data=None, cookies=None):
    """
    This function is getting the url_path for the tested url a method by which to test the url and expected status
    code for the response, and an additional json_data if valid in the url
    :param url_path: the route to test
    :param method: the method to use in the test
    :param expected_status_code: the expected status code of the response from the url
    :param json_data: the aditional json_data to send to the url
    :param cookies: the additional cookies to insert the request headers
    :return: the response from the url for additional assets
    """
    response = method(url=BASE_URL + url_path, json=json_data, cookies=cookies)
    assert response.status_code == expected_status_code
    return response


def test_get_recommendation_by_main_category_there_is_products_with_connected_categories_in_db():
    main_category = "בריאות ויופי"
    response = method_specified_test("recommendation/"+main_category, method=client.get, expected_status_code=200)
    products_array = response.json()['Product']
    assert len(products_array) == 1
    assert isinstance(products_array, list)
    assert products_array[0]['main_category'] == 'לבוש ואביזרים'


def test_get_recommendation_by_main_category_dont_have_products_with_connected_categories_in_db():
    main_category = "אלקטרוניקה"
    response = method_specified_test("recommendation/"+main_category, method=client.get, expected_status_code=200)
    products_array = response.json()['Product']
    assert len(products_array) == 2
    assert isinstance(products_array, list)

