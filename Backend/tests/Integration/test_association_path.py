import os
import Backend.tests.initialize as init
from fastapi.testclient import TestClient
from Backend.server.app import app
from Backend.server.routers.association import UNAUTHORIZED_ERROR, USERNAME_IS_NOT_EXISTS, MUST_BE_GREATER_THEN_ZERO
import pytest
import sys
from Backend.server.routers.routes_config import encrypt_string, ADMIN_COOKIE

client = TestClient(app)
VALID_USER_EMAIL = 'or4family@gmail.com'
INVALID_USER_EMAIL = 'arnav12@gmail.com'
VALID_USER_PASS = '123456'
INVALID_USER_PASS = '1234567'
BASE_URL = '/association/'

OS_SEPRATOR = '/' if 'linux' in sys.platform else '\\'


@pytest.fixture(scope="module", autouse=True)
def pytest_sessionstart(session_mocker):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """
    json_file = str(os.path.dirname(__file__)) + OS_SEPRATOR + '..' + OS_SEPRATOR + 'files' + OS_SEPRATOR + 'Associations.json'
    init.clean_collection(init.associations_collection, json_file)


VALID_ASC_USRNM = "john_doe"
VALID_ASC_EMAIL = "or4family@gmail.com"

fake_assc = {
    "_id": "5ff37a3ba2f287a7e125ff89",
    "name": "John Doe",
    "username": VALID_ASC_USRNM,
    "password": "123456",
    "email": VALID_USER_EMAIL,
    "phone_number": "0555555555",
    "url": "give@gmail.com",
    "bank_account": {
        "branch_number": "123",
        "account_number": "123456",
        "bank_name": "Hapoalim",
        "bank_number": "12"
    },
    "activity": "Army",
    "image": [],
    "description": "Fake Association for tests"
}


# @pytest.fixture(autouse=True)
# def after_each_delete_invalid_user():
#     """
#     This is an after_each function
#     it runs after each test
#     to validate all the function that are called after the yield statement
#     """
#     yield
#     cookie = {ADMIN_COOKIE: encrypt_string(VALID_USER_EMAIL).decode()}
#     method_specified_test('/users/delete/'+INVALID_USER_EMAIL, method=client.delete,
#                           expected_status_code=200, json_data=INVALID_USER_EMAIL,
#                           cookie=cookie)


# @pytest.fixture(autouse=True)
# def before_each_set_mock_db(monkeypatch):
#     """
#     This is an before_each function
#     it runs before each test
#     to validate all the function that are called before the yield statement
#     """
#     mock_db(monkeypatch)
#     yield


class FakeDb:
    asc_list = [fake_assc]
    name = 'test'

    @staticmethod
    async def find_associations():
        return FakeDb.asc_list

    @staticmethod
    async def insert_association(new_association: dict):
        if new_association['username'] != VALID_ASC_USRNM:
            FakeDb.asc_list.append(new_association)
            return new_association
        else:
            return None

    @staticmethod
    async def find_association_by_username(username):
        return fake_assc if username == VALID_ASC_USRNM else None

    @staticmethod
    async def find_user_by_email(email):
        return fake_assc if email == VALID_USER_EMAIL else None

    @staticmethod
    async def delete_association_by_email(email):
        FakeDb.asc_list = [asc for asc in FakeDb.asc_list if asc['email'] != email]
        return 'deleted'

    @staticmethod
    async def get_random_associations(num):
        return FakeDb.asc_list[:num]


def mock_db(monkeypatch):
    return
    import inspect
    list_functions = inspect.getmembers(FakeDb, predicate=inspect.isfunction)
    for func in list_functions:
        monkeypatch.setattr(db, func[0], getattr(FakeDb, func[0]))


def method_specified_test(url_path, method=client.get, expected_status_code=200, base_URL=BASE_URL,
                          json_data=None, cookies=None):
    """
    This function is getting the url_path for the tested url a method by which to test the url and expected status
    code for the response, and an additional json_data if valid in the url
    :param base_URL:
    :param cookies:
    :param url_path: the route to test
    :param method: the method to use in the test
    :param expected_status_code: the expected status code of the response from the url
    :param json_data: the aditional json_data to send to the url
    :return: the response from the url for additional assets
    """
    response = method(url=base_URL + url_path, json=json_data, cookies=cookies)
    assert response.status_code == expected_status_code
    return response


def register_user_func(register_user_dict, status_code=201):
    cookie = {ADMIN_COOKIE: encrypt_string("arnav@gmail.com").decode()}
    result = method_specified_test("register_association", method=client.post, expected_status_code=status_code,
                                   base_URL='/admin/', json_data=register_user_dict, cookies=cookie)
    return result


def delete_asc(email):
    cookie = {ADMIN_COOKIE: encrypt_string("arnav@gmail.com").decode()}
    deleted = method_specified_test('delete/association/' + email, method=client.delete, expected_status_code=200,
                                    cookies=cookie, base_URL='/admin/')
    assert deleted.json() == 'deleted'


def test_get_all_associations(monkeypatch):
    mock_db(monkeypatch)
    response = method_specified_test("", method=client.get, expected_status_code=200)
    association_array = response.json()['Association']
    assert len(association_array) == 5
    assert isinstance(association_array, list)
    assert association_array[0]['name'] == 'אור למשפחות'


def test_get_all_associations_expected_values(monkeypatch):
    mock_db(monkeypatch)
    response = method_specified_test("", method=client.get, expected_status_code=200)
    association_array = response.json()['Association']
    assert len(association_array) == 5
    assert isinstance(association_array, list)
    assert association_array[0]['name'] == 'אור למשפחות'
    assert association_array[1]['name'] == 'בית השנטי'
    assert association_array[2]['name'] == 'ויצו ישראל'
    assert association_array[3]['name'] == 'לוחמים ללא גבולות'
    assert association_array[4]['name'] == 'פעמונים ארגון חסד'


def test_post_all_associations():
    method_specified_test("", method=client.post, expected_status_code=405)


def test_post_get_association():
    method_specified_test("john_doe", method=client.post, expected_status_code=405)


def test_get_association_user_exists(monkeypatch):
    mock_db(monkeypatch)
    response = method_specified_test( VALID_ASC_USRNM, expected_status_code=200)
    assc = response.json()['Association']
    expected_assc = {'activity': 'כוחות הבטחון',
                     'email': 'or4family@gmail.com',
                     'image': [
                         'https://yt3.ggpht.com/ytc/AAUvwnjiTYkzY6-wtwulLBcXs3uazyw2OzHiGScjCSiE=s900-c-k-c0x00ffffff-no-rj'],
                     'name': 'אור למשפחות',
                     'phone_number': '0555555555',
                     'url': 'give@gmail.com',
                     'username': VALID_ASC_USRNM}
    assert isinstance(assc, dict)
    assert expected_assc == {
        'activity': assc['activity'],
        'email': assc['email'],
        'image': assc['image'],
        'name': assc['name'],
        'phone_number': assc['phone_number'],
        'url': assc['url'],
        'username': assc['username']

    }


def test_get_association_user_not_exists():
    response = method_specified_test("john_doe2", expected_status_code=404)
    assert response.json()['message'] == USERNAME_IS_NOT_EXISTS

def test_get_association_user_exists_by_email(monkeypatch):
    mock_db(monkeypatch)
    response = method_specified_test('get_association_by_email/'+ VALID_ASC_EMAIL, expected_status_code=200)
    assc = response.json()['Association']
    expected_assc = {'activity': 'כוחות הבטחון',
                     'email': 'or4family@gmail.com',
                     'name': 'אור למשפחות',
                     'phone_number': '0555555555',
                     'url': 'give@gmail.com',
                     'username': VALID_ASC_USRNM}
    assert isinstance(assc, dict)
    assert expected_assc == {
        'activity': assc['activity'],
        'email': assc['email'],
        'name': assc['name'],
        'phone_number': assc['phone_number'],
        'url': assc['url'],
        'username': assc['username']

    }


def test_get_association_user_not_exists_by_email():
    method_specified_test("get_association_by_email/"+INVALID_USER_EMAIL, method=client.get, expected_status_code=404)


def test_random_association_number_less_then_one():
    response = method_specified_test("get_random_associations/0", method=client.get, expected_status_code=406)
    assert response.json() == MUST_BE_GREATER_THEN_ZERO


def test_random_association_number_bigger_then_assoc_in_db():
    random_associations = method_specified_test("get_random_associations/9", method=client.get, expected_status_code=200)

    rand_ascs = random_associations.json()['Association']

    assert isinstance(rand_ascs, list)

    assert len(rand_ascs) < 9

def test_random_association_post():
    method_specified_test("get_random_associations/2", method=client.post, expected_status_code=405)

def test_random_association_number_equal_to_assoc_in_db():
    random_associations = method_specified_test("get_random_associations/5", method=client.get, expected_status_code=200)
    association_array = random_associations.json()['Association']
    assert len(association_array) == 5
    assert isinstance(association_array, list)
    assert association_array[0]['name'] != (association_array[1]['name'] or association_array[2]['name'] or
                                            association_array[3]['name'] or association_array[4]['name'])
    assert association_array[1]['name'] != (association_array[0]['name'] or association_array[2]['name'] or
                                            association_array[3]['name'] or association_array[4]['name'])
    assert association_array[2]['name'] != (association_array[1]['name'] or association_array[0]['name'] or
                                            association_array[3]['name'] or association_array[4]['name'])
    assert association_array[3]['name'] != (association_array[1]['name'] or association_array[2]['name'] or
                                            association_array[0]['name'] or association_array[4]['name'])
    assert association_array[4]['name'] != (association_array[1]['name'] or association_array[2]['name'] or
                                            association_array[3]['name'] or association_array[0]['name'])


def test_insert_assoc_and_get_random_association(monkeypatch):
    mock_db(monkeypatch)
    # INIT DB with 2 associations
    register_user = {
        'activity': 'Battered Women',
        'description': '',
        'email': 'shtut1@gmail.com',
        'image': [],
        'name': 'John Doe',
        'phone_number': '0555555555',
        'url': 'test_register_asc@gmail.com',
        'password': '123456',
        'donations': 0,
        'bank_account': {
            'branch_number': '',
            'account_number': '',
            'bank_name': '',
            'bank_number': ''
        },
        'username': 'testUser'
    }
    response = register_user_func(register_user)
    assert response.status_code == 201

    random_associations = method_specified_test("get_random_associations/6", method=client.get,
                                                expected_status_code=200)

    rand_ascs = random_associations.json()['Association']
    assert isinstance(rand_ascs, list)
    assert len(rand_ascs) == 6
    assert isinstance(rand_ascs[0], dict)
    assert isinstance(rand_ascs[1], dict)
    assert len([asc for asc in rand_ascs if asc['email'] == register_user['email']]) == 1
    assert len([asc for asc in rand_ascs if asc['email'] == VALID_USER_EMAIL]) == 1

    delete_asc(register_user['email'])

    response = method_specified_test("get_random_associations/-2", method=client.get, expected_status_code=406)
    assert response.json() == MUST_BE_GREATER_THEN_ZERO

    response = method_specified_test("get_random_associations/15.5", method=client.get, expected_status_code=422)


def test_users_login_get():
    method_specified_test("login", method=client.get, expected_status_code=404)


def test_users_login_post_succeed(monkeypatch):
    mock_db(monkeypatch)
    assoc_login = {
        'email': VALID_USER_EMAIL,
        'password': VALID_USER_PASS
    }
    response = method_specified_test("login", method=client.post, json_data=assoc_login)
    assert response.json() == assoc_login['email']


def test_users_login_post_fail_invalid_email(monkeypatch):
    mock_db(monkeypatch)
    assoc_login = {
        'email': INVALID_USER_EMAIL,
        'password': VALID_USER_PASS
    }
    response = method_specified_test("login", method=client.post, json_data=assoc_login, expected_status_code=401)
    assert response.json() == UNAUTHORIZED_ERROR


def test_users_login_post_fail_invalid_pass(monkeypatch):
    mock_db(monkeypatch)
    assoc_login = {
        'email': VALID_ASC_EMAIL,
        'password': INVALID_USER_PASS
    }
    response = method_specified_test("login", method=client.post, json_data=assoc_login, expected_status_code=401)
    assert response.json() == UNAUTHORIZED_ERROR


def test_users_login_post_fail_invalid_pass_invalid_email(monkeypatch):
    mock_db(monkeypatch)
    assoc_login = {
        'email': INVALID_USER_EMAIL,
        'password': INVALID_USER_PASS
    }
    response = method_specified_test("login", method=client.post, json_data=assoc_login, expected_status_code=401)
    assert response.json() == UNAUTHORIZED_ERROR


def test_get_associations_by_one_category(monkeypatch):
    mock_db(monkeypatch)
    CATEGORY = "כוחות הבטחון"
    response = method_specified_test("get_associations_by_category/" + CATEGORY, method=client.get, expected_status_code=200)
    association_array = response.json()['Association']
    assert len(association_array) == 1
    assert isinstance(association_array, list)
    assert association_array[0]['name'] == 'אור למשפחות'


def test_get_associations_by_two_categories(monkeypatch):
    mock_db(monkeypatch)
    CATEGORY = "כוחות הבטחון&רווחה"
    response = method_specified_test("get_associations_by_category/" + CATEGORY, method=client.get, expected_status_code=200)
    association_array = response.json()['Association']
    assert len(association_array) == 2
    assert isinstance(association_array, list)
    assert association_array[0]['name'] == 'אור למשפחות' or association_array[1]['name'] =='אור למשפחות'
    assert association_array[0]['name'] == 'פעמונים ארגון חסד' or association_array[1]['name'] =='פעמונים ארגון חסד'


def test_get_associations_by_not_exist_category(monkeypatch):
    mock_db(monkeypatch)
    INVALID_CATEGORY = "לא קיים"
    response = method_specified_test("get_associations_by_category/" + INVALID_CATEGORY, method=client.get, expected_status_code=200)
    association_array = response.json()['Association']
    assert len(association_array) == 0


# @router.patch("/update", response_description="update association details")
# async def change_association_details(change_details: ChangeAssociationDetailsSchema, req: Request, res: Response):
#     try:
#
#         association_email_from_cookie = req.cookies[COOKIE_NAME]
#         logger.info("fetching encrypted email from cookie " + str(association_email_from_cookie))
#         if not association_email_from_cookie:
#             res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
#             return {'message': SOMETHING_WENT_WRONG}
#         logger.info("got encrypted email from cookie " + str(association_email_from_cookie))
#         logger.info("try decrypting email from cookie")
#         association_email = decrypt_string(association_email_from_cookie.encode())
#         if association_email is None:
#             res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
#             return {'message': SOMETHING_WENT_WRONG}
#         logger.info("decrypted email from cookie " + str(association_email))
#         logger.info("check email match with cookie")
#         if association_email == change_details.email:
#             logger.info("update user details for: " + str(association_email))
#             updated = await db.update_association_details(change_details.email, change_details.username,
#                                                           change_details.url, change_details.phone_number,
#                                                           change_details.description)
#             if updated:
#                 logger.info("updated success " + str(association_email))
#                 res.status_code = status.HTTP_202_ACCEPTED
#                 return 'updated'
#             else:
#                 logger.info("update user details unsuccessful: " + str(association_email))
#                 res.status_code = status.HTTP_451_UNAVAILABLE_FOR_LEGAL_REASONS
#                 return
#         else:
#             logger.info("emails not match to cookie : " + str(association_email))
#             res.status_code = status.HTTP_409_CONFLICT
#
#     except Exception as e: # pragma: no cover
#         logger.error(e)
#         res.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
#         return ErrorResponseModel("An error occurred.", status.HTTP_500_INTERNAL_SERVER_ERROR)
