from fastapi.testclient import TestClient
from Backend.server.app import app
from Backend.server.routers.payRoutes import FAILED_PAYMENT

client = TestClient(app)


# @pytest.fixture(autouse=True)
# def after_each_delete_invalid_user():
#     """
#     This is an after_each function
#     it runs after each test
#     to validate all the function that are called after the yield statement
#     """
#     yield
#

# @pytest.fixture(autouse=True)
# def before_each_delete_invalid_user():
#     """
#     This is an before_each function
#     it runs before each test
#     to validate all the function that are called before the yield statement
#     """
#     yield


def method_specified_test(url_path, method=client.get, expected_status_code=200, json_data=None):
    """
    This function is getting the url_path for the tested url a method by which to test the url and expected status
    code for the response, and an additional json_data if valid in the url
    :param url_path: the route to test
    :param method: the method to use in the test
    :param expected_status_code: the expected status code of the response from the url
    :param json_data: the aditional json_data to send to the url
    :return: the response from the url for additional assets
    """
    response = method(url_path, json=json_data)
    assert response.status_code == expected_status_code
    return response


def test_pay_get():
    method_specified_test("/pay/", method=client.get, expected_status_code=405)


def test_pay_post():
    method_specified_test("/pay/", method=client.post, expected_status_code=202)


def test_pay_fail_get():
    method_specified_test("/pay/fail", method=client.get, expected_status_code=405)


def test_pay_fail_post():
    response = method_specified_test("/pay/fail", method=client.post, expected_status_code=402)
    assert 'message' in response.json()
    assert response.json()['message'] == FAILED_PAYMENT
