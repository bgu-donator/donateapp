import os
import Backend.tests.initialize as init
from fastapi.testclient import TestClient
from Backend.server.app import app
from Backend.server.routers.products import PRODUCT_DOES_NOT_EXISTS, ADDED_SUCCESSFULLY, NO_COOKIE_FOUND, \
    SOMETHING_WENT_WRONG, NO_PRODUCT_WITH_THIS_ID, NO_ID_BELONGS_TO_THIS_USER, MUST_BE_GREATER_THEN_ZERO
from Backend.server.routers.routes_config import encrypt_string, COOKIE_NAME
import pytest
import json
import sys

client = TestClient(app)
VALID_USER_EMAIL = 'arnav@gmail.com'
INVALID_USER_EMAIL = 'arnav12@gmail.com'
VALID_USER_PASS = '123456'

BASE_URL = '/product/'

fake_prod = {
    'id': '5ff71fa409ca7a36f17525cd',
    "product_name": "מחשב",
    "description": "מחשב חדש דנדש",
    "price": 3500,
    "main_category": "אלקטרוניקה",
    "city": "מיתר",
    'status': 'FOR_SALE',
    "phone_number": "(050) 848 - 2761",
    "seller_email": VALID_USER_EMAIL,
    "sub_category": ["מחשב-נייד", "מחשב-נייח"],
}
OS_SEPRATOR = '/' if 'linux' in sys.platform else '\\'
json_file = str(os.path.dirname(__file__)) + OS_SEPRATOR + '..' + OS_SEPRATOR + 'files' + OS_SEPRATOR + 'Products.json'


@pytest.fixture(scope="module", autouse=True)
def pytest_sessionstart(session_mocker):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """

    db_col_to_clean = [
        {
            'name': 'Users.json',
            'collection': init.users_collection
        },{
            'name': 'Products.json',
            'collection': init.products_collection
        }]
    for col in db_col_to_clean:
        json_file = str(os.path.dirname(__file__)) + OS_SEPRATOR + '..' + OS_SEPRATOR + 'files' + OS_SEPRATOR + col['name']
        init.clean_collection(col['collection'], json_file)


fake_prod_to_delete = {
    'id': '',
}

fake_prod_to_insert = {
    "product_name": "מחשב",
    "description": "מחשב חדש דנדש",
    "price": 3500,
    "main_category": "אלקטרוניקה",
    "city": "מיתר",
    'status': 'FOR_SALE',
    "phone_number": "(050) 848 - 2761",
    "seller_email": VALID_USER_EMAIL,
    "sub_category": ["מחשב-נייד", "מחשב-נייח"],
}

product_id_update_status = {
    "p_id": "5ff71fa409ca7a36f17525cd"
}

product_id_not_exist_update_status = {
    "p_id": "2342342"
}


class FakeDb:
    prod_list = [fake_prod_to_insert]
    name = 'test'

    @staticmethod
    async def get_all_products():
        return FakeDb.prod_list

    @staticmethod
    async def get_product_by_id(prod_id):
        for p in FakeDb.prod_list:
            if p['id'] == prod_id:
                return p
        return None

    @staticmethod
    async def insert_product(prod: dict):
        prod['id'] = fake_prod_to_insert['id']
        FakeDb.prod_list.append(prod)
        return prod

    @staticmethod
    async def delete_product_by_id(p_id):
        FakeDb.prod_list = [p for p in FakeDb.prod_list if p['id'] != p_id]

    @staticmethod
    async def get_random_products(num):
        return FakeDb.prod_list[:num]


def mock_db(monkeypatch):
    return
    import inspect
    list_functions = inspect.getmembers(FakeDb, predicate=inspect.isfunction)
    for func in list_functions:
        monkeypatch.setattr(db, func[0], getattr(FakeDb, func[0]))


def method_specified_test(url_path, method=client.get, expected_status_code=200, json_data=None, cookies=None):
    """
    This function is getting the url_path for the tested url a method by which to test the url and expected status
    code for the response, and an additional json_data if valid in the url
    :param url_path: the route to test
    :param method: the method to use in the test
    :param expected_status_code: the expected status code of the response from the url
    :param json_data: the aditional json_data to send to the url
    :param cookies: the additional cookies to insert the request headers
    :return: the response from the url for additional assets
    """
    response = method(url=BASE_URL + url_path, json=json_data, cookies=cookies)
    assert response.status_code == expected_status_code
    return response


def test_get_all_products():
    response = method_specified_test("", method=client.get, expected_status_code=200)
    products_array = response.json()['Products']
    assert len(products_array) == 4
    assert isinstance(products_array, list)
    assert products_array[0]['product_name'] == 'מחשב'


def test_post_all_products():
    method_specified_test("", method=client.post, expected_status_code=405)


def test_post_get_product():
    method_specified_test("john_doe", method=client.post, expected_status_code=405)


def test_get_product_exists(monkeypatch):
    mock_db(monkeypatch)
    # global fake_prod
    with open(json_file, 'r', encoding='utf-8') as f:
        prod_list = json.load(f)

    prod_list = init.fix_file_issues(prod_list)
    fake_prod = prod_list[0]
    fake_prod['id'] = str(fake_prod.pop('_id'))
    # remove the publish_at field since it is not comparable with the retrieved document
    fake_prod.pop('publish_at')
    response = method_specified_test(fake_prod['id'], expected_status_code=200)
    prod = response.json()['Products']
    expected_prod = fake_prod
    assert isinstance(prod, dict)
    # assert that the product retrieved contains all the keys in the expected product
    assert all(elem in prod.keys() for elem in expected_prod.keys())
    assert prod_in_returned_prod(expected=expected_prod, actual=prod)


def test_get_product_not_exists(monkeypatch):
    mock_db(monkeypatch)
    response = method_specified_test("6054d3d2a6e7162c90bb79f1", expected_status_code=404)
    assert response.json()['message'] == PRODUCT_DOES_NOT_EXISTS


def test_insert_product_get():
    method_specified_test("add_product", expected_status_code=404)


def prod_in_returned_prod(expected: dict, actual: dict):
    """
    This function gets 2 dictionaries of product
    and returns True if they contains the same keys and values
    :param expected: the expected dictionary that should be generated hard coded
    :param actual: the actual dictionary retrieved from mongo
    :return: Boolean
    """
    for key, value in expected.items():
        if key not in actual.keys():
            return False

        if actual[key] != value:
            return False

    return True


def test_insert_fake_product(monkeypatch):
    mock_db(monkeypatch)
    global fake_prod_to_insert, fake_prod_to_delete

    response = method_specified_test(url_path='add_product', method=client.post, json_data=fake_prod_to_insert,
                                     expected_status_code=201)
    return_prod = response.json()['data'][0]
    fake_prod_to_delete['id'] = return_prod['id']
    assert response.json()['message'] == ADDED_SUCCESSFULLY
    assert prod_in_returned_prod(fake_prod_to_insert, return_prod)


def test_delete_product_no_cookie():
    cookie = {COOKIE_NAME: ''}
    response = method_specified_test(url_path='delete/' + fake_prod_to_delete['id'], method=client.delete,
                                     cookies=cookie, expected_status_code=500)
    assert response.json()['message'] == NO_COOKIE_FOUND


def test_delete_product_wrong_cookie():
    cookie = {COOKIE_NAME: "asdfklsdaf"}
    response = method_specified_test(url_path='delete/' + fake_prod_to_delete['id'], method=client.delete,
                                     cookies=cookie, expected_status_code=500)
    assert response.json()['message'] == SOMETHING_WENT_WRONG


def test_delete_product_wrong_p_id(monkeypatch):
    mock_db(monkeypatch)
    cookie = {COOKIE_NAME: encrypt_string(VALID_USER_EMAIL).decode()}
    response = method_specified_test(url_path='delete/' + '606c81e24fc44f173adc5bcf', method=client.delete,
                                     cookies=cookie, expected_status_code=500)
    assert response.json()['message'] == NO_PRODUCT_WITH_THIS_ID


def test_delete_product_user_pid_not_belongs(monkeypatch):
    mock_db(monkeypatch)
    prod_id = fake_prod_to_delete['id']
    cookie = {COOKIE_NAME: encrypt_string(INVALID_USER_EMAIL).decode()}
    response = method_specified_test(url_path='delete/' + prod_id, method=client.delete,
                                     cookies=cookie, expected_status_code=403)
    assert response.json()['message'] == NO_ID_BELONGS_TO_THIS_USER


def test_delete_product_execpt_on_wrong_pid_cookie():
    cookie = {COOKIE_NAME: encrypt_string(INVALID_USER_EMAIL).decode()}
    response = method_specified_test(url_path='delete/' + 'another_sheker', method=client.delete,
                                     cookies=cookie, expected_status_code=500)
    assert response.json()['message'] == NO_PRODUCT_WITH_THIS_ID


def test_delete_fake_product(monkeypatch):
    mock_db(monkeypatch)
    global fake_prod_to_delete
    # delete returned prod
    cookie = {COOKIE_NAME: encrypt_string(VALID_USER_EMAIL).decode()}
    response = method_specified_test('delete/' + fake_prod_to_delete['id'], method=client.delete,
                                     expected_status_code=202, cookies=cookie)

    assert isinstance(response.json(), dict)

    # Validate the product is marked as Deleted
    response = method_specified_test(fake_prod_to_delete['id'], expected_status_code=200)
    product = response.json()['Products']
    assert product['status'] == 'DELETED'

# ------------------ get random products tests ------------------
def test_random_products_post():
    method_specified_test("get_random_products/2", method=client.post, expected_status_code=405)


def test_random_products_get():
    random_products = method_specified_test("get_random_products/2", method=client.get, expected_status_code=200)
    rand_prod = random_products.json()['Product']
    assert isinstance(rand_prod, list)
    assert len(rand_prod) == 2
    assert isinstance(rand_prod[0], dict)
    assert isinstance(rand_prod[1], dict)

    random_products = method_specified_test("get_random_products/16", method=client.get, expected_status_code=200)
    rand_prod = random_products.json()['Product']
    assert isinstance(rand_prod, list)
    assert len(rand_prod) == 2
    assert isinstance(rand_prod[0], dict)
    assert isinstance(rand_prod[1], dict)

    response = method_specified_test("get_random_products/-3", method=client.get, expected_status_code=406)
    assert response.json() == MUST_BE_GREATER_THEN_ZERO


# ------------------ update sold & for sale tests ------------------

def test_update_sold_exist_product():
    response = method_specified_test(url_path='update-sold', method=client.post, json_data=product_id_update_status,
                                     expected_status_code=200)
    return_ans = response.json()['Product']
    assert return_ans is True


def test_update_sold_not_exist_product():
    response = method_specified_test(url_path='update-sold', method=client.post,
                                     json_data=product_id_not_exist_update_status,
                                     expected_status_code=409)
    return_ans = response.json()['Product']
    assert return_ans is False


def test_update_for_sale_exist_product():
    response = method_specified_test(url_path='update-for-sale', method=client.post, json_data=product_id_update_status,
                                     expected_status_code=200)
    return_ans = response.json()['Product']
    assert return_ans is True


def test_update_for_sale_not_exist_product():
    response = method_specified_test(url_path='update-for-sale', method=client.post, json_data=product_id_not_exist_update_status,
                                     expected_status_code=409)
    return_ans = response.json()['Product']
    assert return_ans is False


# ------------------ search products tests ------------------
def test_search_exist_product_with_all_params():
    global fake_prod_to_insert
    main = 'אלקטרוניקה'
    sub = 'מחשב-נייד,מחשב-נייח'
    min_price = '0'
    max_price = '4000'
    city = "מיתר"
    query = main + "/" + sub + "/" + min_price + "/" + max_price + "/" + city
    response = method_specified_test(
        "search_products/" + query, method=client.get, expected_status_code=200)
    prod = response.json()['Product']
    assert len(prod) == 1
    assert isinstance(prod, list)
    fake_prod_to_insert['id'] = prod[0]['id']
    expected_prod = fake_prod_to_insert
    # assert that the product retrieved contains all the keys in the expected product
    assert all(elem in prod[0].keys() for elem in expected_prod.keys())


def test_search_exist_product_by_price_and_main_category():
    global fake_prod_to_insert
    main = 'אלקטרוניקה'
    sub = 'null'
    min_price = '0'
    max_price = '4000'
    city = "null"
    query = main + "/" + sub + "/" + min_price + "/" + max_price + "/" + city
    print("query" + query)
    response = method_specified_test(
        "search_products/" + query, method=client.get, expected_status_code=200)
    prod = response.json()['Product']
    assert len(prod) == 1
    assert isinstance(prod, list)
    fake_prod_to_insert['id'] = prod[0]['id']
    expected_prod = fake_prod_to_insert
    # assert that the product retrieved contains all the keys in the expected product
    assert all(elem in prod[0].keys() for elem in expected_prod.keys())


def test_search_exist_product_by_price_and_city():
    global fake_prod_to_insert
    main = 'null'
    sub = 'null'
    min_price = '0'
    max_price = '4000'
    city = "מיתר"
    query = main + "/" + sub + "/" + min_price + "/" + max_price + "/" + city
    response = method_specified_test(
        "search_products/" + query, method=client.get, expected_status_code=200)
    prod = response.json()['Product']
    assert len(prod) == 1
    assert isinstance(prod, list)
    fake_prod_to_insert['id'] = prod[0]['id']
    expected_prod = fake_prod_to_insert
    # assert that the product retrieved contains all the keys in the expected product
    assert all(elem in prod[0].keys() for elem in expected_prod.keys())


def test_search_exist_product_by_price_main_category_and_sub_category():
    global fake_prod_to_insert
    main = 'אלקטרוניקה'
    sub = 'מחשב-נייד,מחשב-נייח'
    min_price = '0'
    max_price = '4000'
    city = "null"
    query = main + "/" + sub + "/" + min_price + "/" + max_price + "/" + city
    response = method_specified_test(
        "search_products/" + query, method=client.get, expected_status_code=200)
    prod = response.json()['Product']
    assert len(prod) == 1
    assert isinstance(prod, list)
    fake_prod_to_insert['id'] = prod[0]['id']
    expected_prod = fake_prod_to_insert
    # assert that the product retrieved contains all the keys in the expected product
    assert all(elem in prod[0].keys() for elem in expected_prod.keys())


def test_search_no_exist_product_with_all_params_except_main():
    global fake_prod_to_insert
    main = 'אספנות'
    sub = 'מחשב-נייד,מחשב-נייח'
    min_price = '0'
    max_price = '4000'
    city = "מיתר"
    query = main + "/" + sub + "/" + min_price + "/" + max_price + "/" + city
    response = method_specified_test(
        "search_products/" + query, method=client.get, expected_status_code=204)



def test_search_no_exist_product_by_price():
    global fake_prod_to_insert
    main = 'אלקטרוניקה'
    sub = 'null'
    min_price = '3800'
    max_price = '4000'
    city = "null"
    query = main + "/" + sub + "/" + min_price + "/" + max_price + "/" + city
    response = method_specified_test(
        "search_products/" + query, method=client.get, expected_status_code=204)



def test_search_no_exist_product_by_price_and_city():
    global fake_prod_to_insert
    main = 'null'
    sub = 'null'
    min_price = '3800'
    max_price = '4000'
    city = "תל אביב"
    query = main + "/" + sub + "/" + min_price + "/" + max_price + "/" + city
    response = method_specified_test(
        "search_products/" + query, method=client.get, expected_status_code=204)



# def test_search_no_exist_product_by__sub_category():
#     main = 'אלקטרוניקה'
#     sub = 'מחשב-נייד,מחשב-נייח,טאבלט'
#     min_price = '0'
#     max_price = '4000'
#     city = "מיתר"
#     query = main + "/" + sub + "/" + min_price + "/" + max_price + "/" + city
#     response = method_specified_test(
#         "search_products/" + query, method=client.get, expected_status_code=204)
#     prod = response.json()['Product']
#     assert len(prod) == 0
