import os
import Backend.tests.initialize as init
from fastapi.testclient import TestClient
from Backend.server.app import app
from Backend.server.routers.events import EMAIL_IS_NOT_EXISTS, ADDED_SUCCESSFULLY, MUST_BE_GREATER_THEN_ZERO,DELETED
import pytest
import sys

client = TestClient(app)
VALID_ASC_EMAIL = 'or4family@gmail.com'
INVALID_ASC_EMAIL = 'nonexist@gmail.com'
BASE_URL = '/association/event/'

OS_SEPRATOR = '/' if 'linux' in sys.platform else '\\'


@pytest.fixture(scope="module", autouse=True)
def pytest_sessionstart(session_mocker):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """
    db_col_to_clean = [
        {
            'name': 'Events.json',
            'collection': init.events_collection
        },{
            'name': 'Associations.json',
            'collection': init.associations_collection
        }]
    for col in db_col_to_clean:
        json_file = str(os.path.dirname(__file__)) + OS_SEPRATOR + '..' + OS_SEPRATOR + 'files' + OS_SEPRATOR + col['name']
        init.clean_collection(col['collection'], json_file)



fake_event = {
    "_id": "609a9d07a9c946b46fd58c15",
    "date": "2019-06-01",
    "description": "אירוע התרמה למען משפחות שכולות",
    "association_name": "אור למשפחות",
    "association_email": VALID_ASC_EMAIL
}

class FakeDb:
    event_list = [fake_event]
    name = 'test'

    @staticmethod
    async def find_associations():
        return FakeDb.event_list

    @staticmethod
    async def insert_event(new_event: dict):
        if new_event['association_email'] != VALID_ASC_EMAIL:
            FakeDb.event_list.append(new_event)
            return new_event
        else:
            return None

    @staticmethod
    async def find_event_by_email(association_email):
        return fake_event if association_email == VALID_ASC_EMAIL else None

    @staticmethod
    async def get_random_events(num):
        return FakeDb.event_list[:num]


def mock_db(monkeypatch):
    return
    import inspect
    list_functions = inspect.getmembers(FakeDb, predicate=inspect.isfunction)
    for func in list_functions:
        monkeypatch.setattr(db, func[0], getattr(FakeDb, func[0]))


def method_specified_test(url_path, method=client.get, expected_status_code=200, json_data=None):
    """
    This function is getting the url_path for the tested url a method by which to test the url and expected status
    code for the response, and an additional json_data if valid in the url
    :param url_path: the route to test
    :param method: the method to use in the test
    :param expected_status_code: the expected status code of the response from the url
    :param json_data: the aditional json_data to send to the url
    :return: the response from the url for additional assets
    """
    response = method(url=BASE_URL + url_path, json=json_data)
    assert response.status_code == expected_status_code
    return response


def test_get_all_events(monkeypatch):
    mock_db(monkeypatch)
    response = method_specified_test("", method=client.get, expected_status_code=200)
    event_array = response.json()['Event']
    assert len(event_array) == 4
    assert isinstance(event_array, list)
    assert event_array[0]['association_name'] == 'אור למשפחות'
    assert event_array[0]['association_email'] == VALID_ASC_EMAIL


def test_get_all_events_of_specipic_association(monkeypatch):
    mock_db(monkeypatch)
    response = method_specified_test(VALID_ASC_EMAIL, expected_status_code=200)
    event_array = response.json()['Event']
    assert len(event_array) == 3
    assert isinstance(event_array, list)
    assert event_array[0]['association_name'] == 'אור למשפחות'
    assert event_array[1]['association_name'] == 'אור למשפחות'
    assert event_array[2]['association_name'] == 'אור למשפחות'


def test_get_all_events_of_non_exist_association():
    response = method_specified_test(INVALID_ASC_EMAIL, expected_status_code=404)
    assert response.json()['message'] == EMAIL_IS_NOT_EXISTS


def test_random_events():
    random_events = method_specified_test("get_random_events/5", method=client.get, expected_status_code=200)
    event_array = random_events.json()['Event']
    assert isinstance(event_array, list)
    assert len(event_array) == 4


def add_event_func(new_event, status_code):
    return method_specified_test("add_event", method=client.post, expected_status_code=status_code,
                                 json_data=new_event)

def test_add_event_with_valid_parameters(monkeypatch):
    mock_db(monkeypatch)
    new_event = {
        "date": "2021-05-26",
        "description": "אירוע התרמה חדש שנוסף בטסטים",
        "association_name": "אור למשפחות",
        "association_email": VALID_ASC_EMAIL
    }
    response = add_event_func(new_event, status_code=201)

def test_add_event_with_invalid_parameters_email(monkeypatch):
    mock_db(monkeypatch)
    new_event = {
        "date": "2021-05-26",
        "description": "אירוע התרמה חדש שנוסף בטסטים",
        "association_name": "אור למשפחות",
        "association_email": INVALID_ASC_EMAIL
    }
    response = add_event_func(new_event, status_code=404)


def test_add_event_with_invalid_parameters(monkeypatch):
    mock_db(monkeypatch)
    new_event = {
        "date": "2021-05-26",
        "description": "אירוע התרמה חדש שנוסף בטסטים",
        "association_email": VALID_ASC_EMAIL
    }
    response = add_event_func(new_event, status_code=422)
