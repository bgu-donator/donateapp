import Backend.tests.initialize as init
from fastapi.testclient import TestClient
from Backend.server.app import app
from Backend.server.routers.routes_config import ADMIN_COOKIE, encrypt_string, COOKIE_NAME
from Backend.server.routers.users import UNAUTHORIZED_ERROR, ALREADY_EXISTS_ERROR_MESSAGE,USER_ADDED_SUCCESSFULLY
import pytest
import os
import sys

client = TestClient(app)
VALID_USER_EMAIL = 'arnav@gmail.com'
INVALID_USER_EMAIL = 'not_exists@nope.com'
VALID_USER_PASS = '123456'

OS_SEPRATOR = '/' if 'linux' in sys.platform else '\\'

@pytest.fixture(scope="module", autouse=True)
def pytest_sessionstart(session_mocker):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """
    json_file = str(os.path.dirname(__file__)) + OS_SEPRATOR + '..' + OS_SEPRATOR + 'files' + OS_SEPRATOR + 'Users.json'
    init.clean_collection(init.users_collection, json_file)


logined_user = {
    "id": '48194dsaf78a0ds9f',
    "name": 'Test',
    "email": VALID_USER_EMAIL,
    "username": 'testUser',
    "total_donation": 250,
    "is_admin": False,
    'password': VALID_USER_PASS,
    "products_for_sale": []
}


@pytest.fixture(autouse=True)
def after_each_delete_invalid_user():
    """
    This is an after_each function
    it runs after each test
    to validate all the function that are called after the yield statement
    """
    yield
    response = client.get(url='/users/'+INVALID_USER_EMAIL)
    if response.status_code == 200:
        cookie = {ADMIN_COOKIE: encrypt_string(VALID_USER_EMAIL).decode()}
        method_specified_test('/admin/delete/'+INVALID_USER_EMAIL, method=client.delete,
                              expected_status_code=200, json_data=INVALID_USER_EMAIL,
                              cookie=cookie)



# @pytest.fixture(autouse=True)
# def before_each_set_mock_db(monkeypatch):
#     """
#     This is an before_each function
#     it runs before each test
#     to validate all the function that are called before the yield statement
#     """
#     mock_db(monkeypatch)
#     yield


class FakeDb:
    def __init__(self):
        name = 'test'

    @staticmethod
    async def login(email, passw):
        return logined_user if email == VALID_USER_EMAIL else None

    @staticmethod
    async def find_user_by_email(email):
        return logined_user if email == VALID_USER_EMAIL else None

    @staticmethod
    async def insert_user(user: dict):
        return logined_user

    @staticmethod
    async def get_donations_for_user(email):
        return [] if email == VALID_USER_EMAIL else None


def mock_db(monkeypatch):
    return
    import inspect
    list_functions = inspect.getmembers(FakeDb, predicate=inspect.isfunction)
    for func in list_functions:
        monkeypatch.setattr(db, func[0], getattr(FakeDb, func[0]))


def method_specified_test(url_path, method=client.get, expected_status_code=200, json_data=None, cookie=None):
    """
    This function is getting the url_path for the tested url a method by which to test the url and expected status
    code for the response, and an additional json_data if valid in the url
    :param url_path: the route to test
    :param method: the method to use in the test
    :param expected_status_code: the expected status code of the response from the url
    :param json_data: the aditional json_data to send to the url
    :return: the response from the url for additional assets
    """
    response = method(url=url_path, json=json_data, cookies=cookie)
    assert response.status_code == expected_status_code
    return response


def test_users_with_no_path():
    method_specified_test("/users", method=client.get, expected_status_code=404)


def test_users_get_user_by_email(monkeypatch):
    mock_db(monkeypatch)
    response = method_specified_test("/users/"+VALID_USER_EMAIL, method=client.get, expected_status_code=200)
    assert (response.json()['User']['email'], VALID_USER_EMAIL)


def test_users_get_user_by_email_not_exists():
    method_specified_test("/users/"+INVALID_USER_EMAIL, method=client.get, expected_status_code=404)


def test_users_logout_get():
    method_specified_test("/users/logout", method=client.get, expected_status_code=404)


def test_users_login_get():
    method_specified_test("/users/login", method=client.get, expected_status_code=404)


def test_users_login_post_succeed(monkeypatch):
    mock_db(monkeypatch)
    user_to_login = {
        'email': VALID_USER_EMAIL,
        'password': VALID_USER_PASS
    }
    response = method_specified_test("/users/login", method=client.post, json_data=user_to_login)
    assert response.json() == user_to_login['email']


def test_users_login_post_fail(monkeypatch):
    mock_db(monkeypatch)
    user_to_login = {
        'email': INVALID_USER_EMAIL,
        'password': VALID_USER_PASS
    }
    response = method_specified_test("/users/login", method=client.post, json_data=user_to_login, expected_status_code=401)
    assert response.json() == UNAUTHORIZED_ERROR


def test_users_logout_post():
    response = method_specified_test("/users/logout", method=client.post)
    assert response.json() == 'Logged Out'


def test_users_register_get():
    method_specified_test('/users/register', method=client.get, expected_status_code=404)


def test_users_register_existing_email(monkeypatch):
    mock_db(monkeypatch)
    user_to_register = {
        'name': 'shtut',
        'username': 'shtut',
        'email': VALID_USER_EMAIL,
        'password': VALID_USER_PASS
    }
    response = method_specified_test('/users/register', method=client.post, expected_status_code=409, json_data=user_to_register)
    assert response.json()['message'] == ALREADY_EXISTS_ERROR_MESSAGE


def test_users_register_new_email(monkeypatch):
    mock_db(monkeypatch)
    user_to_register = {
        'name': 'shtut',
        'username': 'shtut',
        'email': INVALID_USER_EMAIL,
        'password': VALID_USER_PASS
    }
    response = method_specified_test('/users/register', method=client.post, expected_status_code=201, json_data=user_to_register)
    assert response.json()['message'] == USER_ADDED_SUCCESSFULLY


def test_get_donations_for_user_post():
    method_specified_test('/users/get_donations/' + INVALID_USER_EMAIL, method=client.post, expected_status_code=405)


def test_get_donations_for_user_invalid_user(monkeypatch):
    mock_db(monkeypatch)
    method_specified_test('/users/get_donations/' + INVALID_USER_EMAIL, expected_status_code=404)


def test_get_donations_for_user_valid_user(monkeypatch):
    mock_db(monkeypatch)
    user_donation = method_specified_test('/users/get_donations/' + VALID_USER_EMAIL, expected_status_code=200)
    assert isinstance(user_donation.json()['Donation'], list)
    assert len(user_donation.json()['Donation']) == 0


def test_users_change_password_no_cookie(monkeypatch):
    mock_db(monkeypatch)
    json_data = {
        'current_password': '123456',
        'new_password': "111111",
        'confirm_password': "111111"
    }
    method_specified_test('/users/change-password', method=client.patch, expected_status_code=500, json_data=json_data)

def test_users_change_password_invalid_cookie(monkeypatch):
    mock_db(monkeypatch)
    cookie = {COOKIE_NAME: encrypt_string("blabla").decode()}
    json_data = {
        'current_password': '123456',
        'new_password': "111111",
        'confirm_password': "111111"
    }
    method_specified_test('/users/change-password', method=client.patch, expected_status_code=500, json_data=json_data,
                          cookie=cookie)


def test_users_change_password_invalid_password(monkeypatch):
    mock_db(monkeypatch)
    cookie = {COOKIE_NAME: encrypt_string("arnav@gmail.com").decode()}
    json_data = {
        'current_password': '1234567',
        'new_password': "111111",
        'confirm_password': "111111"
    }
    method_specified_test('/users/change-password', method=client.patch, expected_status_code=404, json_data=json_data,
                          cookie=cookie)


def test_users_change_password_invalid_confirm(monkeypatch):
    mock_db(monkeypatch)
    cookie = {COOKIE_NAME: encrypt_string("arnav@gmail.com").decode()}
    json_data = {
        'current_password': '123456',
        'new_password': "111111",
        'confirm_password': "11111"
    }
    method_specified_test('/users/change-password', method=client.patch, expected_status_code=409, json_data=json_data,
                          cookie=cookie)


def test_users_change_password_valid_confirm(monkeypatch):
    mock_db(monkeypatch)
    cookie = {COOKIE_NAME: encrypt_string("arnav@gmail.com").decode()}
    json_data = {
        'current_password': '123456',
        'new_password': "111111",
        'confirm_password': "111111"
    }
    method_specified_test('/users/change-password', method=client.patch, expected_status_code=202, json_data=json_data,
                          cookie=cookie)
    json_data = {
        'current_password': '111111',
        'new_password': "123456",
        'confirm_password': "123456"
    }
    method_specified_test('/users/change-password', method=client.patch, expected_status_code=202, json_data=json_data,
                          cookie=cookie)


def test_users_update_details_no_cookie(monkeypatch):
    mock_db(monkeypatch)
    json_data = {
        "email": "arnav@gmail.com",
        "username": "user",
        "firstName": "user",
        "lastName": "user"
    }
    method_specified_test('/users/update', method=client.patch, expected_status_code=500, json_data=json_data)


def test_users_update_details_invalid_cookie(monkeypatch):
    mock_db(monkeypatch)
    cookie = {COOKIE_NAME: encrypt_string("rtrtrtr").decode()}
    json_data = {
        "email": "arnav@gmail.com",
        "username": "user",
        "firstName": "user",
        "lastName": "user"
    }
    method_specified_test('/users/update', method=client.patch, expected_status_code=500, json_data=json_data,
                          cookie=cookie)


def test_users_update_details_invalid_cookie_mail(monkeypatch):
    mock_db(monkeypatch)
    cookie = {COOKIE_NAME: encrypt_string("arnav12@gmail.com").decode()}
    json_data = {
        "email": "arnav@gmail.com",
        "username": "user",
        "firstName": "user",
        "lastName": "user"
    }
    method_specified_test('/users/update', method=client.patch, expected_status_code=409, json_data=json_data,
                          cookie=cookie)


def test_users_update_details_valid(monkeypatch):
    mock_db(monkeypatch)
    cookie = {COOKIE_NAME: encrypt_string("arnav@gmail.com").decode()}
    json_data = {
        "email": "arnav@gmail.com",
        "username": "user",
        "firstName": "user",
        "lastName": "user"
    }
    method_specified_test('/users/update', method=client.patch, expected_status_code=202, json_data=json_data,
                          cookie=cookie)

    response = method_specified_test("/users/"+"arnav@gmail.com", method=client.get)
    assert (response.json()['User']['name'],json_data["firstName"]+" "+json_data['lastName'])
    assert (response.json()['User']['username'],json_data["username"])
    json_data = {
        "email": "arnav@gmail.com",
        "username": "john_doe",
        "firstName": "John",
        "lastName": "Doe"
    }
    method_specified_test('/users/update', method=client.patch, expected_status_code=202, json_data=json_data,
                          cookie=cookie)


def test_users_get_user_products(monkeypatch):
    mock_db(monkeypatch)
    response = method_specified_test("/users/"+VALID_USER_EMAIL, method=client.get, expected_status_code=200)
    assert (len(response.json()['User']['products_for_sale']), 3)
